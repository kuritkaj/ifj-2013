/*
    IFJ 2013

    xmarko07 - Antonín Marko
    xmahne00 - Jakub Mahnert
    xkurit00 - Jakub Kuřitka
    xkubic34 - Martin Kubíček
    xlechp00  - Pali Lech

    "Let the C language die."
 */
#ifndef PARSER_H
#define PARSER_H

#include "ast.h"
#include "common.h"


/* Hlavicky */

void parser_init(struct data* d);
bool parser_run(struct data* d);

bool accept(struct data* d, enum token_type t);
bool accept_next(struct data* d, enum token_type t);
bool expect(struct data* d, enum token_type t, int error);
bool expect_next(struct data* d, enum token_type t, int error);

bool token_empty(struct data* d);
bool equals_sign(struct data* d);
bool semicolon(struct data* d);
bool comma(struct data* d);
bool lpar(struct data* d);
bool rpar(struct data* d);
bool lbrace(struct data* d);
bool rbrace(struct data* d);
bool keyword_if(struct data* d);
bool keyword_else(struct data* d);
bool keyword_while(struct data* d);
bool keyword_return(struct data* d);
bool keyword_function(struct data* d);

bool id(struct data* d, string* id);
bool var(struct data* d, string* var_name);
bool expr(struct data* d, struct ast_node* expression);
bool assign(struct data* d, struct ast_node* assign);
bool fn_arguments(struct data* d, struct ast_node* arguments);
bool fn_definition(struct data* d, struct ast_node* definition);
bool _while(struct data* d, struct ast_node* _while);
bool _if(struct data* d, struct ast_node* _if);
bool _return(struct data* d, struct ast_node* _return);
bool fn_call_arguments(struct data* d, struct ast_node* arguments);
bool fn_call(struct data* d, struct ast_node* f_call);
bool statement_decision(struct data* d, struct ast_node* result, bool in_root);
bool statement(struct data* d, struct ast_node* statement, bool in_root);
bool program_body(struct data* d, struct ast_node* tree);

struct ast_node* make_var(string* var_name);
struct ast_node* make_assign(string* var_name, struct ast_node* expression);
struct ast_node* make_fn_definition(string* name, struct ast_node* arguments, struct ast_node* body);
struct ast_node* make_while(struct ast_node* condition, struct ast_node* body);
struct ast_node* make_if(struct ast_node* condition, struct ast_node* if_body, struct ast_node* else_body);
struct ast_node* make_return(struct ast_node* expr);
struct ast_node* make_fn_call(string* name, struct ast_node* args);


/*
 * Makro, ktere ukonci funkci a vrati false, kdyz nastal error
 */
#define EXPECT(e)\
		if(!e) return false;


#define EXPECTO_PATRONUM(e)\
        if(!e) return false;


#define EXPECTO_NOT_ERRORUM(d)\
        if(d->error != 0) return false;

#endif
