/*
    IFJ 2013

    xmarko07 - Antonín Marko
    xmahne00 - Jakub Mahnert
    xkurit00 - Jakub Kuřitka
    xkubic34 - Martin Kubíček
    xlechp00  - Pali Lech

    "Let the C language die."
 */

#include <stdio.h>
#include "str.h"
#include "ast.h"

#ifndef COMMON_H
#define COMMON_H

/* Falesne struktury, dokud mi je neposkytne scanner */
enum token_type {
    T_ID,// 0
    T_VAR,// 1

    T_NUMBER, // 2 ukladani cisel
    T_STRING, // 3 ukladani stringu

    T_FUNCTION, // 4
    T_IF, // 5
    T_ELSE, // 6
    T_WHILE, // 7
    T_RETURN, // 8

    T_LPAR, // 9 (
    T_RPAR, // 10 )
    T_LBRACE, // 11 {
    T_RBRACE, // 12}
    T_NOT, // 13
    T_PLUS, // 14
    T_MINUS, // 15
    T_MULTIPLY, // 16
    T_DIVIDE, // 17
    T_LESS, // 18
    T_MORE, // 19
    T_LESSOREQ, // 20
    T_MOREOREQ, // 21
    T_EQUALS, // 22 ==
    T_NOTEQUAL, // 23 !=
    T_STRICTEQ, // 24 ===
    T_STRICTNOTEQ, // !25 ==
    
    T_ASSIGN, // 26 =
    T_TRUE, // 27
    T_FALSE, // 28
    T_NULL, //29 
    T_COMMA, // 30
    T_SEMICOLON, // 31
    T_CONCATENATE, // 32

    T_THE_END // 33
};

struct token
{
    enum token_type type;
    int line;
    
    union token_data
    {
        string* string_data;
        double  numeric_data; 
    } d;
};

struct symbol_table_data
{
	double numeric_data;
	string* string_data;
	enum ast_literal_type literal_type;
};

struct symbol_table
{
	//klic a nebo nazev promenne
	char* key;
	
	//typ obsahu
	enum ast_var_type var_type;
	
	//datova struktura
	struct symbol_table_data d;
		
	//v pripade funkce pro uchovani posloupnosti prikazu
	struct ast_node* statement;
	struct ast_list* params;
	
	//leva a prava podvetev
	struct symbol_table *left;
	struct symbol_table *right;
};

struct data
{
    int error; // jsem se ulozi error, ten se dispatchne na nejvyssi urovni
    
    struct token* token; // aktualni token
    struct token* token_next; // nasledujici token
    
    FILE* file; // soubor, ktery bude interpretovan

    struct symbol_table* functions;
    struct symbol_table* vars;

    struct ast_node* tree;
};

#endif
