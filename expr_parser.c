/*
    IFJ 2013

    xmarko07 - Antonín Marko
    xmahne00 - Jakub Mahnert
    xkurit00 - Jakub Kuřitka
    xkubic34 - Martin Kubíček
    xlechp00  - Pali Lech

    "Let the C language die."
 */

#include <string.h>
#include "parser.h"
#include "stdbool.h"
#include "common.h"
#include "const.h"
#include "stack.h"
#include "gc.h"
#include "ast.h"
#include "scanner.h"
#include "expr_parser.h"

#define PRINT 1


/**
 * kontroluje, jestli je token typu ktery muze byt ve vyrazu
 * @param  data allfather
 * @return      bitches ain't shit but hoes and tricks
 */
int is_expression(struct data * d)
{
	return ((d->token->type == T_LPAR) || (d->token->type == T_RPAR) || \
		(d->token->type == T_NUMBER) || (d->token->type == T_PLUS) || \
		(d->token->type == T_MINUS) || (d->token->type == T_VAR) || \
		(d->token->type == T_MULTIPLY) || (d->token->type == T_DIVIDE) || \
		(d->token->type == T_EQUALS) || (d->token->type == T_NOTEQUAL) || \
		(d->token->type == T_MOREOREQ) || (d->token->type == T_LESSOREQ) || \
		(d->token->type == T_STRICTEQ) || (d->token->type == T_STRICTNOTEQ) || \
		(d->token->type == T_ID) || (d->token->type == T_MORE) || \
		(d->token->type == T_LESS));
}
/**
 * kontroluje, jestli je prave probirany token vnitrni vyrazovy delimiter (equals, assign a podobne)
 * @param  data [description]
 * @return      [description]
 */
int is_delimiter(struct data * d)
{
	return (d->token->type == T_EQUALS || d->token->type == T_NOTEQUAL || \
		d->token->type == T_LESSOREQ || d->token->type == T_MOREOREQ || \
		d->token->type == T_STRICTEQ || d->token->type == T_STRICTNOTEQ || \
		d->token->type == T_ASSIGN || d->token->type == T_LESS || d->token->type == T_MORE);
}

/**
 * kontroluje, jestli bylo dosazeno konce vyrazu
 * pocita "interne" zavorky ve vyrazu
 * 	leva zavorka pricita jednicku, prava zavorka odecita jednicku
 * @param  data             allfather
 * @param  paretheses_count pocet zavorek ve vyrazu
 * @return                  pravda/nepravda
 */
int is_end(struct data * d, int * paretheses_count)
{
	if (d->token_next->type == T_THE_END) {
		return 1;
	}
	//vyraz konci, pokud je na konci prava zavorka navic
	if ((d->token->type == T_RPAR) && (*paretheses_count == 0))
		return 1;
	//vyraz konci, pokud je na konci strednik
	if (d->token->type == T_SEMICOLON)
		return 1;
	//vyraz konci, pokud je na konci carka (oddelovac argumentu volane funkce)
	if (d->token->type == T_COMMA)
		return 1;
	//pokud je zpracovavana leva zavorka, pricte se jednicka k pocitadlu
	if (d->token->type == T_LPAR) 
		(*paretheses_count)++;
	//pokud je zpracovavana prava zavorka, tak se odecte jednicka
	if (d->token->type == T_RPAR)
		(*paretheses_count)--;
	return 0;

}
/**
 * kontroluje validitu vyrazu (operand operator operand operator atd)
 * @param  data     [description]
 * @param  validity [description]
 * @return          [description]
 */
void check_validity(struct data * d, int * validity)
{
	if (d->token->type == T_VAR || d->token->type == T_NUMBER) {
		if (*validity == 0)
			(*validity) = 1;
		else (*validity) = -1;
	}
	else if (d->token->type == T_MINUS || d->token->type == T_PLUS || \
		d->token->type == T_MULTIPLY || d->token->type == T_DIVIDE) {
			if (*validity == 1)
				(*validity) = 0;
			else (*validity) = -1;
		}
	else if (d->token->type == T_ID) {
		*validity = 1;
	}
}

/**
 * dalsi pomocna procedura k infix2postfix
 * @param s             zasobnik
 * @param post_expr     vysledny vyraz
 * @param post_expr_len delka vysledneho vyrazu
 */
void until_left_par (struct stack * s, struct expr_list * list)
{
	string * stck_top = new_str(NULL);
	do {
		if (stack_empty(s)) return;
		stck_top = stack_top(s);
		if (strcmp(stck_top->str, "(") != 0)
			add_to_list(list, new_op_node(stck_top));
		stack_pop(s);
		stck_top = stack_top(s);
	} while(strcmp(stck_top->str, "(") != 0);

}

/**
 * pomocna procedura pro funkci infix2postfix
 * @param s             zasobnik
 * @param data.token.d->id        aktualni zpracovavany token
 * @param post_expr     vysledny vyraz
 * @param post_expr_len delka vysledneho vyrazu
 */
void do_operation (struct stack * s, struct data * d, struct expr_list * list)
{
	string * stck_top = new_str(NULL);
	while (1) {
		stck_top = stack_top(s);;
		if (stack_empty(s) || (!strcmp(stck_top->str, "("))) {
			stack_push(s, d->token->d.string_data);
			break;
		} else if (((d->token->type == T_MULTIPLY) || (d->token->type == T_DIVIDE)) && ((strcmp(stck_top->str, "+") == 0) || (strcmp(stck_top->str, "-") == 0))) {
			stack_push(s, d->token->d.string_data);
			break;
		} else {
			if (strcmp(stck_top->str, "(") != 0) {
				add_to_list(list, new_op_node(stck_top));
			}
			stack_pop(s);
		} 
	}	
}
/**
 * 	vytvari jednosmerne vazany seznam pro elegantni drzeni postfixoveho vyrazu
 * 	fce pro pridavani nodu do seznamu
 */
void add_to_list(struct expr_list * list, struct ast_node * node)
{
	if (!(list->first)) {
		//prvni node
		list->first = list->current = node;
		list->current->right = list->current->left = NULL;
	} else {
		list->current->right = node;
		list->current = list->current->right;
		list->current = node;
		list->current->right = list->current->left = NULL;
		return;
	}
}
/**
 * zaobalovaci funkce pro get next token, stejne ji nepouzivam, ale w/e
 * rekneme ze tu je z legacy duvodu :DDD
 * @param  d [description]
 * @return   [description]
 */
int get_another_token(struct data * d)
{
	if (d->token_next->type == T_THE_END)
		return 0;
	else {
		if (PRINT) printf(CYAN "expr_parser:getting next token, now it's %d\n" RESET, d->token->type);
		EXPECTO_PATRONUM (get_next_token(d));
		if (PRINT) printf(CYAN "expr_parser:getting next token, after it's %d\n" RESET, d->token->type);
		return 1;
	}
}
/**
 * self expln
 */
struct expr_list * init_list()
{
	struct expr_list * temp = (struct expr_list *) gc_malloc(sizeof(struct expr_list));
	temp->first = temp->current = NULL;
	if (PRINT) printf(CYAN "expr_parser: initialized list %p\n" RESET, (void*)temp);
	return temp;
}
/**
 * parsovani stringu a kokotenace
 * @param  d [description]
 * @return   [description]
 */
struct ast_node * parse_concatenation (struct data * d)
{
	struct ast_node * radix;
	//prvni token co prijde
	if (d->token->type == T_STRING) 
		radix = new_string_node(d);
	else if (d->token->type == T_VAR)
		radix = new_var_node(d);
	else if (d->token->type == T_ID)
		radix = new_fn_call_node(d);
	else
		return NULL;
	EXPECTO_PATRONUM (get_next_token(d));
	//nejde o kokotenaci, jen o jeden string/fn/whatever, koncime
	if (d->token->type != T_CONCATENATE) {
		if (PRINT) printf(CYAN "expr_parser:string: parsing solo string %p\n" RESET, (void *)radix);
		return radix;
	}
	//ted uz je to jen konkatenace
	while ((d->token->type == T_CONCATENATE) && ((d->token_next->type == T_STRING) || (d->token_next->type == T_VAR) || (d->token_next->type == T_ID))) {
		if (PRINT) printf(CYAN "expr_parser: PARSING CONCATENATION OF %d\n" RESET, d->token->type);
		//node konkatenace
		struct ast_node * new_radix = ast_create_node();
		new_radix->left = ast_create_node();
		new_radix->right = ast_create_node();

		new_radix->type = AST_BINARY_OP;
		new_radix->d.binary = AST_BINARY_STRING_CONCATENATION;
		*(new_radix->left) = *radix;
		EXPECTO_PATRONUM (get_next_token(d));
		if (d->token->type == T_STRING) {
			if (PRINT) printf(CYAN "expr_parser: with string %s\n" RESET, d->token->d.string_data->str);
			new_radix->right = (struct ast_node *) new_string_node(d);
		} else if (d->token->type == T_VAR) {
			if (PRINT) printf(CYAN "expr_parser: with var %s\n" RESET, d->token->d.string_data->str);
			new_radix->right = (struct ast_node *) new_var_node(d);
		} else {
			if (PRINT) printf(CYAN "expr_parser: with ID CALL %s\n" RESET, d->token->d.string_data->str);
			radix = ast_create_node();
			fn_call(d, radix);
			*(new_radix->right) = *radix;
		}
		radix = new_radix;
		EXPECTO_PATRONUM (get_next_token(d));
	}
	return radix;
}

struct ast_node * parse_boolean (struct data * d)
{
	struct ast_node * radix = ast_create_node();
	if (PRINT) printf(CYAN "expr_parser: PARSING BOOLEAN %d\n" RESET, d->token->type);
	radix->type = AST_LITERAL;
	if (d->token->type == T_TRUE)
		radix->literal = AST_LITERAL_TRUE;
	else if (d->token->type == T_FALSE)
		radix->literal = AST_LITERAL_FALSE;
	else if (d->token->type == T_NULL)
		radix->literal =AST_LITERAL_NULL;
	else
		return NULL;
	EXPECTO_PATRONUM ( get_next_token(d) );
	return radix;
}

/**
 * novy stringovy node
 * @param  d [description]
 * @return   [description]
 */
struct ast_node * new_string_node(struct data * d)
{
	struct ast_node * temp = ast_create_node();
	temp->type = AST_LITERAL;
	temp->literal = AST_LITERAL_STRING;
	temp->var_type = AST_VAR_STRING;
	temp->left = temp->right = NULL;
	
	// jakubik
	temp->d.string_data = new_str(NULL);
	*(temp->d.string_data) = *(d->token->d.string_data); // musi se predavat hodnout, jinak se pri dalsim nacteni tokenu zmeni hodnota v pointeru
	// end jakubik

	//if (PRINT) printf("expr_parser:string:new node is %s\n", temp->d.string_data->str);
	return temp;
}
/**
 * konvertuje infixovy retezec na postfixovy pro dalsi zpracovani
 * @return postfixovy retezec ve forme ukazatele na string
 */
struct expr_list * infix2postfix (struct data * d, int * continues)
{
	struct expr_list * post_expr = init_list();
	string * stck_top = new_str(NULL);

	int paretheses_count = 0;
	struct stack * s;
	EXPECTO_PATRONUM ((s = (struct stack*) gc_malloc(sizeof(struct stack))));
	stack_init(s);
	int validity = 0;
	int tokens = 0;
	int mrdat = 1;
	//if (PRINT) printf("expr_parser: token-> %s %d\n", d->token->d.string_data->str, d->token->type);
	while (!(is_end(d, &paretheses_count))) {
		//if (PRINT) printf("expr_parser:valid token, ID is %d\n",d->token->type);
		mrdat = 0;
		EXPECTO_PATRONUM (is_expression(d));
		check_validity(d, &validity);
		
		// promin Kubo
		struct token* previous = gc_malloc(sizeof(struct token));
		*previous = *(d->token);

		if (is_delimiter(d)) {
			if (PRINT) printf(CYAN "expr_parser:%s IS DELIMITER\n" RESET, d->token->d.string_data->str);
			*continues = 1;
			break;
		} else if (d->token->type == T_LPAR) {
			//vstup je leva zavorka
			if (PRINT) printf(CYAN "expr_parser: IS LEFT_PAR\n" RESET);
			stack_push(s, d->token->d.string_data);
		} else if (d->token->type == T_RPAR) {
			//vstup je prava zavorka
			if (PRINT) printf(CYAN "expr_parser: IS RIGHT_PAR\n" RESET);
			until_left_par(s, post_expr);
		} else if ((d->token->type == T_MULTIPLY) || (d->token->type == T_DIVIDE) || (d->token->type == T_PLUS) || (d->token->type == T_MINUS)) {
			//vstup je operator
			if (PRINT) printf(CYAN "expr_parser: IS OPERATOR\n" RESET);
			do_operation(s, d, post_expr);
		} else if (d->token->type == T_ID) {
			//vstup je volaniiiii vzdusnejch zamkuuuuu
			if (PRINT) printf(CYAN "expr_parser: IS FN_CALL\n" RESET);
			add_to_list(post_expr, new_fn_call_node(d));
		} else { //vstup je promenna nebo literal
			if (PRINT) printf(CYAN "expr_parser: IS VARIABLE\n" RESET);
			add_to_list(post_expr, new_var_node(d));
		}

		// fakt se za to omlouvam
		if(previous->type != T_ID)
			EXPECT (get_next_token(d));
		
		tokens++;
	}
	if (PRINT) printf(CYAN "expr_parser:finished - parsed %d tokens.\n" RESET, tokens);
	//kontrola validity - spravny vyraz konci jednickou!
	if ((validity != 1) && (mrdat == 0)) {
		return NULL;
	}
	//jsme uz na konci vyrazu, proto je cas ho prevest
	while (!(stack_empty(s))) {
		//if (PRINT) printf("expr_parser:mary POPpins\n");
		stck_top = stack_pop(s);
		if (strcmp(stck_top->str, "(") != 0) {
			add_to_list(post_expr, new_op_node(stck_top));
		}
	}
	return post_expr;
}
/**
 * vytvari node operatoru
 * @param  op [description]
 * @return    [description]
 */
struct ast_node * new_op_node(string * op)
{
	//if (PRINT) printf("expr_parser: new_op_node of %s\n",op->str);
	struct ast_node * temp = ast_create_node();
	if (strcmp(op->str, "-") == 0) 
		temp->d.binary = AST_BINARY_MINUS;
	else if (strcmp(op->str, "+") == 0)
		temp->d.binary = AST_BINARY_PLUS;
	else if (strcmp(op->str, "*") == 0)
		temp->d.binary = AST_BINARY_TIMES;
	else if (strcmp(op->str, "/") == 0)
		temp->d.binary = AST_BINARY_DIVIDE;
	else if (strcmp(op->str, "<") == 0)
		temp->d.binary = AST_BINARY_LESS;
	else if (strcmp(op->str, ">") == 0)
		temp->d.binary = AST_BINARY_MORE;
	else if (strcmp(op->str, "==") == 0)
		temp->d.binary = AST_BINARY_EQUALS;
	else if (strcmp(op->str, "!=") == 0)
		temp->d.binary = AST_BINARY_NOT_EQUALS;
	else if (strcmp(op->str, "<=") == 0)
		temp->d.binary = AST_BINARY_LESS_EQUALS;
	else if (strcmp(op->str, ">=") == 0)
		temp->d.binary = AST_BINARY_MORE_EQUALS;
	else if (strcmp(op->str, "===") == 0)
		temp->d.binary = AST_BINARY_STRICT_EQUALS;
	else if (strcmp(op->str, "!==") == 0)
		temp->d.binary = AST_BINARY_STRICT_NOT_EQUALS;
	else if (strcmp(op->str, "=") == 0)
		temp->d.binary = AST_ASSIGN;
	else {
		if (PRINT) printf("tfw no gf\n");
		return NULL;
	}

	temp->type = AST_BINARY_OP;
	return temp;
}
/**
 * vytvari node pro volani funkce, WIP lebo je to v pici jak krava
 * @param  d [description]
 * @return   [description]
 */
struct ast_node * new_fn_call_node(struct data * d)
{
	struct ast_node * temp = (struct ast_node*) gc_malloc(sizeof(struct ast_node));
	fn_call(d, temp);
	return temp;

}

/**
 * vytvari node promennych a konstant (ale nenapada me uz jak ty picoviny furt pojmenovavat)
 * @param  tken [description]
 * @return      [description]
 */
struct ast_node * new_var_node(struct data * d)
{
	struct ast_node * temp = (struct ast_node *) gc_malloc(sizeof(struct ast_node));
	if (d->token->type == T_VAR) { //jde o promennou
		//if (PRINT) printf("expr_parser:new var node\n");
		temp->type = AST_VAR;
		temp->d.string_data = new_str(NULL);
		*(temp->d.string_data) = *(d->token->d.string_data);
		temp->var_type = AST_VAR_DOUBLE;
		temp->left = temp->right = NULL;
	} else { //jde o literal, ASPON DOUFAAAAAAAAAM
		//if (PRINT) printf("expr_parser:new literal node\n");
		temp->type = AST_LITERAL;
		switch (d->token->type) {
			case T_TRUE: {
				temp->literal = AST_LITERAL_TRUE;
				temp->var_type = AST_VAR_BOOLEAN;
				break;
			}
			case T_FALSE: {
				temp->literal = AST_LITERAL_FALSE;
				temp->var_type = AST_VAR_BOOLEAN;
				break;
			}
			case T_NULL: {
				temp->literal = AST_LITERAL_NULL;
				temp->var_type = AST_VAR_NULL;
				break;
			}
			case T_NUMBER: {
				temp->d.numeric_data = d->token->d.numeric_data;
				temp->literal = AST_LITERAL_NUMERIC;
				temp->var_type = AST_VAR_DOUBLE;
				break;
			}
			default: return NULL;
		}
	}
	return temp;
	//tobedone
}


/**
 * cekuje jestli je nejblizsi nasledujici trojice v seznamu vyrazu zjebnutelna do stromecku
 * @param  list zoznam vole
 * @return      fuck the police
 */
int is_triplet(struct expr_list * list)
{
	EXPECTO_PATRONUM (list->current);
	EXPECTO_PATRONUM (list->current->right);
	EXPECTO_PATRONUM (list->current->right->right);
	EXPECTO_PATRONUM (((list->current->type == AST_VAR) || (list->current->type == AST_LITERAL) || (list->current->type == AST_CALL) || (list->current->type == AST_RETURN)));
	EXPECTO_PATRONUM (((list->current->right->type == AST_LITERAL) || (list->current->right->type == AST_VAR) || (list->current->right->type == AST_CALL) || (list->current->right->type == AST_RETURN)));
	EXPECTO_PATRONUM (((list->current->right->right->type == AST_BINARY_OP)));
	return 1;
}
/**
 * vytvori ze soucasny, nasledujici a dalsi nasledujici bunky trojicku,
 * zestromuje ji a zaradi do seznamu. treba nejdriv zkontrolovat jestli to jde!!!!
 * jo, a nedela to cistej seznam, budou tam zdvojeny nebinarni nody ktery se mazou!!
 * ZDVOJENY NODY MAJ TYP AST_RETURN!!!!!!!!!
 * hakuna matata
 * hakuna matata
 * hakuna matata
 * @param  list [description]
 * @return      wu tang clan ain't nuttin to fuck wit!
 */
void form_triplet(struct expr_list * list)
{
	struct ast_node * hakunamatata = ast_create_node();
	hakunamatata->type = AST_RETURN;
	struct ast_node * op;
	hakunamatata->right = list->current->right->right->right;
	int was_first = 1;
	if (list->current != list->first) {
		was_first = 0;
		struct ast_node * previous = list->first;
		while ((previous) && (previous->right != list->current)) {
			previous = previous->right;
		}
		previous->right = hakunamatata;
	}	
	//tohle ani nebudu debugovat, kdyz to nepujde tak proste skocim ze skaly a vy budete doufat ze vam projekt odpusti
	op = list->current->right->right;
	op->left = list->current;
	op->right = list->current->right;
	list->current->right->right = NULL;
	list->current->right = NULL;
	hakunamatata->left = op;
	if (was_first) {
		list->first = list->current = hakunamatata;
	}
	if (PRINT) printf(CYAN "expr_parser:new node:OP is %d,operands %d and %d\n" RESET, op->d.binary,op->left->type,op->right->type);
	//baka it's not like I wanted to enjoy this or anything :X
}

/**
 * prevadi drive vytvoreny jednosmerny seznam s postfixovym vyrazem na AST
 * seznam neni cisty!!! potreba vycistit nebinarni nody!!
 * ZDVOJENY NEBINARNI NODY MAJI TYP AST_RETURN!!!!!!!!!!!!!!!!!!!!!!
 * @param  list postfixovy vyraz
 * @return      ukazatel na "vrsek" (zacatek, radix, w/e) stromku
 */
struct ast_node * list2ast_tree(struct expr_list * list)
{
	//nutne minimum
	list->current = list->first;
	EXPECTO_PATRONUM (list->first);
	if (!(list->first->right)) {
		//if (PRINT) printf("expr_parser: tree has only ROOT node! returning\n");
		return list->first;
	}
	EXPECTO_PATRONUM (list->first->right);
	EXPECTO_PATRONUM (list->first->right->right);

	while (list->first->right) {
		list->current = list->first;
		while (list->current->right) {
			//prochazeni 
			if (is_triplet(list))
				form_triplet(list);
			else
				list->current = list->current->right;
		}
	}
	return list->first;
}
/**
 * cisti stromek od zdvojenych nodu s typem AST_RETURN
 * rekurzivne lajkabaus
 * @param  radix radix stromku
 * @return       NIX
 */
void clean_tree(struct ast_node * radix)
{
	if (!radix) return;
	if (!(radix->left) && !(radix->right)) return;
	if (radix->left->type == AST_RETURN) {
		*(radix->left) = *(radix->left->left);
	}
	if ((radix->right) && (radix->right->type == AST_RETURN)) {
		*(radix->right) = *(radix->right->left);
	}
	if (radix->left) clean_tree(radix->left);
	if (radix->right) clean_tree(radix->right);
	return;
}
