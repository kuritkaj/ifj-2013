/*
    IFJ 2013

    xmarko07 - Antonín Marko
    xmahne00 - Jakub Mahnert
    xkurit00 - Jakub Kuřitka
    xkubic34 - Martin Kubíček
    xlechp00  - Pali Lech

    "Let the C language die."
 */
#include <stdbool.h>
#include "expr_parser.h"
#include "const.h"
#include "common.h"
#include "parser.h"
#include "scanner.h"
#include "ast.h"
#include "str.h"
#include "gc.h"
#include "interpret.h"

#define PRINT 1


/**
 * Funkce vrati true, pokud je soucasny token stejneho typu, jako predany argument
 *
 * @param d data*
 * @param t token_type
 *
 * @return  bool
 */
bool accept(struct data* d, enum token_type t)
{
    if ( PRINT ) printf("\tparser:accept:%d == %d, error = %d\n", d->token->type, t, d->error);
    return (d->token->type == t && d->error == E_OK);
}

bool accept_next(struct data* d, enum token_type t)
{
    if ( PRINT ) printf("\tparser:accept_next:%d == %d, error = %d\n", d->token_next->type, t, d->error);
    return (d->token_next->type == t && d->error == E_OK);
}

/**;
 * V pripade, ze je token stejneho typu jako prvi arument vraci true
 * a nacte dalsi token, jinak vraci false
 *
 * @TODO int error nahradit enumem
 * 
 * @param d     data*
 * @param t     token_type
 * @param error int
 *
 * @return      bool
 */
bool expect(struct data* d, enum token_type t, int error)
{
    if ( PRINT ) printf("\tparser:expect:%d\n", t);

    if(!accept(d, t)) {
        if ( PRINT ) printf("ERROR: %d\n", error);
        (d->error) = error;
        return false;
    }

    EXPECTO_PATRONUM( get_next_token(d) )
    
    if ( PRINT ) printf("parser:expect:after:get_next_token\n");

    return true;
}

bool expect_next(struct data* d, enum token_type t, int error)
{
    if ( PRINT ) printf("\tparser:expect_next:%d\n", t);

    if(!accept_next(d, t)){
        if ( PRINT ) printf("ERROR: %d\n", error);
        (d->error) = error;
        return false;
    }

    EXPECTO_PATRONUM( get_next_token(d) )

    return true;
}

bool token_empty(struct data* d)
{
    if ( PRINT ) printf("\tparser:token_empty\n");

    return accept(d, T_THE_END);
}

/* Funkce pro kontrolu jednotlivych tokenu.
 * U nich neni potreba komentare.
 */

bool equals_sign(struct data* d)
{
    if ( PRINT ) printf("\tparser:equals_sign\n");
    return expect(d, T_ASSIGN, E_SYN);
}

bool semicolon(struct data* d)
{
    if ( PRINT ) printf("\tparser:semicolon\n");
    return expect(d, T_SEMICOLON, E_SYN);
}

bool comma(struct data* d)
{
    if ( PRINT ) printf("\tparser:comma\n");
    return expect(d, T_COMMA, E_SYN);
}

bool lpar(struct data* d)
{
    if ( PRINT ) printf("\tparser:lpar\n");
    return expect(d, T_LPAR, E_SYN);
}

bool rpar(struct data* d)
{
    if ( PRINT ) printf("\tparser:rpar\n");
    return expect(d, T_RPAR, E_SYN);
}

bool rbrace(struct data* d)
{
    if ( PRINT ) printf("\tparser:rbrace\n");
    return expect(d, T_RBRACE, E_SYN);
}

bool lbrace(struct data* d)
{
    if ( PRINT ) printf("\tparser:lbrace\n");
    return expect(d, T_LBRACE, E_SYN);
}

bool keyword_if(struct data* d)
{
    if ( PRINT ) printf("\tparser:keyword_if\n");
    return expect(d, T_IF, E_SYN);
}

bool keyword_else(struct data* d)
{
    if ( PRINT ) printf("\tparser:keyword_else\n");
    return expect(d, T_ELSE, E_SYN);
}

bool keyword_while(struct data* d)
{
    if ( PRINT ) printf("\tparser:keyword_while\n");
    return expect(d, T_WHILE, E_SYN);
}

bool keyword_return(struct data* d)
{
    if ( PRINT ) printf("\tparser:keyword_return\n");
    return expect(d, T_RETURN, E_SYN);
}

bool keyword_function(struct data* d)
{
    if ( PRINT ) printf("\tparser:keyword_function\n");
    return expect(d, T_FUNCTION, E_SYN);
}

/* samotne parsovani */

/**
 * Sparsuje id
 * 
 * @param  d
 * @param  id
 * 
 * @return bool
 */
bool id(struct data* d, string* id)
{
    if(accept(d, T_ID)) {
        *id = *(d->token->d.string_data); // viz var
        if ( PRINT ) printf("\tparser:id:%s\n", id->str);
    }

    EXPECTO_PATRONUM( expect(d, T_ID, E_SYN) );

    return true;
}

/**
 * Sparsuje promennou
 * 
 * @param  d
 * @param  var_name
 * 
 * @return bool
 */
bool var(struct data* d, string* var_name)
{
    if(accept(d, T_VAR)) {
        *var_name = *(d->token->d.string_data); // zkopirujeme hodnotu, protoze hodnota dat tokenu se meni pri kazdem zavolani
        if ( PRINT ) printf("\tparser:var:%s\n", var_name->str);
    }

    EXPECTO_PATRONUM( expect(d, T_VAR, E_SYN) )

    return true;
}

/**
 * To bude Kubovo, jeho black magic
 * dajaky kokot mi to tu premazal, to mate radne v pici chalani koukam
 * nevadi, si to dopisu sam znova zejo
 * ne ze bych mel dost jiny prace nebo tak
 * ;)
 */
bool expr(struct data* d, struct ast_node* expression)
{
    struct ast_node * temp;
    struct ast_node * kokomango = ast_create_node();
    if ( PRINT ) printf(CYAN "parser:expr\n" RESET);
    if(d->token->type == T_ID || d->token->type == T_STRING) {
        if ( PRINT ) printf("parser:expr: token is %s\n" RESET, d->token->d.string_data->str);
    } else if(d->token->type == T_NUMBER){
        if ( PRINT ) printf(CYAN "parser:expr: token is %g\n" RESET, d->token->d.numeric_data);
    }
    //skipovani stredniku
    if ((d->token->type == T_SEMICOLON) || (d->token->type == T_COMMA)) {
        if ( PRINT ) printf(CYAN "parser:expr: skipping token %s\n" RESET, d->token->d.string_data->str);
        EXPECTO_PATRONUM (get_next_token(d));
        expression = NULL;
        return true;
    }
    //jestli vyraz pokracuje po rovnitku
    int * continues = (int *) gc_malloc(sizeof(int));
    *continues = 0;
    //parsovani stringu/kokotenace
    if ((d->token->type == T_STRING) || (d->token_next->type == T_STRING) || ((d->token->type == T_VAR) && (d->token_next->type == T_CONCATENATE))) {
        if ( PRINT ) printf(CYAN "parser:expr:STRING\n" RESET);
        temp = parse_concatenation(d);
        EXPECTO_PATRONUM (temp);
        *expression = *temp;
        return true;
    }
    //parsovani booleanu
    if ((d->token->type == T_TRUE) || (d->token->type == T_FALSE) || (d->token->type == T_NULL)) {
        if ( PRINT ) printf(CYAN "parser:expr:parsing boolean\n" RESET);
        temp = parse_boolean(d);
        EXPECTO_PATRONUM (temp);
        *expression = *temp;
        return true;
    }

    //parsovani ciselnych vyrazu
    if ( PRINT ) printf(CYAN "parser:expr begin\n" RESET);
    struct expr_list * post_expr = gc_malloc(sizeof(struct expr_list));
    //vytvoreni seznamu s postfixovou notaci vyrazu
    struct expr_list * post_ptr = infix2postfix(d, continues);
    EXPECTO_PATRONUM (post_ptr);
    *post_expr = *(post_ptr);

    //postaveni stromu ze seznamu
    if ( PRINT ) printf(CYAN "parser: building tree\n" RESET);
    temp = list2ast_tree(post_expr);
    EXPECTO_PATRONUM (temp);
    *expression = *temp;
    if ( PRINT ) printf(CYAN "parser: tree built\n" RESET);

    //cisteni stromu
    clean_tree(expression);
    while (expression->type == AST_RETURN) {
        //asi nechapu jak funguje Ccko tyvole kdyz tohle funguje
        *(expression) = *(expression->left);
    }
    while (*continues) {
        *continues = 0;
        if ( PRINT ) printf(CYAN "parser: expr continues!!\n" RESET);
        //soucasny token je vnitrni vyrazovy delimiter (equals, assign a podobne)
        //je tedy nutno tuhle operaci do stromu zaradit na vrsek a pokracovat s prasovanim
        struct ast_node * new_radix = new_op_node(d->token->d.string_data);
        if ( PRINT ) printf(CYAN "parser:expr:DELIMITER WAS %d\n" RESET, new_radix->d.binary);
        new_radix->left = ast_create_node();
        new_radix->right = ast_create_node();
        *(new_radix->left) = *expression;
        get_next_token(d);

        //po delimiteru je konkatenace
        if ((d->token->type == T_STRING) || (d->token_next->type == T_STRING) || ((d->token->type == T_VAR) && (d->token->type == T_CONCATENATE))) {
            if ( PRINT ) printf(CYAN "parser:expr:STRING OR VAR WITH CONCAT AFTER DELIMITER\n" RESET);
            temp = parse_concatenation(d);
            EXPECTO_PATRONUM (temp);
            new_radix->right = temp;
            *expression = *new_radix;
        //po delimiteru je boolean
        } else if ((d->token->type == T_TRUE) || (d->token->type == T_FALSE) || (d->token->type == T_NULL)) {
            if ( PRINT ) printf(CYAN "parser:expr:BOOLEAN AFTER DELIMITER\n" RESET);
            temp = parse_boolean(d);
            EXPECTO_PATRONUM (temp);
            new_radix->right = temp;
            *expression = *new_radix;
        //po delimiteru je ciselny vyraz
        } else {
            if ( PRINT ) printf(CYAN "parser:expr:EXPR AFTER DELIMITER\n" RESET);
            post_ptr = infix2postfix(d, continues);
            EXPECTO_PATRONUM (post_ptr);
            *post_expr = *(post_ptr);
            temp = list2ast_tree(post_expr);
            EXPECTO_PATRONUM (temp);
            *kokomango = *temp;
            EXPECTO_PATRONUM (kokomango);
            clean_tree(kokomango);
            //eliminace zdvojeni return nodem v koreni
            if (kokomango->type == AST_RETURN)
                *kokomango = *(kokomango->left);
            *(new_radix->right) = *kokomango;
            *expression = *new_radix;
        }
    }
    if ( PRINT ) printf(CYAN "parser: expression is OVER! AND WE LIKE IT\n" RESET);
    if ( PRINT ) printf(CYAN "parser:expr is of type %d\n" RESET, expression->type);
    if ( PRINT ) printf(CYAN "parser:current token is of type %d\n" RESET, d->token->type);
    if (expression->type == 9) {
        if ( PRINT ) printf(CYAN "parser:expr is of BINOP type %d\n" RESET, expression->d.binary);
        if ( PRINT ) printf(CYAN "parser:expr left son is %d\n" RESET, expression->left->type);
        if ( PRINT ) printf(CYAN "parser:expr left son is %d\n" RESET, expression->right->type);
    }
    return true;
}

/**
 * Zparsuje prirazeni
 * <id> = <expression> ;
 *
 * @param d
 *
 * @return  bool
 */
bool assign(struct data* d, struct ast_node* assign)
{
    if ( PRINT ) printf("\tparser:assign\n");

    string* var_name = new_str(NULL);
    struct ast_node* expression = ast_create_node();

    ALLOC( var_name, false )
    ALLOC( expression, false )

    EXPECTO_PATRONUM( var(d, var_name) )
    EXPECTO_PATRONUM( equals_sign(d) )
    //promin jakubiku, dopisuju ja, Kubik
    if (d->token->type == T_ID) {
        EXPECTO_PATRONUM( fn_call(d, expression) )
    } else {
        EXPECTO_PATRONUM( expr(d, expression) )
        if ( PRINT ) printf("\tparser:assign:expression:%p\n", (void*)expression);
    }
    EXPECTO_NOT_ERRORUM( d )

    EXPECTO_PATRONUM( semicolon(d) )

    struct ast_node* temp = make_assign(var_name, expression);

    ALLOC( temp, false )

    *assign = *temp;

    return true;
}

/**
 * Sparsuje argumenty fce
 *
 * ([<var>,])
 * 
 * @param  d
 * @param  arguments
 * 
 * @return bool
 */
bool fn_arguments(struct data* d, struct ast_node* arguments)
{
    if ( PRINT ) printf("\tparser:fn_arguments\n");

    arguments->d.list = ast_create_list();

    ALLOC( arguments->d.list, false ) 

    EXPECTO_PATRONUM( lpar(d) )

    string* var_name; //temp  
    
    while(!accept(d, T_RPAR)){

        if ( PRINT ) printf("\tparser:fn_arguments:while\n");

        var_name = new_str(NULL); // hodnota str se kopiruje, nekopiruje se pointer 

        ALLOC( var_name, false )
        
        //argument je $promenna
        EXPECTO_PATRONUM( var(d, var_name) )
        
        //vlozime ho do stromu argumentu
        ast_list_insert(arguments->d.list, make_var(var_name));
        
        // pokud dalsi neni ) ocekavame carku a dalsi argument
        if(!accept(d, T_RPAR)){
            EXPECTO_PATRONUM( comma(d) )
            //EXPECTO_PATRONUM( get_next_token(d) )
        }
    }   

    EXPECTO_PATRONUM( rpar(d) )

    return true;
}

/**
 * Sparsuje definici fce
 *
 * <id>([<var>,]){
 *     [statement]
 * }
 * 
 * @param  d
 * 
 * @return bool
 */
bool fn_definition(struct data* d, struct ast_node* definition)
{
    if ( PRINT ) printf("\tparser:fn_definition\n");

    string* fn_name = new_str(NULL);
    struct ast_node* arguments = ast_create_node();
    struct ast_node* body      = ast_create_node();

    ALLOC( fn_name, false )
    ALLOC( arguments, false )
    ALLOC( body, false )

    EXPECTO_PATRONUM( keyword_function(d) )
    EXPECTO_PATRONUM( id(d, fn_name) )
    EXPECTO_PATRONUM( fn_arguments(d, arguments) )
    EXPECTO_PATRONUM( lbrace(d) )
    EXPECTO_PATRONUM( statement(d, body, false) )
    EXPECTO_PATRONUM( rbrace(d) )

    struct ast_node* temp = make_fn_definition(fn_name, arguments, body);

    ALLOC( temp, false )

    *definition = *temp;

    return true;
}

/**
 * while(<expression>){
 *     <statement>
 * }
 * 
 * @param  d
 * @param  _while
 * 
 * @return bool      
 */
bool _while(struct data* d, struct ast_node* _while)
{
    if ( PRINT ) printf("\tparser:_while\n");

    struct ast_node* condition = ast_create_node();
    struct ast_node* body      = ast_create_node();

    ALLOC( condition, false )
    ALLOC( body, false )

    EXPECTO_PATRONUM( keyword_while(d) )
    EXPECTO_PATRONUM( lpar(d) )
    EXPECTO_PATRONUM( expr(d, condition) )
    EXPECTO_NOT_ERRORUM( d )
    EXPECTO_PATRONUM( rpar(d))
    EXPECTO_PATRONUM( lbrace(d) )
    EXPECTO_PATRONUM( statement(d, body, false) )
    EXPECTO_PATRONUM( rbrace(d) )
    
    struct ast_node* temp = make_while(condition, body);

    ALLOC( temp, false )

    *_while = *temp;

    return true;
}

/**
 * if(<condition>){
 *     <statement>
 * } else {
 *     <statement>
 * }
 * 
 * @param  d
 * @param  _if
 * 
 * @return bool
 */
bool _if(struct data* d, struct ast_node* _if)
{
    if ( PRINT ) printf("\tparser:_if\n");

    struct ast_node* condition = ast_create_node();
    struct ast_node* if_body   = ast_create_node();
    struct ast_node* else_body = ast_create_node();

    ALLOC( condition, false )
    ALLOC( if_body, false )
    ALLOC( else_body, false )

    EXPECTO_PATRONUM( keyword_if(d) )

    EXPECTO_PATRONUM( lpar(d) )
    
    EXPECTO_PATRONUM( expr(d, condition) )
    EXPECTO_NOT_ERRORUM( d )

    //vymazat
    if ( PRINT ) printf("%p\n", (void*)condition);

    print_ast(condition, 0, '<');



    EXPECTO_PATRONUM( rpar(d) )

    EXPECTO_PATRONUM( lbrace(d) )
    EXPECTO_PATRONUM( statement(d, if_body, false) )
    EXPECTO_PATRONUM( rbrace(d) )

    EXPECTO_PATRONUM( keyword_else(d) )
    EXPECTO_PATRONUM( lbrace(d) )
    EXPECTO_PATRONUM( statement(d, else_body, false) )
    EXPECTO_PATRONUM( rbrace(d) )

    struct ast_node* temp = make_if(condition, if_body, else_body);

    ALLOC( temp, false )

    *_if = *temp;

    return true;
}

/**
 * return <expression> ;
 * 
 * @param  d
 * @param  _return
 * 
 * @return bool        
 */
bool _return(struct data* d, struct ast_node* _return)
{
    if ( PRINT ) printf("\tparser:_return\n");

    struct ast_node* exprs = ast_create_node();

    ALLOC( exprs, false )

    EXPECTO_PATRONUM( keyword_return(d) )
    EXPECTO_PATRONUM( expr(d, exprs) )
    EXPECTO_PATRONUM( semicolon(d) )

    struct ast_node* temp = make_return(exprs);

    ALLOC( temp, false )

    *_return = *temp;

    return true;
}

/**
 * Sparsuje argumenty u volani fce
 *
 * ([<expression>],)
 * 
 * @param  d
 * @param  arguments
 * 
 * @return bool
 */
bool fn_call_arguments(struct data* d, struct ast_node* arguments)
{
    if ( PRINT ) printf("\tparser:fn_call_arguments\n");

    arguments->d.list = ast_create_list();

    ALLOC( arguments->d.list, false )

    EXPECTO_PATRONUM( lpar(d) )

    struct ast_node* arg;

    while(!accept(d, T_RPAR)){
        if ( PRINT ) printf("\tparser:fn_call_arguments:while\n");

        arg = ast_create_node();

        ALLOC( arg, false )
        //dalsi uprava od Kubika, promin
        if (d->token->type == T_ID) {
            EXPECTO_PATRONUM( fn_call(d, arg) )
        } else {
            EXPECTO_PATRONUM( expr(d, arg) )
        }

        EXPECTO_NOT_ERRORUM( d )
        
        //vlozime ho do stromu argumentu
        ast_list_insert(arguments->d.list, arg);
        
        // pokud dalsi neni ) ocekavame carku a dalsi argument
        if(!accept(d, T_RPAR)){
            EXPECTO_PATRONUM( comma(d) )
            //EXPECTO_PATRONUM( get_next_token(d) )
        }
    }

    EXPECTO_PATRONUM( rpar(d) )

    return true;
}

/**
 * Sparsuje volani fce
 *
 * <id>([<expression>,])
 *
 * [TODO: problem stredniku]
 * 
 * @param  d
 * @param  fn_call
 * 
 * @return bool
 */
bool fn_call(struct data* d, struct ast_node* f_call)
{
    if ( PRINT ) printf("\tparser:fn_call\n");

    string* fn_name = new_str(NULL);
    struct ast_node* fn_args = ast_create_node();

    ALLOC( fn_name, false )
    ALLOC( fn_args, false )

    EXPECTO_PATRONUM( id(d, fn_name) )
    EXPECTO_PATRONUM( fn_call_arguments(d, fn_args) )

    if ( PRINT ) printf("\tparser:fn_call:arguments:\n");
    print_instruction_list(fn_args->d.list, 0);

    struct ast_node* temp = make_fn_call(fn_name, fn_args);

    ALLOC( temp, false )

    *f_call = *temp;

    return true;
}

/**
 * Sparsuje jeden prikaz
 * 
 * @param  d
 * @param  statement 
 * @param  in_root   jsme v tele programu ne ve fci nebo while
 * 
 * @return bool
 */
bool statement_decision(struct data* d, struct ast_node* result, bool in_root)
{
    //expression => $ffa; | $ffa + 2; | fca() + $ffa; | fca(2 + 2) + 2;
    //assing => $fafsa = 2; | $fafa = $fafasf;
    //fn declar => fce() { body } | fce($args...) { body }
    //fn call => fca(); | fca($args1, 2 + 2, ..);
    //while
    //return
    
    //struct ast_node* result;

    if ( PRINT ) printf("\tparser:statement_decision\n");

    //assignment
    if(accept(d, T_VAR)){
        if ( PRINT ) printf("\tparser:statement_decision:T_VAR\n");
        // mame T_VAR, muze nasledovat pouze assign nebo expression
        if(accept_next(d, T_ASSIGN)){
            if ( PRINT ) printf("\tparser:statement_decision:ASSIGN\n");
            EXPECTO_PATRONUM( assign(d, result) )
        } else {
            if ( PRINT ) printf("\tparser:statement_decision:EXPR\n");
            EXPECTO_PATRONUM( expr(d, result) )
            EXPECTO_NOT_ERRORUM( d )
        }

    //vytvareni fce
    } else if(accept(d, T_FUNCTION)){
        if ( PRINT ) printf("\tparser:statement_decision:T_FUNCTION\n");
        EXPECTO_PATRONUM( in_root ) // ve fci a while nesmime vytvaret dalsi fce

        EXPECTO_PATRONUM( fn_definition(d, result) )

    //while
    } else if(accept(d, T_WHILE)){
        if ( PRINT ) printf("\tparser:statement_decision:T_WHILE\n");
        
        EXPECTO_PATRONUM( _while(d, result) )
    } else if (accept(d, T_IF)) {
        if ( PRINT ) printf("\tparser:statement_decision:T_IF\n");

        EXPECTO_PATRONUM( _if(d, result) )
    //return
    } else if(accept(d, T_RETURN)){
        if ( PRINT ) printf("\tparser:statement_decision:T_RETURN\n");

        EXPECTO_PATRONUM( _return(d, result) )

    //fn call [To asi bude muset delat kuba, protoze to muze byt soucasti expression]
    } else if(accept(d, T_ID) && accept_next(d, T_LPAR)){
        if ( PRINT ) printf("\tparser:statement_decision:FN CALL\n");
        fn_call(d, result);
        //EXPECTO_PATRONUM( expect(d, T_SEMICOLON, E_SYN) )
    } else if(accept(d, T_SEMICOLON)){
        get_next_token(d);
        if ( PRINT ) printf("\tparser:statement_decision:SEMICOLON\n");
        return true;

    } else if (token_empty(d)) {
        if ( PRINT ) printf("\tparser:statement_decision:T_THE_END\n");
        return true;

    } else {
        if ( PRINT ) printf("\tparser:statement_decision:EXPR\n");

        EXPECTO_PATRONUM( expr(d, result) )
        EXPECTO_NOT_ERRORUM( d )
    }

    return true;
}

/**
 * Sparsuje jakykoliv prikaz
 * 
 * @param  d
 * @param  statement 
 * @param  in_root   jsme v tele programu ne ve fci nebo while
 * 
 * @return bool
 */
bool statement(struct data* d, struct ast_node* statement, bool in_root)
{
    if ( PRINT ) printf("\tparser:statement\n");

    statement->d.list = ast_create_list();

    ALLOC( statement->d.list, false )

    if ( PRINT ) printf("\tparser:after:ast_create_list\n");

    struct ast_node* s;

    while(true){
        // konec programu
        if(in_root && token_empty(d)){
            break;
        } else if(!in_root && token_empty(d)){
            if ( PRINT ) printf("ERROR: %d\n", E_SYN);
            (d->error) = E_SYN;
            return false;
        // konec bloku
        } else if(!in_root && accept(d, T_RBRACE)){
            break;
        }

        s = ast_create_node();

        ALLOC( s, false )

        EXPECTO_PATRONUM( statement_decision(d, s, in_root) )

        if ( PRINT ) printf("\tparser:statement_decision:before check for empty statement\n");
        // prazdny statement ignorujeme
        if(s->type == AST_STATEMENT && s->d.list == NULL && !in_root) continue;
        if(s->type == AST_STATEMENT && s->d.list != NULL && s->d.list->elem == NULL && !in_root) continue;

        if ( PRINT ) printf("\tparser:statement:node to insert:\n");
        print_ast(s, 0, '<');
        //ast_node_print(s);

        ast_list_insert(statement->d.list, s);

        if(token_empty(d)) break;
    }

    return true;
}

/**
 * Sparsuje telo programu a ulozi ho do stromu
 * 
 * @param  d
 * @param  tree
 * 
 * @return bool
 */
bool program_body(struct data* d, struct ast_node* tree)
{
    if ( PRINT ) printf("\tparser:program_body\n");

    ALLOC( tree, false )

    EXPECTO_PATRONUM( get_next_token(d) )

    EXPECTO_PATRONUM( statement(d, tree, true) )

    return true;
}

/*
    Run and init
 */

void parser_init(struct data* d)
{
    d->tree = ast_create_node();
    //tady se bude volat i scanner_init, kdyby nejaky existoval
}

bool parser_run(struct data* d)
{
    ALLOC( d->tree, false )

    EXPECTO_PATRONUM( program_body(d, d->tree) )

    return true;
}

/*
    MAKE
 */

/**
 * Vytvori node pro promennou - jen tu cast se jmenem
 * 
 * @param  var_name
 * 
 * @return ast_node*
 */
struct ast_node* make_var(string* var_name)
{
    if ( PRINT ) printf("\tparser:make_var:$%s\n", var_name->str);

    struct ast_node* n = ast_create_node();

    ALLOC( n, NULL )

    n->type          = AST_VAR;
    n->d.string_data = var_name;

    return n;
}

/**
 * Vytvori node pro prirazeni hodnoty do promenne
 * 
 * @param  var_name
 * @param  expression
 * 
 * @return ast_node*
 */
struct ast_node* make_assign(string* var_name, struct ast_node* expression)
{
    if ( PRINT ) printf("\tparser:make_assign:%s = %p\n", var_name->str, (void*)expression);

    struct ast_node* n = ast_create_node();

    ALLOC( n, NULL )

    n->type  = AST_ASSIGN;
    n->left  = make_var(var_name);
    n->right = expression;

    return n;
}


/**
 * Vytvori fci
 * 
 * @param  name
 * @param  arguments
 * @param  body
 * 
 * @return ast_node*
 */
struct ast_node* make_fn_definition(string* name, struct ast_node* arguments, struct ast_node* body)
{
    if ( PRINT ) printf("\tparser:make_fn_definition:%s(%p){ %p }\n", name->str, (void*)arguments, (void*)body);
    //[TODO] nazev fce davat do tabulky symbolu

    struct ast_node* n = ast_create_node();

    ALLOC( n, NULL )

    n->type          = AST_FUNCTION;
    n->d.string_data = name;
    n->left          = arguments;
    n->right         = body;

    return n;
}

/**
 * Vytvori while
 * 
 * @param  condition
 * @param  body
 * 
 * @return ast_node*
 */
struct ast_node* make_while(struct ast_node* condition, struct ast_node* body)
{
    if ( PRINT ) printf("\tparser:make_while:while(%p){ %p }\n", (void*)condition, (void*)body);

    struct ast_node* n = ast_create_node();

    ALLOC( n, NULL )

    n->type  = AST_WHILE;
    n->left  = condition;
    n->right = body;

    return n;
}

/**
 * Vytvori if
 *
 * @param  condition
 * @param  _if
 * @param  _else
 * 
 * @return ast_node*
 */
struct ast_node* make_if(struct ast_node* condition, struct ast_node* if_body, struct ast_node* else_body)
{
    if ( PRINT ) printf("\tparser:make_if:if(%p){ %p } else  { %p }\n", (void*)condition, (void*)if_body, (void*)else_body);

    struct ast_node* n = ast_create_node();

    ALLOC( n, NULL )

    n->type        = AST_IF;
    n->d.condition = condition;
    n->left        = if_body;
    n->right       = else_body;

    return n;
}


/**
 * Vytvori return
 * 
 * @param  exprt
 * 
 * @return ast_node*
 */
struct ast_node* make_return(struct ast_node* expr)
{
    if ( PRINT ) printf("\tparser:make_return:return %p\n", (void*)expr);

    struct ast_node* n = ast_create_node();

    ALLOC( n, NULL )

    n->type = AST_RETURN;
    n->left = expr;

    return n;
}

/**
 * Vytvori volani fce
 * 
 * @param  name
 * @param  args
 * 
 * @return bool
 */
struct ast_node* make_fn_call(string* name, struct ast_node* args)
{
    if ( PRINT ) printf("\tparser:make_fn_call:%s(%p);\n", name->str, (void*)args);

    struct ast_node* n = ast_create_node();

    ALLOC( n, NULL )

    n->d.string_data = name;

    n->type  = AST_CALL;
    n->right = args;

    ast_list_print(args->d.list);

    return n;
}
