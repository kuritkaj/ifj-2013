/*
    IFJ 2013

    xmarko07 - Antonín Marko
    xmahne00 - Jakub Mahnert
    xkurit00 - Jakub Kuřitka
    xkubic34 - Martin Kubíček
    xlechp00  - Pali Lech

    "Let the C language die."
 */

#ifndef IAL_H
#define IAL_H

int * build_pattern_table(string str);
int find_string(string * field, string * pattern);
string* sort_string(string* source);
void merge_sort(string * array, string * aux, int left, int right);
void merge(string * array, string * aux, int left, int right);

#endif
