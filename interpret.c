#include <stdio.h>		// klasika
#include <stdlib.h>		// klasika
#include "ast.h"		// cely abstraktni syntakticky strom
#include <stdbool.h>	// bool promenne a hodnoty
#include "interpret.h"	// hlavicky
#include "common.h"		// potrebne struktury
#include "s_table.h"	// tabulka symbolu
#include "str.h"		// struktura string*
#include <string.h>		// strcmp
#include "library.h"	// vestavenne funkce
#include "ial.h"		// find_string
#include "const.h"		// chybove hlasky
#include "gc.h"			// gc_malloc
#include <ctype.h>		// isdigit

#define ERROR(d) if(d->error) { return false; }
#define C_Y "\x1b[33m"
#define C_R "\x1b[0m"

#define PRINT 0

char* encode_name(char* name, int scope)
{
	int name_len = strlen(name);
	int scope_len = (scope / 10) + 1;

	char* out = gc_malloc(sizeof(char)*(name_len + scope_len + 2));
	sprintf(out, "%s#%d", name, scope);
	
	return out;
}
char* decode_name(char* name, int* scope)
{
	string* decode = NULL;

	char cur;
	int i = 0;
	int state = 0;

	while((cur = name[i]) != '\0'){
		if(cur == '#'){
			state = 1;
			i++;
			continue;
		}
		else {
			if(state == 0){
				add_char(decode, cur);
			}
			else {
				if(isdigit(cur)){
					*scope = (*scope * 10) + ((int)cur - '0');
				}
				else
					break;
			}
		}

		i++;	
	}	

	return decode->str;
}

void dispose_scope(struct data* d, struct symbol_table* item, int scope)
{
	if(item) {
		//rekurzivne prochaze dokud se nenarazi na potrebny scope a ten se odstrani
		if(item->left) { 
			if(PRINT) printf("dispose_scope:deleting...left\n");

			dispose_scope(d, item->left, scope);
		}

		int scp;
		char* decoded = decode_name(item->key, &scp);
		if(PRINT) printf("dispose_scope: %s %d\n", decoded, scp);	

		if(scp == scope) {
			if(PRINT) printf("dispose_scope:scopes are equals\n");
			symbol_table_delete(&d->vars, item->key);

			if(!symbol_search(d->vars, item->key)){
				if(PRINT) printf("dispose_scope deleted\n");
			}
		}

		if(item->right)	{
			if(PRINT) printf("dispose_scope:deleting...right\n");
		
			dispose_scope(d, item->right, scope);
		}
	}
}
/* DOPLNUJICI POMOCNE FUNKCE */
struct symbol_table fill_variable(struct symbol_table* var)
{
	if(PRINT) printf(C_Y "interpret:fill_variable:start\n" C_R);
	struct symbol_table s_table;

	//if(strcmp(s_table.key, var->key) == 0)
	s_table.var_type = var->var_type;
	s_table.d.numeric_data = var->d.numeric_data;
	s_table.d.literal_type = var->d.literal_type;
	if(var->d.string_data == NULL)
		s_table.d.string_data = new_str(var->d.string_data->str);
	else
		s_table.d.string_data = NULL;


	if(PRINT) printf(C_Y "interpret:fill_variable:returning var\n" C_R);
	return s_table;
}
char* get_literal_type_string(int type)
{
	if(type >= 0 && type < 5){
		char* types[5] = { "AST_LITERAL_FALSE", "AST_LITERAL_TRUE", "AST_LITERAL_NULL", "AST_LITERAL_NUMERIC", "AST_LITERAL_STRING" };
		return types[type];
	}
	
	return "not literal";
}
char* get_var_type_string(int type)
{
	if(type >= 0 && type < 5){
		char* types[5] = { "AST_VAR_INT", "AST_VAR_DOUBLE", "AST_VAR_STRING", "AST_VAR_BOOLEAN", "AST_VAR_NULL" };
		return types[type];
	}

	return "not var";
}
char* get_op_type_string(int type)
{
	if(type >= 0 && type < 10){
		char* types[10] = { "AST_STATEMENT", "AST_ASSIGN", "AST_WHILE", "AST_RETURN", "AST_LITERAL", "AST_IF", "AST_FUNCTION", "AST_CALL", "AST_VAR", "AST_BINARY_OP" }; 
		return types[type];
	}
	
	return "not valid operation";
}
int ast_is_bool(enum ast_literal_type x)
{
	return (x == AST_LITERAL_TRUE || x == AST_LITERAL_FALSE);
}
int ast_is_numeric(enum ast_literal_type x)
{
	return (x == AST_LITERAL_NUMERIC);
}
int ast_is_string(enum ast_literal_type x)
{
	return (x == AST_LITERAL_STRING);
}
int ast_is_null(enum ast_literal_type x)
{
	return (x == AST_LITERAL_NULL);
}
int ast_is_strict_equals(struct ast_node* l, struct ast_node* r)
{
	//oba uzly jsou promenne
	if(l->type == AST_LITERAL && r->type == AST_LITERAL) {
		if (ast_is_bool(l->literal) == ast_is_bool(r->literal)) return true;
		else if(ast_is_null(l->literal) == ast_is_null(r->literal)) return true;
		else if(ast_is_numeric(l->literal) == ast_is_numeric(r->literal)) return true;	
		else if(ast_is_string(l->literal) == ast_is_string(r->literal)) return true;
		else return false;
	}
	else
		return false;
}
void print_var(struct symbol_table* var, char* title)
{
	if(PRINT){
		printf("interpret:print_var:%s\n", title);
		
		printf("\t%s\n", var->key);
		printf("\t%s\n", get_var_type_string(var->var_type));
		printf("\t%g\n", var->d.numeric_data);
		printf("\t%s\n", (var->d.string_data)?var->d.string_data->str:"NULL");
		printf("\t%s\n", get_literal_type_string(var->d.literal_type));
		
		printf("interpret:print_var: end\n");
	}
}
void print_ast(struct ast_node* tree, int lvl, char dir)
{
	if(PRINT){
		if(!tree){
			if(PRINT) printf("INTERPRET:PRINT_AST:TREE == NULL!!!\n");
			return;
		}

		if(tree->left != NULL) print_ast(tree->left, lvl + 1, 'L');

		for(int i = 0; i < lvl; i++) printf("  ");
		printf("%c ", dir);
		
		switch(tree->type){
			case AST_STATEMENT:
				//printf(C_Y "[ STATEMENT ]\n" C_R); 
				print_param_list(tree->d.list, lvl);
				break;
			case AST_ASSIGN:
				printf(C_Y "[ ASSIGN ]\n" C_R); 
				break;
			case AST_WHILE:
				printf(C_Y "[ WHILE ]\n" C_R); 
				break;
			case AST_RETURN:
				printf(C_Y "[ RETURN ]\n" C_R); 
				break;
			case AST_LITERAL:
				switch(tree->literal){
					case AST_LITERAL_NULL:
						printf(C_Y "[ LITERAL | NULL ]\n" C_R);
						break;
					case AST_LITERAL_TRUE:
						printf(C_Y "[ LITERAL | TRUE ]\n" C_R);
						break;	
					case AST_LITERAL_FALSE:
						printf(C_Y "[ LITERAL | FALSE ]\n" C_R);
						break;
					case AST_LITERAL_NUMERIC:
						printf(C_Y "[ LITERAL | %g ]\n" C_R, tree->d.numeric_data);
						break;
	 				case AST_LITERAL_STRING:
						printf(C_Y "[ LITERAL | %s ]\n" C_R, tree->d.string_data->str);
						break;
				}
				break;
			case AST_IF:
				printf(C_Y "[ IF ]\n" C_R); 
				break;
		 	case AST_FUNCTION:
				printf(C_Y "[ FUNCTION | %s ]\n" C_R, tree->d.string_data->str); 
				break;
			case AST_CALL:
				printf(C_Y "[ CALL | function %s ]\n" C_R, tree->d.string_data->str); 
				print_param_list(tree->right->d.list, lvl + 2);
				break;
			case AST_VAR:
				printf(C_Y "[ VAR | %s ]\n" C_R, tree->d.string_data->str); 
				break;
			case AST_BINARY_OP:
				switch(tree->d.binary){
					case AST_BINARY_MINUS: //0
						printf(C_Y "[ BIN OP | - ]\n" C_R); 
						break;
					case AST_BINARY_PLUS: //1
						printf(C_Y "[ BIN OP | + ]\n" C_R); 
						break;
					case AST_BINARY_TIMES: //2
						printf(C_Y "[ BIN OP | * ]\n" C_R); 
						break;
					case AST_BINARY_DIVIDE: //3
						printf(C_Y "[ BIN OP | / ]\n" C_R); 
						break;
					case AST_BINARY_LESS: //4
						printf(C_Y "[ BIN OP | < ]\n" C_R); 
						break;
					case AST_BINARY_MORE: //5
						printf(C_Y "[ BIN OP | > ]\n" C_R); 
						break;
					case AST_BINARY_EQUALS: //6
						printf(C_Y "[ BIN OP | == ]\n" C_R); 
						break;
					case AST_BINARY_NOT_EQUALS: //7
						printf(C_Y "[ BIN OP | != ]\n" C_R); 
						break;
					case AST_BINARY_LESS_EQUALS: //8
						printf(C_Y "[ BIN OP | <= ]\n" C_R); 
						break;
					case AST_BINARY_MORE_EQUALS: //9
						printf(C_Y "[ BIN OP | >= ]\n" C_R); 
						break;
					case AST_BINARY_STRICT_EQUALS: //10
						printf(C_Y "[ BIN OP | === ]\n" C_R); 
						break;
					case AST_BINARY_STRICT_NOT_EQUALS: //11
						printf(C_Y "[ BIN OP | !== ]\n" C_R); 
						break;
					case AST_BINARY_STRING_CONCATENATION: //12
						printf(C_Y "[ BIN OP | . ]\n" C_R); 
						break;
				}
				break;
			default: {
				printf(C_Y "[ UNDEF ]\n" C_R);
				break;
			}
		}
		
		if(tree->right != NULL) print_ast(tree->right, lvl + 1, 'R');		
	}
}
void print_param_list(struct ast_list* list, int l)
{
	if(PRINT){
		//printf(C_Y "interpret:print_param_list:start\n" C_R);
		struct ast_list* cur = list;
		//printf(C_Y "interpret:print_param_list:cur = %p\n" C_R, (void*)cur);

		while(cur != NULL && cur->elem != NULL) {
			if(cur->elem){
				for(int i = 0; i < l; i++) printf("  ");
				
				//printf(C_Y "interpret:print_param_list:cur %p\n" C_R, (void*)cur);
				//printf(C_Y "interpret:print_param_list:cur->elem = %p\n" C_R, (void*)cur->elem);
				printf(C_Y "-{ %s }-\n" C_R, get_op_type_string(cur->elem->type));

				//print_ast(cur->elem, 0, '<');
				
				if(cur->next) {
					cur = cur->next;
				}
				else
					break;
			}
			else
				break;
		}
		//printf(C_Y "interpret:print_param_list:end\n" C_R);
	}
}
void print_tree(struct symbol_table* root, int lvl, char dir)
{
	if(PRINT){
		if(root){
			if(root->left) print_tree(root->left, lvl + 1, 'L');

			for(int i = 0; i < lvl; i++) printf("  ");

			printf(C_Y "%c:[ %p | \"%s\" | ( %.2f | \"%s\" | %s ) ]\n" C_R, dir, (void*)(root), root->key, root->d.numeric_data, 
			(root->d.string_data)?root->d.string_data->str:NULL, get_literal_type_string(root->d.literal_type));
		

			if(root->right)	print_tree(root->right, lvl + 1, 'R');
		}
	}
}
void print_instruction_list(struct ast_list* zero_lvl, int l)
{
	//if(PRINT) {
		struct ast_list* lvl = zero_lvl;
		
		int counter = 1;

		while(lvl != NULL && lvl->elem != NULL){		
			print_ast(lvl->elem, l, counter + '0');
			lvl = lvl->next;
			//printf("\n");
			counter++;
		}
	//}
}
int is_logic_op(struct ast_node* nd)
{
	if(PRINT) printf(C_Y "interpret:is_logic_op\n" C_R);
	
	if(nd->type != AST_BINARY_OP)
		return false;
	
	switch(nd->d.binary){
		case AST_BINARY_EQUALS:
		case AST_BINARY_NOT_EQUALS:
		case AST_BINARY_LESS:
		case AST_BINARY_LESS_EQUALS:
		case AST_BINARY_MORE:
		case AST_BINARY_MORE_EQUALS:
		case AST_BINARY_STRICT_EQUALS:
		case AST_BINARY_STRICT_NOT_EQUALS:
			if(PRINT) printf(C_Y "interpret:is_logic_op:TRUE\n" C_R);
			return true;
		default:
			if(PRINT) printf(C_Y "interpret:is_logic_op:FALSE\n" C_R);
			return false;
	}
	
	return false;
}
int is_math_op(struct ast_node* nd)
{	
	if(PRINT)printf(C_Y "interpret:is_math_op\n" C_R);
	
	if(nd->type != AST_BINARY_OP)
		return false;
		
	switch(nd->d.binary){
		case AST_BINARY_PLUS:
		case AST_BINARY_MINUS:
		case AST_BINARY_TIMES:
		case AST_BINARY_DIVIDE:
			if(PRINT) printf(C_Y "interpret:is_math_op:TRUE\n" C_R);
			return true;
		default:
			if(PRINT) printf(C_Y "interpret:is_math_op:FALSE\n" C_R);
			return false;
	}
	
	return false;
}
int make_from_literal(struct data* d, struct ast_node* lit, struct symbol_table* out)
{
	if(PRINT) printf(C_Y "interpret:make_from_literal\n" C_R);
	ERROR(d);
	
	if(lit->type == AST_LITERAL) {
		if(PRINT) printf(C_Y "interpret:make_from_literal:AST_LITERAL\n" C_R);
		struct symbol_table* ret = new_symbol_table();
		
		
		switch(lit->literal){
			case AST_LITERAL_TRUE:
				ret->var_type = AST_VAR_BOOLEAN;
				ret->d.literal_type = AST_LITERAL_TRUE;
				break;
			case AST_LITERAL_FALSE:
				ret->var_type = AST_VAR_BOOLEAN;
				ret->d.literal_type = AST_LITERAL_FALSE;
				break;
			case AST_LITERAL_NUMERIC:
				ret->var_type = AST_VAR_DOUBLE;
				ret->d.numeric_data = lit->d.numeric_data;
				break;
			case AST_LITERAL_STRING:
				ret->var_type = AST_VAR_STRING;
				ret->d.string_data = new_str(lit->d.string_data->str);
				break;
			case AST_LITERAL_NULL:
				break;
		}
		
		if(PRINT) printf("interpret:make_from_literal:\nvar_type = %s\nstring = \"%s\"\nnumeric = %g\nliteral = %s\n", 
			get_var_type_string(ret->var_type),
			ret->d.string_data->str,
			ret->d.numeric_data,
			get_literal_type_string(ret->d.literal_type));
	
		out->var_type = ret->var_type;
		out->d.numeric_data = ret->d.numeric_data;
		out->d.string_data = new_str(ret->d.string_data->str);
		out->d.literal_type = ret->d.literal_type;

		return true;
	}	
	else if (lit->type == AST_VAR){
		if(PRINT) printf(C_Y "interpret:make_from_literal:AST_VAR\n" C_R);
		out = symbol_search(d->vars, lit->d.string_data->str);
		return true;
	}
	else
		return false;
}
int make_from_node(struct data* d, struct ast_node* node, struct symbol_table* out, int scope)
{
	if(PRINT) printf(C_Y "interpret:make_from_node\n" C_R);
	if(PRINT) printf(C_Y "interpret:make_from_node:error = %d\n" C_R, d->error);
	ERROR(d);

	char* name = encode_name(node->d.string_data->str, scope);	

	struct symbol_table var;
	strcmp(var.key, name);
	var.var_type = AST_VAR_NULL;
	var.d.numeric_data = 0.0;
	var.d.string_data = NULL;
	var.d.literal_type = AST_LITERAL_NULL;
	
	if(PRINT) printf(C_Y "interpret:make_from_node: new symbol table\n" C_R);	
	
	if(node->type == AST_VAR){
		// ziskani promenne z tabulky symbolu
		if(PRINT) printf(C_Y "interpret:make_from_node:AST_VAR (%s)\n" C_R, name);
				
		struct symbol_table* output = new_symbol_table();
		if((output = symbol_search(d->vars, name))) {
			print_var(output, "make_from_node ast_var OUTPUT");
		
			strcmp(out->key, output->key);
			out->var_type = output->var_type;
			out->d.numeric_data = output->d.numeric_data;
			out->d.literal_type = output->d.literal_type;
			if(output->d.string_data)
				out->d.string_data = new_str(output->d.string_data->str);
			else
				out->d.string_data = NULL;
		}
		else{
			if(PRINT) printf("interpret:make_from_node: var not found\n");
		}		
	}
	else if(node->type == AST_LITERAL){
		if(PRINT) printf(C_Y "interpret:make_from_node:AST_LITERAL\n" C_R);
		// provedeni prace nad literalem
		switch(node->literal){
			case AST_LITERAL_TRUE:
			case AST_LITERAL_FALSE:
				out->var_type = AST_VAR_BOOLEAN;
				out->d.literal_type = node->literal;
				break;
			case AST_LITERAL_NULL:
				out->var_type = AST_VAR_NULL;
				break;
			case AST_LITERAL_NUMERIC:
				out->var_type = AST_VAR_DOUBLE;
				out->d.numeric_data = node->d.numeric_data;
				break;
			case AST_LITERAL_STRING:
				out->var_type = AST_VAR_STRING;
				out->d.string_data = new_str(node->d.string_data->str);
				break;
		}
		
		strcmp(out->key, var.key);
		return true;
	}
	else if(node->type == AST_CALL){
		if(PRINT) printf(C_Y "interpret:make_from_node:AST_CALL\n" C_R);
		// zavolani funkce a vraceni vysledku
		
		struct symbol_table* fce = symbol_search(&(*(d->functions)), node->d.string_data->str);
		if(fce == NULL){
			if(PRINT) printf(C_Y "INTERPRET:MAKE_FROM_NODE:FUNC NOT FOUND"C_R " d->error = 4");
			(d->error) = 4;
			return false;
		}
		
		if(PRINT) printf(C_Y "interpret:make_from_node:volani funkce\n" C_R);
		struct symbol_table s_ret;
		if(!ast_call(d, fce->statement, &s_ret, scope))
			return false;
		ERROR(d);

		strcmp(out->key, s_ret.key);
		out->var_type = s_ret.var_type;
		out->d.numeric_data = s_ret.d.numeric_data;
		out->d.literal_type = s_ret.d.literal_type;
		if(s_ret.d.string_data)
			out->d.string_data = new_str(s_ret.d.string_data->str);
		else
			out->d.string_data = NULL;

		return true;
	}	
	else if(node->type == AST_BINARY_OP){
		if(PRINT) printf(C_Y "interpret:make_from_node:AST_BINARY_OP\n" C_R);

		if(is_logic_op(node)){
			out->var_type = AST_VAR_BOOLEAN;
			if(ast_logic_binary_op(d, node, scope))
				out->d.literal_type = AST_LITERAL_TRUE;
			else
				out->d.literal_type = AST_LITERAL_FALSE;
		}
		else if(is_math_op(node)){
			struct symbol_table ret = *new_symbol_table();
			if(!ast_binary_op(d, node, &ret, scope))
				return false;
			
			ERROR(d);
			
			out->var_type = AST_VAR_DOUBLE;
			out->d.numeric_data = ret.d.numeric_data;					
		}
		else {
			// CONCAT
			struct symbol_table cated = *new_symbol_table();
			if(!ast_concat(d, node, &cated, scope))
				return false;

			ERROR(d);

			out->var_type = AST_VAR_STRING;
			out->d.string_data = strval(d, cated);
		}			
	}
	else {
		// nepodporovana operace
		if(PRINT) printf(C_Y "INTERPRET:MAKE_FROM_NODE:NONDEFINED OP!!!!!" C_R " d->error = 13\n");
		(d->error) = 13;
		
		return false;		
	}

	return true;
}
int exec_logic_operation(struct data* d, struct symbol_table* l, struct symbol_table* r, enum ast_binary_op_type op)
{	
	ERROR(d);
	if(PRINT) printf(C_Y "interpret:exec_logic_op:start\n" C_R);
	
	if(l->var_type == r->var_type){
		if(PRINT) printf(C_Y "interpret:exec_logic_op:same vartype\n" C_R);
		//pokud jsou stejneho typu
		switch(l->var_type){
			//podle typu promenne se budou provadet operace
			case AST_VAR_BOOLEAN: {
				if(PRINT) printf(C_Y "interpret:exec_logic_op:VAR_BOOL\n" C_R);
				switch(op) {
					case AST_BINARY_EQUALS: 
						return ((l->d.literal_type == AST_LITERAL_TRUE && r->d.literal_type == AST_LITERAL_TRUE) ||
							(l->d.literal_type == AST_LITERAL_FALSE && r->d.literal_type == AST_LITERAL_FALSE));	
					case AST_BINARY_NOT_EQUALS:
						return ((l->d.literal_type == AST_LITERAL_TRUE && r->d.literal_type == AST_LITERAL_FALSE) ||
							(l->d.literal_type == AST_LITERAL_FALSE && r->d.literal_type == AST_LITERAL_TRUE));
					case AST_BINARY_LESS:
						return (l->d.literal_type == AST_LITERAL_FALSE && r->d.literal_type == AST_LITERAL_TRUE);
					case AST_BINARY_LESS_EQUALS:
						return ((l->d.literal_type == AST_LITERAL_FALSE) && 
							(r->d.literal_type == AST_LITERAL_FALSE || r->d.literal_type == AST_LITERAL_TRUE));
					case AST_BINARY_MORE: 
						return (l->d.literal_type == AST_LITERAL_TRUE && r->d.literal_type == AST_LITERAL_FALSE);
					case AST_BINARY_MORE_EQUALS:
						return ((l->d.literal_type == AST_LITERAL_TRUE) && 
							(r->d.literal_type == AST_LITERAL_FALSE || r->d.literal_type == AST_LITERAL_TRUE));
					case AST_BINARY_STRICT_EQUALS:
						return (l->d.literal_type == r->d.literal_type);
					case AST_BINARY_STRICT_NOT_EQUALS:
						return (l->d.literal_type != l->d.literal_type);
					default: break;
				}			
			}
			case AST_VAR_INT: {
				if(PRINT) printf(C_Y "interpret:exec_logic_op:VAR_INT\n" C_R);
				switch(op){
					case AST_BINARY_EQUALS:
					case AST_BINARY_STRICT_EQUALS:
						return ((int)l->d.numeric_data == (int)r->d.numeric_data);
					case AST_BINARY_NOT_EQUALS:
					case AST_BINARY_STRICT_NOT_EQUALS:
						return ((int)l->d.numeric_data != (int)r->d.numeric_data);
					case AST_BINARY_LESS:
						return ((int)l->d.numeric_data < (int)r->d.numeric_data);
					case AST_BINARY_LESS_EQUALS:
						return ((int)l->d.numeric_data <= (int)r->d.numeric_data);
					case AST_BINARY_MORE:
						return ((int)l->d.numeric_data > (int)r->d.numeric_data);
					case AST_BINARY_MORE_EQUALS:
						return ((int)l->d.numeric_data >= (int)r->d.numeric_data);
					default: break;
				}
			}
			case AST_VAR_DOUBLE: {
				if(PRINT) printf(C_Y "interpret:exec_logic_op:VAR_DBL\n" C_R);
				switch(op){
					case AST_BINARY_EQUALS:
					case AST_BINARY_STRICT_EQUALS:
						return (l->d.numeric_data == r->d.numeric_data);
					case AST_BINARY_NOT_EQUALS:
					case AST_BINARY_STRICT_NOT_EQUALS:
						return (l->d.numeric_data != r->d.numeric_data);
					case AST_BINARY_LESS:
						return (l->d.numeric_data < r->d.numeric_data);
					case AST_BINARY_LESS_EQUALS:
						return (l->d.numeric_data <= r->d.numeric_data);
					case AST_BINARY_MORE:
						return (l->d.numeric_data > r->d.numeric_data);
					case AST_BINARY_MORE_EQUALS:
						return (l->d.numeric_data >= r->d.numeric_data);
					default: break;
				}
			}
			case AST_VAR_NULL: {
				if(PRINT) printf(C_Y "interpret:exec_logic_op:VAR_NULL\n" C_R);
				switch(op){
					case AST_BINARY_EQUALS:
					case AST_BINARY_STRICT_EQUALS:
					case AST_BINARY_LESS_EQUALS:
					case AST_BINARY_MORE_EQUALS:
						return (l->d.literal_type == AST_LITERAL_NULL && r->d.literal_type == AST_LITERAL_NULL);
					case AST_BINARY_NOT_EQUALS:
					case AST_BINARY_STRICT_NOT_EQUALS:
						return (l->d.literal_type != AST_LITERAL_NULL || r->d.literal_type != AST_LITERAL_NULL);
					case AST_BINARY_LESS:
					case AST_BINARY_MORE:
						return false;
					default: break;
				}
			}
			case AST_VAR_STRING: {
				if(PRINT) printf(C_Y "interpret:exec_logic_op:VAR_STR\n" C_R);
				int compare = strcmp(l->d.string_data->str, r->d.string_data->str);
				switch(op){
					case AST_BINARY_EQUALS:
					case AST_BINARY_LESS_EQUALS:
					case AST_BINARY_MORE_EQUALS:
					case AST_BINARY_STRICT_EQUALS:
						return (compare == 0);
					case AST_BINARY_NOT_EQUALS:
					case AST_BINARY_STRICT_NOT_EQUALS:
						return (compare != 0);
					case AST_BINARY_LESS:
						return (compare < 0);
					case AST_BINARY_MORE:
						return (compare > 0);
					default: break;					
				}
			}
			default: break;
		}
	}
	else {
		//nejsou typove stejne
		//pravy operand pretypovat na typ leveho a rekurzivne porovnat
		struct symbol_table converted;
		if(PRINT) printf(C_Y "interpret:exec_logic_op:not same vartype\n" C_R);
		
		switch(l->var_type) {
			case AST_VAR_BOOLEAN: {
				if(PRINT) printf(C_Y "interpret:exec_logic_op:VAR_BOOL\n" C_R);
				int ret = boolval(d, r);
				
				converted.var_type = AST_VAR_BOOLEAN;
				if(ret == true)	
					converted.d.literal_type = AST_LITERAL_TRUE;
				else 
					converted.d.literal_type = AST_LITERAL_FALSE;
				
				return exec_logic_operation(d, l, &converted, op);			
			}
			case AST_VAR_DOUBLE: {
				if(PRINT) printf(C_Y "interpret:exec_logic_op:VAR_DOUBLE\n" C_R);
				converted.var_type = AST_VAR_DOUBLE;
				converted.d.numeric_data = doubleval(d, r);
				return exec_logic_operation(d, l, &converted, op);
			}
			case AST_VAR_INT: {
				if(PRINT) printf(C_Y "interpret:exec_logic_op:VAR_INT\n" C_R);
				converted.var_type = AST_VAR_INT;
				converted.d.numeric_data = intval(d, r);
				return exec_logic_operation(d, l, &converted, op);
			}
			case AST_VAR_NULL: {
				if(PRINT) printf(C_Y "interpret:exec_logic_op:VAR_NULL\n" C_R);
				//neda se porovnavat, vracim hned 0
				return false;
			}
			case AST_VAR_STRING: {
				if(PRINT) printf(C_Y "interpret:exec_logic_op:VAR_STR\n" C_R);
				converted.var_type = AST_VAR_STRING;
				converted.d.string_data = strval(d, *r);
				
				return exec_logic_operation(d, l, &converted, op);
			}
		}
	}
	if(PRINT) printf(C_Y "interpret:exec_logic_op:end\n" C_R);
	return false;
}
int exec_var(struct symbol_table var)
{
	/*
	 * Logicke vyhodnoceni promenne
	 */
	if(PRINT) printf(C_Y "interpret:exec_var:start\n" C_R);
	
	switch(var.var_type) {
		case AST_VAR_BOOLEAN:
			if(PRINT) printf(C_Y "interpret:exec_var:BOOL\n" C_R);
			if(var.d.literal_type == AST_LITERAL_TRUE)
				return true;
			else
				return false;
			break;
		case AST_VAR_INT:
		case AST_VAR_DOUBLE:
			if(PRINT) printf(C_Y "interpret:exec_var:INT_DBL\n" C_R);
			return (var.d.numeric_data != 0);
			break;
		case AST_VAR_STRING:
			if(PRINT) printf(C_Y "interpret:exec_var:STR\n" C_R);
			return (var.d.string_data != NULL);
			break;
		case AST_VAR_NULL:
			if(PRINT) printf(C_Y "interpret:exec_var:NULL\n" C_R);
			return false;
			break;
	}	

	if(PRINT) printf(C_Y "interpret:exec_var:end\n" C_R);
	return false;
}
int is_inline_fn(char* fce, int* n)
{	
	if(PRINT) printf(C_Y "interpret:is_inline_fn:start: %s\n" C_R, fce);

	// podle nazvu funkce zavola inline funkci
	// BOOLVAL, DOUBLEVAL, INTVAL, STRVAL, PUT_STR, GET_STR, STR_LEN, GET_SUB, FIND_STR, SORT_STR
	char* inline_funkce[10] = { "boolval\0", "doubleval\0", "intval\0", "strval\0", "put_string\0", "get_string\0", "strlen\0", "get_substring\0", "find_string\0", "sort_string\0"};

	for(int i = 0; i < 10; i++){
		if(strcmp(inline_funkce[i], fce) == 0){
			*n = i;
			
			if(PRINT) printf(C_Y "interpret:is_inline_fn:found= %d\n" C_R, i);
			return true;
		}
	}
	
	*n = -1;
	if(PRINT) printf(C_Y "interpret:is_inline_fn:not_inline\n" C_R);
	return false;
}
/* 
 * JEDNOTLIVE OPERACE NAD UZLY 
 */

int ast_statement(struct data* d, struct ast_node* nd, struct symbol_table* out, int scope)
{
	if(PRINT) printf(C_Y "interpret:ast_statement:start\n" C_R);
	
	ERROR(d);
	
	if(PRINT){
		printf("interpret:ast_statement:printing statement content v\n");
		print_ast(nd->d.list->elem, 0, '<');
		printf("interpret:ast_statement:printing statement content ^\n");
	}
	//pruchod seznamem prikazu
	struct ast_list* cur = nd->d.list;
	
	
	struct symbol_table var = *new_symbol_table();
	while(cur != NULL && cur->elem != NULL) {
		if(PRINT) printf(C_Y "interpret:ast_statement:start_new_element~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n" C_R);

		if(!walk_through(d, cur->elem, &var, scope)) {
			if(PRINT) printf(C_Y "INTERPRET:ast_statement:ERROR DURING WALKTHROUGH\n" C_R);
			return false;
		}
		if(PRINT) {
			printf("interpret:walk_through:ast_statement:print retuned val\n");
			printf("interpret:walk_through:ast_statement:\nvar_type = %s\nstring = \"%s\"\nnumeric = %g\nliteral = %s\n", 
				get_var_type_string(var.var_type),
				var.d.string_data->str,
				var.d.numeric_data,
				get_literal_type_string(var.d.literal_type));	

			printf(C_Y "interpret:ast_statement:end_new_element~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n" C_R);
		}
		// presun na dalsi instrukci v seznamu
		if(cur->next != NULL)		
			cur = cur->next;	
		else
			break;
	}

	out->key = var.key;
	out->var_type = var.var_type;
	out->d.numeric_data = var.d.numeric_data;
	out->d.string_data = new_str(var.d.string_data->str);
	out->d.literal_type = var.d.literal_type;

	return true;
}
int ast_assign(struct data* d, struct ast_node* nd, struct symbol_table* out, int scope)
{
	if(PRINT) printf(C_Y "interpret:ast_assing:start\n" C_R);

	ERROR(d);

	if(!out){
		out = new_symbol_table();
	}

	struct symbol_table right_val; // = new_symbol_table();
	right_val.var_type = AST_VAR_NULL;
	right_val.d.numeric_data = 0.0;
	right_val.d.string_data = NULL;
	right_val.d.literal_type = AST_LITERAL_NULL;
	
	switch(nd->right->type)	{
		case AST_CALL: {
			if(PRINT) printf(C_Y "interpret:ast_assing:right_side=ast_call\n" C_R);

			//vpravo je volani funkce
			//x = call_function(neco)
			struct symbol_table* ret = new_symbol_table();

			if(!ast_call(d, nd->right, ret, scope)) {
				// chyba!
				if(PRINT) printf("AST_ASSIGN AST_CALL CHYBA\n");
				return false;
			}
			
			right_val = *ret;

			break;
		}
		case AST_LITERAL: {
			right_val.var_type = AST_LITERAL;
			if(PRINT) printf(C_Y "interpret:ast_assing:right_side=ast_literal\n" C_R);
			//vpravo je nejaky druh konstanty
			//x = false
			if(PRINT) printf(C_Y "interpret:ast_assign:right_side = %s\n" C_R, get_literal_type_string(nd->right->literal));
			switch(nd->right->literal) {
				//typ konstanty
				case AST_LITERAL_FALSE:
				case AST_LITERAL_TRUE: {
					if(PRINT) printf(C_Y "interpret:ast_assign:right_side:literal:false || true\n" C_R);							
			
					right_val.var_type = AST_VAR_BOOLEAN;
					right_val.d.literal_type = nd->right->literal;
					
					break;
				}
				case AST_LITERAL_NULL: {
					if(PRINT) printf(C_Y "interpret:ast_assign:right_side:literal:null\n" C_R);							
			
					right_val.var_type = AST_VAR_NULL;
					
					break;
				}								
				case AST_LITERAL_NUMERIC: {
					if(PRINT) printf(C_Y "interpret:ast_assign:right_side:literal:numeric\n" C_R);

					right_val.var_type = AST_VAR_DOUBLE;
					right_val.d.numeric_data = nd->right->d.numeric_data;
 
					if(PRINT) printf(C_Y "interpret:ast_assign:right_side:literal:numeric = %f\n" C_R, right_val.d.numeric_data);
					break;
				}
				case AST_LITERAL_STRING: {
					if(PRINT) printf(C_Y "interpret:ast_assign:right_side:literal:string\n" C_R);

					right_val.var_type = AST_VAR_STRING;
					right_val.d.string_data = new_str(nd->right->d.string_data->str); 

					if(PRINT) printf(C_Y "interpret:ast_assign:right_side:literal:string = %s\n" C_R, right_val.d.string_data->str);		
					break;
				}
			}
			break;
		}
		case AST_BINARY_OP: {
			if(PRINT) printf(C_Y "interpret:ast_assing:right_side=ast_binary_op\n" C_R);
			
			if(is_logic_op(nd->right)){
				int logic = ast_logic_binary_op(d, nd->right, scope);
				ERROR(d);
				
				right_val.var_type = AST_VAR_BOOLEAN;
				right_val.d.literal_type = (logic) ? AST_LITERAL_TRUE : AST_LITERAL_FALSE;								
			}
			else if(is_math_op(nd->right)){
				//vpravo je vyraz => nutno vycislit
				//x = 4*y + 12*5
		
				if(!ast_binary_op(d, nd->right, &right_val, scope)){
					if(PRINT) printf("interpret:ast_assign:ast_binary_op error during \n");
					return false;
				}
				ERROR(d);
			

				if(PRINT) printf(C_Y "interpret:ast_assign:right_val = %f\n" C_R, right_val.d.numeric_data);
				if(PRINT) printf(C_Y "interpret:ast_assign:right_val->var_type = %d\n" C_R, right_val.var_type);
			}
			else {
				if(!ast_concat(d, nd->right, &right_val, scope))
					return false;
					
				ERROR(d);
				right_val.var_type = AST_VAR_STRING;
				if(PRINT) printf(C_Y "interpret:ast_assign:right_val = %s\n" C_R, (right_val.d.string_data)?right_val.d.string_data->str:"NULL");				
			}
			break;
		}
		case AST_VAR: {
			if(PRINT) printf(C_Y "interpret:ast_assing:right_side=ast_var\n" C_R);
			//vpravo je promenna
			//x = y
			
			// potreba zakodovat jmeno promenne
			char* name = encode_name(nd->right->d.string_data->str, scope);
			struct symbol_table* var = symbol_search(d->vars, name);
			if(!var){
				if(PRINT) printf(C_Y "INTERPRET:AST_ASSIGN:VAR NOT DEFINED!!!!!" C_R " d->error = 5\n");
				// nedefinovana promenna
				(d->error) = 5;
				return false;
			}

			right_val.var_type = var->var_type;
			right_val.d.numeric_data = var->d.numeric_data;
			right_val.d.string_data = new_str(var->d.string_data->str);
			right_val.d.literal_type = var->d.literal_type; 
			break;
		}
		default: 
			//chyba - syntakticka - error
			if(PRINT) printf(C_Y "INTERPRET:AST_ASSIGN:WRONG TYPE!!!!!" C_R " d->error = 99\n");
			(d->error) = 99;
			return false;
			break;
	}

	if(PRINT) {	
		//printf(C_Y "interpret:ast_assign:right_val out = var...............................................\n" C_R);
		printf(C_Y "interpret:ast_assign:right_val out = %s\n" C_R, get_var_type_string(right_val.var_type));
		printf(C_Y "interpret:ast_assign:right_val out = %f\n" C_R, right_val.d.numeric_data);
		printf(C_Y "interpret:ast_assign:right_val out = \"%s\"\n" C_R, (right_val.d.string_data == NULL) ? "null" : right_val.d.string_data->str);
		printf(C_Y "interpret:ast_assign:right_val out = %s\n" C_R, get_literal_type_string(right_val.d.literal_type));	
		//printf(C_Y "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n" C_R);
		//printf(C_Y "interpret:ast_assign:end of the right side\n" C_R);
		printf(C_Y "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n" C_R);
	}

	if(nd->left->type == AST_VAR){
		if(PRINT) {
			printf(C_Y "interpret:ast_assing:left_side = ast_var\n" C_R);
			printf(C_Y "interpret:ast_assign:right_val type = %s\n" C_R, get_var_type_string(right_val.var_type));
			printf(C_Y "interpret:ast_assign:left_side:var name = %s \n" C_R, nd->left->d.string_data->str);
		}

		//potreba podle zadaneho jmena promenne vytvorit zakodovany nazev
		char* name = encode_name(nd->left->d.string_data->str, scope);

		struct symbol_table* var = NULL;
		if((var = symbol_search(d->vars, name))) {
			if(PRINT) printf(C_Y "interpret:ast_assign:left_side var = %s\n" C_R, var->key);
		}
		else {	
			if(PRINT) printf(C_Y "interpret:ast_assing:left_side:new var \"%s\"\n" C_R, nd->left->d.string_data->str);
	
			var = symbol_insert(d, &(d->vars), name, right_val.var_type, right_val.d);

			ERROR(d);
			if(PRINT) printf(C_Y "interpret:ast_assign:left_side:var key = %s\n" C_R, var->key);
		}
		if(var == NULL)
			if(PRINT) printf(C_Y "interpret:ast_assign:var == NULL\n" C_R);		

		//if(PRINT) printf(C_Y "interpret:ast_assing:left_side:new_var_copy_data\n" C_R);
		var->var_type = right_val.var_type;
		var->d = *new_symbol_table_data();
		var->d.numeric_data = right_val.d.numeric_data;
		var->d.literal_type = right_val.d.literal_type;
		//if(PRINT) printf(C_Y "interpret:ast_assign:saving > string\n" C_R);
		if(right_val.var_type == AST_VAR_STRING){
			var->d.string_data = new_str(right_val.d.string_data->str);
			if(PRINT) printf(C_Y "interpret:ast_assign:string var = %s\n" C_R, var->d.string_data->str);
		}		
		else{
			var->d.string_data = NULL;
			if(PRINT) printf(C_Y "interpret:ast_assign:string var = null\n" C_R);
		}
		
		// odeslani kopie
		out = var;
		if(PRINT) {
			printf(C_Y "interpret:ast_assign:nastaven out = var\n" C_R);
			printf(C_Y "interpret:ast_assign:nastaven out = %s\n" C_R, get_var_type_string(out->var_type));
			printf(C_Y "interpret:ast_assign:nastaven out = %f\n" C_R, out->d.numeric_data);
			printf(C_Y "interpret:ast_assign:nastaven out = \"%s\"\n" C_R, (out->d.string_data == NULL) ? "null" : out->d.string_data->str);
			printf(C_Y "interpret:ast_assign:nastaven out = %s\n" C_R, get_literal_type_string(out->d.literal_type));
			
			print_tree(d->vars, 0, '<');
		}		

		if(PRINT) printf(C_Y "~~~~~~~~~~~~~~~~~~~~~ast_assign end~~~~~~~~~~~~~~~~~~~~~\n" C_R);
			
		return true;
	}
	else {
		if(PRINT) printf(C_Y "INTERPRET:AST_ASSIGN:UNACCEPTABLE TYPE ON THE LEFT SIDE"C_R " d->error = 13\n");
		(d->error) = 13;
		return false;
	}	
	
	return false;
}
int ast_loop(struct data* d, struct ast_node* nd, struct symbol_table* out, int scope)
{
	if(PRINT) printf(C_Y "interpret:ast_loop:start\n" C_R);

	ERROR(d);
	
	//v leve noze je telo whilu
	struct symbol_table if_ret = *new_symbol_table();
	while(ast_if(d, nd->d.condition, &if_ret, scope)) {
		ERROR(d);

		//na zaklade splnene podminky se provadi telo whilu
		if(!ast_statement(d, nd->left, out, scope+1)) 
			return false;
	}
	if(PRINT) printf(C_Y "interpret:ast_loop:end\n" C_R);
	return true;
}

int ast_if(struct data* d, struct ast_node* nd, struct symbol_table* out, int scope)
{	
	ERROR(d);	
	if(PRINT) printf(C_Y "interpret:ast_if:start\n" C_R);

	
	if(!out){
		// najit vyuziti...
		out = new_symbol_table();
	}

	if(PRINT){
		printf("interpret:ast_if:print condition\n");
		if(nd) print_ast(nd, 0, '<');
		printf("interpret:ast_if:print condition end\n");
	}
	
	// demonstrace if(condition)
	// vrati nenulovy cislo pokud bude splnena podminka
	// vrati nulove pokud ne	
		
	// akceptuje:
	// ASSIGNS
	// LOGIC_OPERATIONS
	// BINARY_OPERATIONS
	// FUNCTION_CALLS
	
	if(nd == NULL){
		if(PRINT) printf("interpret:ast_if:nd == NULL\n");
		return false;
	}	

	
	// podle typu operace provedeme dalsi operace
	switch( nd->type ) {
		case AST_VAR:{
			if(PRINT) printf(C_Y "interpret:ast_if:ast_var\n" C_R);

			//zakodovat nazev
			char* name = encode_name(nd->d.string_data->str, scope);

			// nutno najit promennou, pokud existuje, pokud neexistuje
			struct symbol_table* pom = new_symbol_table();
			if((pom = symbol_search(d->vars, name)) != NULL) {
				// byla nalezena promenna - vratit jeji hodnotu
				return exec_var(*pom);
			}		
			else {
				// nenalezena - automaticky vratit false 
				return false;
			}	
		}
		case AST_LITERAL: {
			if(PRINT) printf(C_Y "interpret:ast_if:ast_literal\n" C_R);
			switch(nd->literal){
				case AST_LITERAL_TRUE:
					return true;
				case AST_LITERAL_FALSE:
					return false;
				case AST_LITERAL_NULL:
					return false;
				case AST_LITERAL_NUMERIC:
					return (nd->d.numeric_data != 0);
				case AST_LITERAL_STRING:
					return (nd->d.string_data != NULL);
			}
		}
		case AST_ASSIGN: { 
			if(PRINT) printf(C_Y "interpret:ast_if:ast_assign\n" C_R);
			// zavolat prirazeni s navratem leve hodnoty
			struct symbol_table navratova = *new_symbol_table();
			if(!ast_assign(d, nd, &navratova, scope))
				return false;
			
			return exec_var(navratova);
		}
		case AST_CALL: {	
			if(PRINT) printf(C_Y "interpret:ast_if:ast_call\n" C_R);
			// volani funkce z tabulky
			
			struct symbol_table navratova = *new_symbol_table();
			if(!ast_call(d, nd->left, &navratova, scope))
				return false;
				
			return exec_var(navratova);
		}
		case AST_BINARY_OP: {
			if(PRINT) printf(C_Y "interpret:ast_if:ast_binary_op\n" C_R);
			// na prvni urovni MUSI byt logicka operace!!!!!!
			if(is_logic_op(nd)){
				bool logic = ast_logic_binary_op(d, nd, scope);
				ERROR(d);
				return logic;
			}
			else if(is_math_op(nd)){
				// na prvni neni logicka, takze rovnou binarni operace obecne
				struct symbol_table ret = *new_symbol_table();
				if(!ast_binary_op(d, nd, &ret, scope))
					return false;
				
				ERROR(d)
				
				return (ret.d.numeric_data != 0);					
			}
			else {
				// CONCAT
				struct symbol_table cated = *new_symbol_table();
				if(!ast_concat(d, nd, &cated, scope))
					return false;

				ERROR(d)

				return boolval(d, &cated);
			}	
			break;					
		}	
		default: { //chyba, volana operace neni podporovana
			if(PRINT) printf(C_Y "INTERPRET:AST_IF:UNAKCEPTABLE OPERATION (%d)"C_R " d->error = 13\n", nd->type);
			(d->error) = 13;
			return false;					
		}
	}

	if(PRINT) printf(C_Y "interpret:ast_if:end\n" C_R);	
	return false;
}
void ast_literal(struct data* d, struct ast_node* nd, struct symbol_table* out, int scope)
{
	if(PRINT) printf(C_Y "interpret:ast_literal:start\n" C_R);

	if(d->error) return;
	
	if(!out){
		// najit vyuziti
		out = new_symbol_table();
	}
	
	if(!nd->literal){
		if(PRINT) printf(C_Y "INTERPRET:AST_LITERAL:NOT LITETAL"C_R " d->error = 3\n");
		(d->error) = 3;
		return;
	}

	struct symbol_table var;
	if(!make_from_node(d, nd, &var, scope))
		return;

	if(d->error) return;
	if(PRINT){
		printf(C_Y "interpret:ast_literal:nastaven var = var\n" C_R);
		printf(C_Y "interpret:ast_literal:nastaven var = %s\n" C_R, get_var_type_string(var.var_type));
		printf(C_Y "interpret:ast_literal:nastaven var = %f\n" C_R, var.d.numeric_data);
		printf(C_Y "interpret:ast_literal:nastaven var = \"%s\"\n" C_R, (var.d.string_data == NULL) ? "null" : var.d.string_data->str);
		printf(C_Y "interpret:ast_literal:nastaven var = %s\n" C_R, get_literal_type_string(var.d.literal_type));
	}	
	
	strcmp(out->key, var.key);
	out->var_type = var.var_type;
	out->d.literal_type = var.d.literal_type;
	out->d.numeric_data = var.d.numeric_data;
	if(var.d.string_data)
		out->d.string_data = new_str(var.d.string_data->str);
	else
		out->d.string_data = NULL;

	if(PRINT){
		printf(C_Y "interpret:ast_literal:nastaven out = var\n" C_R);
		printf(C_Y "interpret:ast_literal:nastaven out = %s\n" C_R, get_var_type_string(out->var_type));
		printf(C_Y "interpret:ast_literal:nastaven out = %f\n" C_R, out->d.numeric_data);
		printf(C_Y "interpret:ast_literal:nastaven out = \"%s\"\n" C_R, (out->d.string_data == NULL) ? "null" : out->d.string_data->str);
		printf(C_Y "interpret:ast_literal:nastaven out = %s\n" C_R, get_literal_type_string(out->d.literal_type));
	}		
	
	if(PRINT) printf(C_Y "interpret:ast_literal:end\n" C_R);
}
int ast_function(struct data* d, struct ast_node* nd)
{		
	ERROR(d);
	if(PRINT) printf(C_Y "interpret:ast_function:start\n" C_R);

	//vytvori zaznam v tabulce symbolu
	//node->d.string_data je nazev funkce -
	//node->left jsou parametry funkce
	//node->right je telo funkce - statement
	
	struct symbol_table* fce = symbol_search(&(*(d->functions)), nd->d.string_data->str);
	if(fce == NULL){
		if(PRINT) printf(C_Y "interpret:ast_function:add_new := %s\n" C_R, nd->d.string_data->str);
		//vytvorit novou funkci podle zadanych pravidel
		struct symbol_table_data s_data;

		fce = symbol_insert(d, &(d->functions), nd->d.string_data->str, AST_VAR_NULL, s_data);
		ERROR(d);
		fce->statement = nd->right;		//kopie ukazatele na telo funkce
		fce->params = nd->left->d.list;	//seznam parametru
		
		return true;
	}
	else
	{
		if(PRINT) printf(C_Y "INTERPRET:AST_FUNCTION:REDEFINITION:%s" C_R " d->error = 3\n", nd->d.string_data->str);

		//funkce uz byla deklarovana - pokus o redefinici!
		(d->error) = 3;
		return false; 
	}
}
int ast_concat(struct data* d, struct ast_node* nd, struct symbol_table* out, int scope)
{
	ERROR(d);
	if(PRINT) printf(C_Y "interpret:ast_concat:start\n" C_R);
	
	if(!out){
		// najit vyuziti....
		out = new_symbol_table();
	}	

	if(nd->left == NULL || nd->right == NULL) {
		if(PRINT) printf("interpret:ast_concat:subnodes are NULL\n");
		return false;	
	}
	
	if(PRINT)printf("interpret:ast_concat:left node\n");
	struct symbol_table leva; 
	if(!make_from_node(d,nd->left, &leva, scope))
		return false;
	ERROR(d);
	if(PRINT)printf("interpret:ast_concat:right node\n");
	struct symbol_table prava;
	if(!make_from_node(d,nd->right, &prava, scope))
		return false;
	ERROR(d);	

	if(PRINT)printf("interpret:ast_concat:left and right var end\n");

	out->var_type = AST_LITERAL_STRING;
	out->d.string_data = cat_str(strval(d, leva), strval(d, prava));
	if(PRINT)printf(C_Y "interpret:ast_concat:out = %s\n" C_R, out->d.string_data->str);
	return true;
}
int ast_call(struct data* d, struct ast_node* nd, struct symbol_table* out, int scope)
{
	if(PRINT) printf(C_Y "interpret:ast_call:start\n" C_R);

	ERROR(d)
	// zavola funkci s nazvem v node->d.string_data
	// a parametry v node.right
	
	if(PRINT) {
		printf("interpret:ast_call:print_tree\n");
		print_tree(d->vars, 0, '<');
		printf("interpret:ast_call:print_tree end\n");

		if(nd) printf("interpret:ast_call:nd != NULL\n");
		else printf("interpret:ast_call:nd == NULL\n");
		
		printf(C_Y "interpret:ast_call:function calling: " C_R "%s\n", (nd->d.string_data)?nd->d.string_data->str:NULL);
	}	
	// ziskani tela volane funkce
	struct symbol_table* fce = symbol_search(&(*(d->functions)), nd->d.string_data->str);
	if(fce == NULL){
		if(PRINT) printf(C_Y"INTERPRET:AST_CALL:SYMBOL_SEARCH"C_R "d->error = %d\n", 5);
		// funkce nebyla deklarovana
		(d->error) = 5;		
		return false;
	} 
	
	ERROR(d)
	if(PRINT) printf(C_Y "interpret:ast_call:" C_R "getting function parameters\n");
	struct ast_list* parametry = (nd->right != NULL) ? nd->right->d.list : NULL;	// kvuli naplneni novych promennnych
	print_param_list(parametry, 1);	
	ERROR(d)

	// zkontrolovat jestli se nevola inline funkce
	int in_key;
	if(is_inline_fn(nd->d.string_data->str, &in_key)) {
		ERROR(d)
		// volana funkce je inline
		if(PRINT) printf(C_Y "interpret:ast_call:inline!\t\t%s\n" C_R, nd->d.string_data->str);
		//char* name = encode_name(z_definice->elem->d.string_data->str, scope);		

		// projit parametry a provyst zpracovani
		// BOOLVAL, DOUBLEVAL, INTVAL, STRVAL, PUT_STR, GET_STR, STR_LEN, GET_SUB, FIND_STR, SORT_STR
		switch(in_key){
			case BOOLVAL: {
				if(PRINT) printf(C_Y "interpret:ast_call:inline:boolval\n" C_R);
				if(parametry == NULL) {
					if(PRINT) printf(C_Y "INTERPRET:AST_CALL:BOOLVAL:NO PARAMETERS"C_R " d->error = 13\n");
					return false;		
				}		

				if(parametry->elem == NULL){
					if(PRINT) printf(C_Y "INTERPRET:AST_CALL:BOOLVAL:ELEM == NULL"C_R " d->error = 13\n");
					(d->error) = 13;
					return false;
				}
				
				struct symbol_table par_var; 
				if(!make_from_node(d, parametry->elem, &par_var, scope))
					return false;
				ERROR(d);

				out->var_type = AST_VAR_BOOLEAN;
				
				if(boolval(d, &par_var)) 
					out->d.literal_type = AST_LITERAL_TRUE;
				else
					out->d.literal_type = AST_LITERAL_FALSE;
				
				if(PRINT) printf(C_Y "interpret:ast_call:boolval = %s\n" C_R, get_literal_type_string(out->d.literal_type));				

				break;
			}
			case DOUBLEVAL: {
				if(PRINT) printf(C_Y "interpret:ast_call:inline:doubleval\n" C_R);
				if(parametry == NULL) {
					if(PRINT) printf(C_Y "INTERPRET:AST_CALL:BOOLVAL:NO PARAMETERS"C_R " d->error = 13\n");
					return false;		
				}
				
				if(parametry->elem == NULL){
					if(PRINT) printf(C_Y "INTERPRET:AST_CALL:DOUBLEVAL:ELEM == NULL" C_R " d->error = 13\n");
					(d->error) = 13;
					return false;
				}
				
				struct symbol_table par_var; 
				if(!make_from_node(d, parametry->elem, &par_var, scope))
					return false;
				ERROR(d);

				out->var_type = AST_VAR_DOUBLE;
			
				double ret = doubleval(d, &par_var);
				if(PRINT) printf(C_Y "interpret:ast_call:doubleval = %f\n" C_R, ret);			
				out->d.numeric_data = ret;				
	
				break;
			}
			case INTVAL: {
				if(PRINT) printf(C_Y "interpret:ast_call:inline:intval\n" C_R);
				
				if(parametry == NULL) {
					if(PRINT) printf(C_Y "INTERPRET:AST_CALL:BOOLVAL:NO PARAMETERS"C_R " d->error = 13\n");
					return false;		
				}				

				if(parametry->elem == NULL){
					if(PRINT) printf(C_Y "INTERRPET:AST_CALL:INTVAL:ELEM == NULL" C_R " d->error = 13\n");
					(d->error) = 13;
					return false;
				}

				struct symbol_table par_var;
				if(!make_from_node(d, parametry->elem, &par_var, scope))
					return false;
				ERROR(d);

				out->var_type = AST_VAR_INT;
			
				int ret = intval(d, &par_var);
				if(PRINT) printf(C_Y "interpret:ast_call:intval = %d\n" C_R, ret);
				out->d.numeric_data = (double)ret;				
	
				break;			
			}
			case STRVAL: {
				if(PRINT) printf(C_Y "interpret:ast_call:inline:strval\n" C_R);

				if(parametry == NULL) {
					if(PRINT) printf(C_Y "INTERPRET:AST_CALL:BOOLVAL:NO PARAMETERS"C_R " d->error = 13\n");
					return false;		
				}
	
				if(parametry->elem == NULL){
					if(PRINT) printf(C_Y "INTERRPET:AST_CALL:STRVAL:ELEM == NULL" C_R " d->error = 13\n");
					(d->error) = 13;
					return false;
				}
				struct symbol_table par_var;
				if(!make_from_node(d, parametry->elem, &par_var, scope))
					return false;
				ERROR(d);

				out->var_type = AST_VAR_STRING;
			
				out->d.string_data = new_str(strval(d, par_var)->str);
				if(PRINT) printf(C_Y "interpret:ast_call:strval = %s\n" C_R, out->d.string_data->str);				
				break;							
			}
			case PUT_STR: {
				if(PRINT) {
					printf(C_Y "interpret:ast_call:inline:put_string\n" C_R);
					printf(C_Y "interpret:ast_call:put_string:start\n" C_R);
				}
				
				if(parametry == NULL) {
					if(PRINT) printf(C_Y "INTERPRET:AST_CALL:PUTSTR:NO PARAMETERS"C_R " d->error = 13\n");
					return false;		
				}
				else {
					print_param_list(parametry, 1);
				}

				int pstr = put_string(d, parametry, &(d->vars), scope);
				if(PRINT) printf("interpret:ast_call:inline:put_string:after call %d\n", pstr);
				out->d.numeric_data = (double)pstr;
				out->var_type = AST_LITERAL;	
				out->d.literal_type = AST_LITERAL_NUMERIC;
				ERROR(d);

				if(PRINT) printf(C_Y "interpret:ast_call:put_string:end\n" C_R);
				break;
			}			
			case GET_STR: {
				struct symbol_table* var = new_symbol_table();

				if(PRINT) printf(C_Y "interpret:ast_call:inline:get_string\n" C_R);
				getstring(var);
				if(PRINT) printf(C_Y "interpret:ast_call:get_string= \"%s\"\n" C_R, var->d.string_data->str);
				
				out->var_type = AST_VAR_STRING;
				out->d.string_data = new_str(var->d.string_data->str);
				
				break;
			}			
			case STR_LEN: {
				if(PRINT) printf(C_Y "interpret:ast_call:inline:strlen\n" C_R);
	
				if(parametry == NULL) {
					if(PRINT) printf(C_Y "INTERPRET:AST_CALL:BOOLVAL:NO PARAMETERS"C_R " d->error = 13\n");
					return false;		
				}

				if(parametry->elem == NULL){
					if(PRINT) printf(C_Y "INTERRPET:AST_CALL:STR_LEN:ELEM == NULL" C_R " d->error = 13\n");
					(d->error) = 13;
					return false;
				}
				struct symbol_table par_var;
				if(!make_from_node(d, parametry->elem, &par_var, scope))
					return false;
				ERROR(d);

				out->var_type = AST_VAR_INT;
				out->d.numeric_data = str_len(par_var.d.string_data);
				if(PRINT) printf(C_Y "interpret:ast_call:strlen=%d\n" C_R, (int)out->d.numeric_data);
				break;	
			}
			case GET_SUB: {
				if(PRINT) printf(C_Y "interpret:ast_call:inline:get_substring\n" C_R);
	
				if(parametry == NULL) {
					if(PRINT) printf(C_Y "INTERPRET:AST_CALL:BOOLVAL:NO PARAMETERS"C_R " d->error = 13\n");
					return false;		
				}

				if(parametry->elem == NULL ||
					parametry->next->elem == NULL ||
					parametry->next->next->elem == NULL) {
					if(PRINT) printf(C_Y "INTERRPET:AST_CALL:GET_SUBSTRING:ELEM == NULL" C_R " d->error = 13\n");
					// pokud je nekde v parametrech chyba
					(d->error) = 13;
					return false;				
				}
				
				struct symbol_table source, start, end;
				if(!make_from_node(d, parametry->elem, &source, scope))
					return false;
				if(!make_from_node(d, parametry->next->elem, &start, scope))
					return false;
				if(!make_from_node(d, parametry->next->next->elem, &end, scope))
					return false;
				ERROR(d);

				if(PRINT) {
					printf(C_Y "interpret:ast_call:get_substring:source=%s\n" C_R, source.d.string_data->str);
					printf(C_Y "interpret:ast_call:get_substring:start=%g\n" C_R, start.d.numeric_data);
					printf(C_Y "interpret:ast_call:get_substring:end=%g\n" C_R, end.d.numeric_data);
				}
				out->var_type = AST_VAR_STRING;
				out->d.string_data = new_str(getsubstring(source.d.string_data, (int)start.d.numeric_data, (int)end.d.numeric_data)->str);
			
				if(PRINT) printf(C_Y "interpret:ast_call:get_substring=%s\n" C_R, out->d.string_data->str);
				break;
			}
			case FIND_STR:{
				if(PRINT) printf(C_Y "interpret:ast_call:inline:find_string\n" C_R);
				
				if(parametry == NULL) {
					if(PRINT) printf(C_Y "INTERPRET:AST_CALL:BOOLVAL:NO PARAMETERS"C_R " d->error = 13\n");
					return false;		
				}

				// potrebuje 2 parametry
				// zdroj a patern
				if(parametry->elem == NULL || parametry->next->elem == NULL){
					// parametry nesedi
					if(PRINT) printf(C_Y "INTERRPET:AST_CALL:FIND_STR:ELEM == NULL" C_R " d->error = 13\n");
					(d->error) = 13;
					return false;
				}
				
				struct symbol_table source, pattern;
				if(!make_from_node(d, parametry->elem, &source, scope))
					return false;
				if(!make_from_node(d, parametry->next->elem, &pattern, scope))
					return false;
				ERROR(d);

				if(PRINT) {
					printf(C_Y "interpret:ast_call:find_string:source=%s\n" C_R, source.d.string_data->str);
					printf(C_Y "interpret:ast_call:find_string:patern=%s\n" C_R, pattern.d.string_data->str);					
				}
				out->var_type = AST_VAR_INT;
				out->d.numeric_data = find_string(source.d.string_data, pattern.d.string_data);					
				if(PRINT) printf(C_Y "interpret:ast_call:find_string=%d\n" C_R, (int)out->d.numeric_data);
				break;
			}
			case SORT_STR: {
				if(PRINT) printf(C_Y "interpret:ast_call:inline:sort_string....%p\n" C_R, (void*)parametry->elem);

				if(parametry == NULL) {
					if(PRINT) printf(C_Y "INTERPRET:AST_CALL:BOOLVAL:NO PARAMETERS"C_R " d->error = 13\n");
					return false;		
				}

				if(!(parametry->elem)){
					if(PRINT) printf(C_Y "INTERRPET:AST_CALL:SORT_STR:ELEM == NULL" C_R " d->error = 13\n");
					(d->error) = 13;
					return false;
				}	
				
				if(PRINT) printf(C_Y "interpret:ast_call:inline:sort_string:make_from_node\n" C_R);
				struct symbol_table source;		
				if(!make_from_node(d, parametry->elem, &source, scope-1))
					return false;
				if(PRINT) printf(C_Y "interpret:ast_call:inline:sort_string:make_from_node end (%s)\n" C_R, source.d.string_data->str);
				ERROR(d);

				if(PRINT) printf(C_Y "interpret:ast_call:sort_string=%s\n" C_R, source.d.string_data->str);				
				out->var_type = AST_VAR_STRING;
				out->d.string_data = new_str(sort_string(source.d.string_data)->str);	
				if(PRINT) printf(C_Y "interpret:ast_call:sort_string:sorted=%s\n" C_R, out->d.string_data->str);				
				break;			
			}
		}
	
	}
	else {
		ERROR(d);
		
		// pomocne odkazy na seznamy parametru funkce
		if(PRINT)printf(C_Y "interpret:ast_call:" C_R "getting definition parameters\n");
		struct ast_list* z_definice = fce->params;	// kvuli ziskani nazvu promennych
		print_param_list(z_definice, 1);
				
		if(PRINT) printf(C_Y "interpret:ast_call:while - getting parameters and settings inner vars\n" C_R);
		// prochazeni podle definovanych parametru a ukladani hodnot z parametru
		while(z_definice != NULL && z_definice->elem != NULL){
			// dokud nedojdu na konec definovanych parametru
			char* name = encode_name(z_definice->elem->d.string_data->str, scope+1);		
			if(PRINT) printf("interpret:ast_call:encoded name = %s\n", name);

			if(z_definice->elem->type == AST_VAR){
				if(PRINT) printf(C_Y "interpret:ast_call:ast_var = \"%s\"\n" C_R, name);
				// je to promenna
				// ziskat nazev a vytvorit v top tabulce 
				// naplnit nove vzniklou promennou daty z predanych parametru			
			
				// nazev promenne z_definice->elem->d.string_data->str;
			
				// vezme nazev promenne z definice funkce
				// podle typu predaneho paremetru provede natypovani nove vznikajici
				// promenne a ulozi do ni hodnoty z predaneho paremetru
				// nakonec nove vzniklou promennou vlozi do TOP tabulky
				switch(parametry->elem->type){
					case AST_VAR:{
						// predany parametr je promenna v tabulce
						struct symbol_table* var = new_symbol_table();
						char* par_name = encode_name(parametry->elem->d.string_data->str, scope);					

						if(!(var = symbol_search(d->vars, par_name))) {
							if(PRINT) printf(C_Y "INTERRPET:AST_CALL:VAR NOT FOUND" C_R " d->error = 5\n");			
							(d->error) = 5;
							return false;
						}

						// vlozeni do top tabulky	
						if(PRINT) printf(C_Y "interpret:ast_call:vkladam_promennou\n" C_R);
						if(symbol_insert(d, &(d->vars), name, var->var_type, var->d))
							ERROR(d);
					
						break;
					}
					case AST_LITERAL:{
						// hodnota se predala literalem

						enum ast_var_type pom_type = AST_VAR_NULL;
						struct symbol_table_data st_data;
						st_data.numeric_data = 0.0;
						st_data.string_data = NULL;
						st_data.literal_type = AST_LITERAL_NULL;
					
						switch(parametry->elem->literal){
							case AST_LITERAL_TRUE:
							case AST_LITERAL_FALSE:
								pom_type = AST_VAR_BOOLEAN;
								st_data.literal_type = parametry->elem->literal;
								break;
							case AST_LITERAL_NUMERIC:
								pom_type = AST_VAR_DOUBLE;
								st_data.numeric_data = parametry->elem->d.numeric_data;
								break;
							case AST_LITERAL_STRING:
								pom_type = AST_VAR_STRING;
								st_data.string_data = new_str(parametry->elem->d.string_data->str);
							case AST_LITERAL_NULL:
								pom_type = AST_VAR_NULL;
								break;
						}

											
						if(PRINT) printf(C_Y "interpret:ast_call:vkladam_literal\n" C_R);
						// pridani prekonvertovaneho parametru do top tabulky
						if(symbol_insert(d, &(d->vars), name, pom_type, st_data))
							ERROR(d);	
					
						break;
					}
					case AST_CALL: {
						// zavolat funkci, ziskat vysledek a ten vlozit do promenne
						struct symbol_table call_out = *new_symbol_table();
						if(!ast_call(d, parametry->elem, &call_out, scope))
							return false;

						if(PRINT) printf(C_Y "interpret:ast_call:vkladam_vysledek_funkce------------------***\n" C_R);			
						// vytvorit promennou podle
						if(symbol_insert(d, &(d->vars), name, call_out.var_type, call_out.d))
							ERROR(d);
						break;
					}		
					case AST_BINARY_OP: {
						if(PRINT) {
							printf("interpret:call:binary op\n");
							print_ast(parametry->elem, 0, '<');
						}
						if(is_logic_op(nd)){
							if(PRINT) printf(C_Y "interpret:call:logic op\n");
							struct symbol_table_data s_data;
							s_data.numeric_data = 0.0;
							s_data.literal_type = AST_LITERAL_NULL;
							s_data.string_data = NULL;
							
							int ret = ast_logic_binary_op(d, parametry->elem, scope);
							ERROR(d);
												
							s_data.literal_type = (ret == 0) ? AST_LITERAL_FALSE : AST_LITERAL_TRUE;
							
							if(symbol_insert(d, &(d->vars), name, AST_VAR_BOOLEAN, s_data))
								ERROR(d);
						}
						else if(is_math_op(nd)){
							if(PRINT) printf(C_Y "interpret:call:binary op\n" C_R);
							struct symbol_table ret = *new_symbol_table();
							if(!ast_binary_op(d, parametry->elem, &ret, scope)){
								return false;
							}
							ERROR(d);
								
							if(PRINT) printf(C_Y "interpret:call:binary op numeric val = %g\n" C_R, ret.d.numeric_data);						
							if(symbol_insert(d, &(d->vars), name, AST_VAR_DOUBLE, ret.d))
								ERROR(d);
						}
						else {
							if(PRINT) printf(C_Y "interpret:call:concat\n"C_R );
							struct symbol_table ret = *new_symbol_table();
							if(!ast_concat(d, parametry->elem, &ret, scope))
								return false;
								
							ERROR(d);
							
							if(symbol_insert(d, &(d->vars), name, AST_VAR_STRING, ret.d))
								ERROR(d);
						}
						break;
					}						
					default: {
						//chyba! spatny parametry
						if(PRINT) printf(C_Y "INTERRPET:AST_CALL:WRONG TYPE OF PARAMS (%d)" C_R " d->error = 13\n", parametry->elem->type);
						(d->error) = 13;
						return false;
					}
				}
			} 
			else {
				// neni promenna - chyba!
				if(PRINT) printf(C_Y "INTERRPET:AST_CALL:NOT AKCEPTABLE TYPE" C_R " d->error = 13\n");
				(d->error) = 13;
				return false;
			}
				
		
			
			// kontrola kvuli nestejnemu poctu parametru definovanych
			// a predanych
			if((z_definice->next) && !(parametry->next)){
				// malo predanych parametru

				if(PRINT) printf(C_Y "INTERRPET:AST_CALL:TOO FEW PARAMS" C_R " d->error = 4\n");
				(d->error) = 4;
				return false;
			}
				
			// pokud jsou stejne pocty
			z_definice = z_definice->next;
			parametry = parametry->next;		
		}	
		if(PRINT) printf(C_Y "interpret:ast_call:" C_R "all parameters set\n");
		ERROR(d);
	
		// vykonani instrukci z tela funkce
		struct symbol_table ret = *new_symbol_table();
		if(!ast_statement(d, fce->statement, &ret, scope+1))
			return false;

		out->var_type = ret.var_type;
		out->d.numeric_data = ret.d.numeric_data;
		out->d.string_data = new_str(ret.d.string_data->str);
		out->d.literal_type = ret.d.literal_type;

		ERROR(d);	
	}	
	// zniceni vrcholu tabulky
	if(PRINT) printf("interpret:ast_call:disposing vars by scope (%d)\n", scope);
	//dispose_scope(d, d->vars, scope);
	if(PRINT) printf("interpret:ast_call:disposing finished\n");
	
	return true;
}
int ast_return(struct data* d, struct ast_node* nd, struct symbol_table* out, int scope)
{
	if(PRINT) printf(C_Y "interpret:ast_return:start\n" C_R);
	ERROR(d);

	if(!out){
		out = new_symbol_table();
	}

	// do outu vratit hodnotu v levem podstromu
	// projit zase podle typu nodu
	
	// struktura muze obsahovat:
	// LITERAL
	// VAR
	// CALL
	// BINARY_OP
	// LOGIC_OP
	// NULL
	
	struct symbol_table* var = new_symbol_table();
	switch(nd->left->type){
		case AST_VAR:{
			if(!make_from_node(d, nd->left, var, scope))
				return false;
			ERROR(d);

			if(PRINT) print_tree(var, 0, 'v');
			*out = *var;
			if(PRINT) print_var(out, "ast_return out");
			
			return true;
		}
		case AST_LITERAL: {
			if(!make_from_literal(d, nd->left, var))
				return false;
			ERROR(d);

			if(PRINT) print_tree(var, 0, 'v');
			*out = *var;
			if(PRINT) print_var(out, "ast_return out");

			return true;
		}
		case AST_BINARY_OP: {
			if(PRINT) printf(C_Y "interpret:ast_return:ast_binary_op\n" C_R);
			// na prvni urovni MUSI byt logicka operace!!!!!!
			if(is_logic_op(nd->left)){
				if(ast_logic_binary_op(d, nd->left, scope)){
					ERROR(d);
			
					var->var_type = AST_VAR_BOOLEAN;
					var->d.literal_type = AST_LITERAL_TRUE;					
				}
				else {
					ERROR(d);
								
					var->var_type = AST_VAR_BOOLEAN;
					var->d.literal_type = AST_LITERAL_FALSE;										
				}

				*out = *var;
				return true;
			}
			else if(is_math_op(nd)){
				// na prvni neni logicka, takze rovnou binarni operace obecne
				if(!ast_binary_op(d, nd->left, var, scope))
					return false;
				
				ERROR(d)
				
				*out = *var;
				return true;					
			}
			else {
				// CONCAT
				if(!ast_concat(d, nd->left, var, scope))
					return false;

				ERROR(d)

				*out = *var;	
				return true;
			}	

			break;
		}
		case AST_CALL: {
			if(!ast_call(d, nd->left, var, scope))
				return false;
			ERROR(d);

			*out = *var;
			return true;
		}
		default: break;
	}

	
	return false;
}
int ast_var(struct data* d, struct ast_node* nd, struct symbol_table* out, int scope)
{
	if(PRINT) printf(C_Y "interpret:ast_var:start\n" C_R);
	
	if(!out){
		// pokud je nulovy
		out = new_symbol_table();
	}
	// zakodovani nazvu pro potreby
	char* name = encode_name(nd->d.string_data->str, scope);
	
	// vytvorit promennou s nazvem node.name
	// nebo zmeni hodnotu 
	struct symbol_table* ret = symbol_search(d->vars, name);
	
	// v pripade ze nebyla nalezena
	if(ret == NULL) {
		// vytvoreni nove promenne v tabulce
		struct symbol_table_data s_data;
		if(PRINT) printf(C_Y "interpret:ast_var:new_var %s\n" C_R, name);
		
		// vytvoreni prazdne promenne
		out = symbol_insert(d, &(d->vars), name, AST_VAR_NULL, s_data);
	}
	
	if(PRINT) printf(C_Y "interpret:ast_var:returning var\n" C_R);
	// vratit pres out odkaz
	strcmp(out->key, ret->key);
	out->var_type = ret->var_type;
	out->d.numeric_data = ret->d.numeric_data;
	out->d.literal_type = ret->d.literal_type;
	if(ret->d.string_data)
		out->d.string_data = new_str(ret->d.string_data->str);
	else
		out->d.string_data = NULL;	
	print_var(out, "returned val");


	return true;
}
int ast_binary_op(struct data* d, struct ast_node* nd, struct symbol_table* out, int scope)
{
	if(PRINT) printf(C_Y "interpret:ast_binary_op:start\n" C_R);
	//print_ast(nd, 0);
	ERROR(d);
	
	// akceptuje:
	// matematickou operaci
	// volani funkce - kontrola navratove hodnoty
	// odkaz do tabulky promennych - ast_var
	// konstanta - ast_literal
	// spojeni stringu
	
	if(PRINT) {
		printf("interpret:ast_binary_op:print nodes\n");
		print_ast(nd, 0, '<');
		printf("interpret:ast_binary_op:print nodes end\n");
	}	


	out->var_type = AST_VAR_NULL;
	out->d.numeric_data = 0.0;
	out->d.string_data = NULL;
	out->d.literal_type = AST_LITERAL_NULL;

	//podle typu uzlu pokracovat
	switch(nd->type) {
		case AST_VAR: {
			char* name = encode_name(nd->d.string_data->str, scope);

			struct symbol_table* ret;
			if((ret = symbol_search(d->vars, name))){
				// promenna byla nalezena!
				if(PRINT) {
					printf(C_Y "interpret:ast_binary_op:ast_var: %s exists\n" C_R, ret->key);
					printf(C_Y "interpret:ast_binary_op:ast_var_type = %s\n" C_R, get_var_type_string(ret->var_type));
				}

				print_var(ret, "vracena promenna");
				*out = *ret;
				print_var(out, "OUT <= RET");
			

				/*
				strcpy(out->key, ret->key);
				out->var_type = ret->var_type;
				out->d.numeric_data = ret->d.numeric_data;
				out->d.literal_type = ret->d.literal_type;
				if(ret->d.string_data){
					out->d.string_data = new_str(ret->d.string_data->str);		
					printf("string data not empty (%s)\n", out->d.string_data->str);
				}
				else {
					printf("string data is empty\n");
					out->d.string_data = NULL;		
				}*/
				return true;
			}
				
			if(PRINT)printf(C_Y "interpret:ast_binary_op:ast_var = NOT FOUND\n" C_R);
			return false;
		}
		case AST_LITERAL: {
			if(PRINT) printf(C_Y "interpret:ast_binary_op:literal\n" C_R);
			//struct symbol_table ret; //= new_symbol_table();
			//ret.var_type = AST_VAR_DOUBLE;
			out->var_type = AST_VAR_NULL;
			out->d.numeric_data = 0.0;
			out->d.string_data = NULL;
			out->d.literal_type = AST_LITERAL_NULL;
			
			switch(nd->literal){
				case AST_LITERAL_TRUE: {
					if(PRINT) printf(C_Y "interpret:ast_binary_op:literal:true\n" C_R);
					out->var_type = AST_VAR_DOUBLE;					
					out->d.numeric_data = 1;
					break;					
				}
				case AST_LITERAL_FALSE: {
					if(PRINT) printf(C_Y "interpret:ast_binary_op:literal:false\n" C_R);
					out->var_type = AST_VAR_DOUBLE;
					out->d.numeric_data = 0;	
					break;	
				}
				case AST_LITERAL_NULL: {
					if(PRINT) printf(C_Y "interpret:ast_binary_op:literal:null\n" C_R);
					out->var_type = AST_VAR_DOUBLE;
					out->d.numeric_data = 0;
					break;
				}
				case AST_LITERAL_NUMERIC: {
					//if(PRINT) printf(C_Y "interpret:ast_binary_op:literal:numeric: nd = %g\n" C_R, nd->d.numeric_data);
					out->var_type = AST_VAR_DOUBLE;
					out->d.numeric_data = nd->d.numeric_data;
					if(PRINT) printf(C_Y "interpret:ast_binary_op:literal:numeric: ret = %g\n" C_R, out->d.numeric_data);
					break;
				}
				case AST_LITERAL_STRING: {
					if(PRINT) printf(C_Y "interpret:ast_binary_op:literal:string := %s\n" C_R, nd->d.string_data->str);
					
					out->var_type = AST_VAR_STRING;
					out->d.string_data = new_str(nd->d.string_data->str);		
										
					if(PRINT) printf(C_Y "interpret:ast_binary_op:literal:ret->d.string_data->str = %s\n" C_R, out->d.string_data->str);				
					break;
				}
			}
			break;
		}
		case AST_CALL: {
			if(PRINT) printf(C_Y "interpret:ast_binary_op:call:name: %s\n" C_R, nd->d.string_data->str);
			struct symbol_table* fce = symbol_search(&(*(d->functions)), nd->d.string_data->str);
			if(fce == NULL){
				// nedefinovana funkce
				if(PRINT) printf(C_Y "INTERRPET:AST_BINARY_OP:NONDEFINED FUNCTION" C_R " d->error = 3");
				(d->error) = 3;
				return false;				
			}			
	
			if(!ast_call(d, nd, out, scope))
				return false;;		

			ERROR(d);
				
			break;
		}
		case AST_ASSIGN: {
			if(PRINT) printf(C_Y "interpret:ast_binary_op:assign\n" C_R);
			if(!ast_assign(d, nd, out, scope))
				return false;
			
			ERROR(d);
			break;
		}
		case AST_BINARY_OP: {
			if(PRINT) {			
				printf(C_Y "interpret:ast_binary_op:binary_op - rekurze\n" C_R);
				printf(C_Y "interpret:ast_binary_op:new_symbol_tables for Left and Right sides\n" C_R);
			}
			struct symbol_table l; // = new_symbol_table(); //leva strana vyrazu
			struct symbol_table r; // = new_symbol_table(); //prava strana vyrazu

			// dalsi binarni operace
			ast_binary_op(d, nd->left, &l, scope);
			ast_binary_op(d, nd->right, &r, scope);
			if(PRINT) {
				printf(C_Y "interpret:ast_binary_op:LEFT = %f\n" C_R, l.d.numeric_data);
				printf(C_Y "interpret:ast_binary_op:RIGHT = %f\n" C_R, r.d.numeric_data);
			}
			ERROR(d);
			
			struct symbol_table ret; // = new_symbol_table();
			ret.d.string_data = NULL;
			ret.var_type = AST_VAR_DOUBLE;

			switch(nd->d.binary) {
				case AST_BINARY_PLUS:
					if(PRINT) printf(C_Y "interpret:ast_binary_op:binary_op:plus\n" C_R); 
					// secist l + r prevedene na typ double
					//l->d.numeric_data = doubleval(d, l);
					//r->d.numeric_data = doubleval(d, r);

					if(PRINT) printf(C_Y "interpret:ast_binary_op:binary_op: %f + %f\n" C_R, l.d.numeric_data, r.d.numeric_data); 
					ret.var_type = AST_VAR_DOUBLE;
					ret.d.numeric_data = l.d.numeric_data + r.d.numeric_data;
					
					break;
				case AST_BINARY_MINUS: 
					if(PRINT) printf(C_Y "interpret:ast_binary_op:binary_op:minus\n" C_R);
					// odecist l - r prevedene na double
					//l->d.numeric_data = doubleval(d, l);
					//r->d.numeric_data = doubleval(d, r);

					if(PRINT) printf(C_Y "interpret:ast_binary_op:binary_op: %f - %f\n" C_R, l.d.numeric_data, r.d.numeric_data); 
					ret.var_type = AST_VAR_DOUBLE;
					ret.d.numeric_data = l.d.numeric_data - r.d.numeric_data;

					break;
				case AST_BINARY_TIMES: 
					if(PRINT) printf(C_Y "interpret:ast_binary_op:binary_op:times\n" C_R);					
					// vynasobit l - r prevedene na double
					//l->d.numeric_data = doubleval(d, l);
					//r->d.numeric_data = doubleval(d, r);
					
					if(PRINT) printf(C_Y "interpret:ast_binary_op:binary_op: %f * %f\n" C_R, l.d.numeric_data, r.d.numeric_data); 
					ret.var_type = AST_VAR_DOUBLE;
					ret.d.numeric_data = l.d.numeric_data * r.d.numeric_data; 

					break;
				case AST_BINARY_DIVIDE:
					if(PRINT) {
						printf(C_Y "interpret:ast_binary_op:binary_op:divide\n" C_R); 
						printf(C_Y "interpret:ast_binary_op:binary_op:divide: doubleval\n" C_R);
						printf(C_Y "interpret:ast_binary_op:binary_op:divide: doubleval\n\tx = %f\n\ty = %f\n" C_R, l.d.numeric_data, r.d.numeric_data);
					}
					if(r.d.numeric_data == 0) {
						(d->error) = 10;
						if(PRINT) printf(C_Y "INTERRPET:AST_BINARY_OP:DEVISION BY ZERO" C_R " d->error = 10");
						return false;
					}

					ret.var_type = AST_VAR_DOUBLE;
					ret.d.numeric_data = l.d.numeric_data / r.d.numeric_data;
					if(PRINT)printf(C_Y "interpret:ast_binary_op:binary_op:divide: (%d) %f\n" C_R, ret.var_type, ret.d.numeric_data);
					break;
				case AST_BINARY_STRING_CONCATENATION: {
					if(PRINT)printf(C_Y "interpret:ast_binary_op:binary_op:concat\n" C_R);
					// spoji dva stringy a vrati vysledek		
					
					if(!ast_concat(d, nd, &ret, scope)) {
						if(PRINT) printf("interpret:ast_concat:error during concatenation\n");
						return false;
					}
					ret.var_type = AST_VAR_STRING;
					if(PRINT)printf(C_Y "interpret:ast_binary_op:binary_op:cated = \"%s\"\n" C_R, ret.d.string_data->str);	

					break;
				}
				default: {
					//nejsou akceptovane operace
					if(PRINT) printf(C_Y "INTERRPET:AST_BINARY_OP:UNAKCEPTABLE OPERATION" C_R " d->error = 13\n");
					(d->error) = 13;
					return false;				
				}
			}			
			
			//ret.var_type = AST_VAR_DOUBLE;
			out->var_type = ret.var_type;	
			if(PRINT)printf("interpret:var_type = %s\n", get_var_type_string(out->var_type));
			out->d.numeric_data = ret.d.numeric_data;
			if(PRINT)printf("interpret:\tnumeric = %g\n", out->d.numeric_data);

			if(ret.d.string_data != NULL){
				out->d.string_data = new_str(ret.d.string_data->str);		
				if(PRINT)printf("interpret:\tstring not NULL (%s)\n", out->d.string_data->str);
			}
			else {
				out->d.string_data = NULL;	
				if(PRINT)printf("interpret:\tstring is NULL\n");
			}
			out->d.literal_type = ret.d.literal_type;
			if(PRINT) {
				printf("interpret:\tliteral = %d\n", out->d.literal_type);
				printf("interpret:ulozeni dat probehne\n");
			}
			break;
		}
		default: {
			// FUCK THE POLICE!
			// ALL HELL IS BREAKIN LOOSE
			if(PRINT) printf(C_Y "INTERRPET:AST_BINARY_OP:UNAKCEPTABLE TYPE OF OPERATION" C_R " d->error = 13");
			(d->error) = 13;
			return false;
		}	
	}

	if(PRINT) {
		printf(C_Y "interpret:ast_binary_op: out typ\t= %s\n" C_R, get_var_type_string(out->var_type));			
		printf(C_Y "interpret:ast_binary_op: out val\t= %g\n" C_R, out->d.numeric_data);	
		printf(C_Y "interpret:ast_binary_op: out str\t= \"%s\"\n" C_R, (out->d.string_data != NULL) ? out->d.string_data->str : "NULL");		
		printf(C_Y "interpret:ast_binary_op: out lit\t= %s\n" C_R, get_literal_type_string(out->d.literal_type));
	}

	return true;
}
int ast_logic_binary_op(struct data* d, struct ast_node* nd, int scope)
{
	/*
		vytvori promennou pro levou a pravou stranu
		zavola na jednotlivych promennych binarni operaci odpovidajici 
		levemu a pravemu podstromu
		nakonec s takto naplnenymi promennymi provede zadanou logickou operaci
	*/
	if(PRINT) printf(C_Y "interpret:ast_logic_binary_op:start\n" C_R);
	ERROR(d);

	struct symbol_table* leva_strana = new_symbol_table();
	struct symbol_table* prava_strana = new_symbol_table();

	ast_binary_op(d, nd->left, leva_strana, scope);
	ast_binary_op(d, nd->right, prava_strana, scope);
	
	ERROR(d);		
	
	return exec_logic_operation(d, leva_strana, prava_strana, nd->d.binary);	
}
int walk_through(struct data* d, struct ast_node* node, struct symbol_table* out, int scope)
{
	if(PRINT)printf(C_Y "interpret:walk_through:start\n" C_R);
	//printf(C_Y "interpret:walk_through:print_tree:vars:start\n" C_R);
	//print_tree(vars, 0, '<');
	//printf(C_Y "interpret:walk_through:print_tree:vars:end\n" C_R);
	
	ERROR(d);	

	struct symbol_table ret = *new_symbol_table();
	
	//prochazeni skrz node
	switch(node->type) {
		case AST_ASSIGN: 
			if(PRINT)printf(C_Y "interpret:walk_through:ast_assign\n" C_R);
			if(!ast_assign(d, node, &ret, scope)){
				if(PRINT) printf("interpret:walk_through:ast_assign - return false\n");
				return false;
			}
			break;
		case AST_CALL:
			if(PRINT)printf(C_Y "interpret:walk_through:ast_call\n" C_R);
			// kdyz vrati null, pak pres boolval prebrat na false
			if(!ast_call(d, node, &ret, scope)){
				if(PRINT) printf("interpret:walk_through:ast_call - return false\n");
				return false;
			}
			break;
		case AST_VAR:
			if(PRINT)printf(C_Y "interpret:walk_through:ast_var\n" C_R);
			if(!ast_var(d, node, &ret, scope)){
				if(PRINT) printf("interpret:walk_through:ast_var - return false\n");
				return false;
			}
			break;			
		case AST_IF: {
			if(PRINT) printf(C_Y "interpret:walk_through:ast_if\n" C_R);
				
			//print_ast(node->d.condition, 0);			
			
			int if_result = ast_if(d, node->d.condition, &ret, scope);
					
			ERROR(d)
				
			if(if_result){
				if(!node->left) 
					if(PRINT) printf(C_Y "interpret:walk_through:ast_if:statement = NULL\n" C_R);
				if(!ast_statement(d, node->left, &ret, scope+1))
					return false;
			}
			else{
				if(!node->right) 
					if(PRINT) printf(C_Y "interpret:walk_through:ast_if:statement = NULL\n" C_R);
				if(!ast_statement(d, node->right, &ret, scope+1))
					return false;
			}
			break;
		}
		case AST_WHILE:
			if(PRINT)printf(C_Y "interpret:walk_through:ast_while\n" C_R);
			if(!ast_loop(d, node, &ret, scope)){
				if(PRINT) printf("interpret:walk_through:ast_while - return false\n");
				return false;
			}
			break;
		case AST_RETURN:
			if(PRINT)printf(C_Y "interpret:walk_through:ast_return\n" C_R);
			
			struct symbol_table ret_var;
			if(!make_from_node(d, node->left, &ret_var, scope))
				return false;
			ERROR(d);

			if(PRINT){
				printf("interpret:walk_through:ast_return:\nvar_type = %s\nstring = \"%s\"\nnumeric = %g\nliteral = %s\n", 
					get_var_type_string(ret_var.var_type),
					ret_var.d.string_data->str,
					ret_var.d.numeric_data,
					get_literal_type_string(ret_var.d.literal_type));
			}

			out->var_type = ret_var.var_type;
			out->d.numeric_data = ret_var.d.numeric_data;
			out->d.string_data = new_str(ret_var.d.string_data->str);
			out->d.literal_type = ret_var.d.literal_type;

			break;
		default: break;
	}
	if(PRINT) printf(C_Y "interpret:walk_through:end\n" C_R);
	return true;
}
int find_all_definitions(struct data* d, struct ast_list* zero_lvl)
{
	if(PRINT) printf(C_Y "interpret:find_add_definitions:start\n" C_R);
	ERROR(d);
	struct ast_list* cur = zero_lvl;

	if(PRINT) printf(C_Y "interpret:find_all_definitions:adding_from_list\n" C_R);
	while(cur->elem != NULL){
		if(PRINT) printf(C_Y "interpret:find_all_definitions:while:elem_type = %s\n" C_R, get_op_type_string(cur->elem->type));
		if(cur->elem->type == AST_FUNCTION){
			// vytvori uzel do tabulky funkci
			//if(PRINT) printf(C_Y "interpret:find_all_definitions:elem := %s\n" C_R, cur->elem->d.string_data->str);
			ast_function(d, cur->elem);
		}
		
		// posun na dalsi instrukci
		if(cur->next) cur = cur->next;
		else break;
	}
	if(PRINT) printf(C_Y "interpret:find_all_definitions:end\n" C_R);	
	return true;
}
int add_inline_functions(struct data* d, bool print)
{
	if(print) printf(C_Y "interpret:add_inline_functions:start\n" C_R);
	ERROR(d);

	// prida definice funkci 
	char* in_fn[10] = { "boolval\0", "doubleval\0", "intval\0", "strval\0", "get_string\0", "put_string\0", "strlen\0", "get_substring\0", "find_string\0", "sort_string\0" };
	
	struct symbol_table_data s_data;

	for(int i = 0; i < 10; i++){
		s_data.string_data = new_str(in_fn[i]);
		symbol_insert(d, &(d->functions), in_fn[i], AST_VAR_NULL, s_data);	
	}

	return true;	
}

int interpret(struct data* d)
{
	if(PRINT) {
		printf(C_Y "=========================================\n" C_R);
		printf(C_Y "=============={ HERE WE GO }=============\n" C_R);
		printf(C_Y "=========================================\n" C_R);
		printf(C_Y "interpret:main:start (%d)\n" C_R, d->error);
	}
	ERROR(d);
	
	//snad inicializace promennych --- snad
	if(PRINT) printf(C_Y "interpret:main:vars init\n" C_R);
	symbol_init(&(d->vars));
	if(PRINT) printf(C_Y "interpret:main:functions init\n" C_R);
	symbol_init(&(d->functions));
	 
	// nalezeni vsech funkci
	find_all_definitions(d, d->tree->d.list);
	if(PRINT) printf(C_Y "interpret:volani add_inline_functions\n" C_R);
	// pridani vsech inline funkci 
	add_inline_functions(d, false);

	print_instruction_list(d->tree->d.list, 0);
	//printf(C_Y "interpret:print_tree:FUNCTIONS:start\n" C_R);
	//print_tree(d->functions, 0, '<');
	//printf(C_Y "interpret:print_tree:FUNCTIONS:end\n" C_R);

	if(PRINT) printf(C_Y "interpret:cur = d->tree->d.list\n" C_R);		
	struct ast_list* cur = d->tree->d.list;

	if(PRINT) printf(C_Y "interpret:out = new_symbol_table\n" C_R);		
	struct symbol_table* out = new_symbol_table();

	if(PRINT) printf(C_Y "interpret:EXECUTION START\n" C_R);
	while(cur != NULL) {
		//tisk vzhledu stromu elementu cur->elem
		print_ast(cur->elem, 0, '<');
		
		//pruchod stromem
		if(!walk_through(d, cur->elem, out, 0)){
			if(PRINT) printf("interpret:walk_through error ending interpret\n");
			//return false;			
			break;
		}

		cur = cur->next;
	}	

	
	if(PRINT) { 
		printf(C_Y "interpret:print_tree:VARS:start\n" C_R);
		print_tree(d->vars, 0, '<');
		printf(C_Y "interpret:print_tree:VARS:end\n" C_R);
	
		printf(C_Y "==========================================\n" C_R);
		printf(C_Y "==============={ I'M DONE }===============\n" C_R);
		printf(C_Y "==========================================\n" C_R);
	}

	ERROR(d);
	return true;
}

