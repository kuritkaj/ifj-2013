/*
    IFJ 2013

    xmarko07 - AntonĂ­n Marko
    xmahne00 - Jakub Mahnert
    xkurit00 - Jakub KuĹ™itka
    xkubic34 - Martin KubĂ­ÄŤek
    xlechp00  - Pali Lech

    "Let the C language die."
 */

#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include "str.h"
#include "gc.h"
#include "common.h"
#include "ast.h"
#include "library.h"
#include "interpret.h"
#include <math.h>

#define CYAN    "\x1b[36m"
#define RESET   "\x1b[0m"
/**
 * to je snad jasny ne
 * nejste kreteni
 * vole
 * :D
 * @param  source [description]
 * @return        [description]
 */
int str_len(string * source)
{
	return (source->len);
}


void print_string(string * source)
{
	int len = str_len(source);
	int i = 0;
	while (i < len) {
		if ((source->str[i] == '\\') && (i != len-1)) {
			//soucasny znak je escape
			if (source->str[i+1] == 't') {
				putchar('\t');
				i += 2;
			} else if (source->str[i+1] == 'n') {
				putchar('\n');
				i += 2;
			} else if (source->str[i+1] == '"') {
				putchar('"');
				i += 2;
			} else if (source->str[i+1] == '\\') {
				putchar('\\');
				i += 2;
			} else if (source->str[i+1] == '$') {
				putchar('$');
				i += 2;
			} else {
				putchar(source->str[i]);
				putchar(source->str[i+1]);
				i += 2;
			}
		} else {
			//znak se prepise
			putchar(source->str[i]);
			i++;
		}
	}
}
/**
 * name: boolval
 * prepis promenne ze sour do dest podle tabulky v zadani
 * kluci vy berete drogy nebo co ze neprepisujete popisy funkci po tom
 * co je refaktorujete?
 * chtel bych to mit takhle v pici
 * @param d:	datova struktura - pro navrat chyby
 * @param sour: vstupni promenna 
 * @return bool reprezentace termu
 */
int boolval(struct data* d, struct symbol_table* sour)
{
	if(sour == NULL) {
		d->error = 13;
		return 0;
	}
	switch (sour->var_type) {
		case AST_VAR_NULL: {
			//printf(CYAN "BOOLVAL: NULL\n" RESET);
			return false;
		}
		case AST_VAR_BOOLEAN: {
			//printf(CYAN "BOOLVAL: BOOLEAN\n" RESET);
			return (sour->d.literal_type == AST_LITERAL_TRUE); 
		}
		case AST_VAR_STRING: {
			//printf(CYAN "BOOLVAL: STRING \"%s\"\n" RESET, sour->d.string_data->str);
			return (sour->d.string_data->str != NULL);
		}
		case AST_VAR_DOUBLE: {
			//printf(CYAN "BOOLVAL: DOUBLE \"%g\"\n" RESET, sour->d.numeric_data);
			return (sour->d.numeric_data != 0);
		}
		case AST_VAR_INT: {
			//printf(CYAN "BOOLVAL: INT\n" RESET);
			return (sour->d.numeric_data != 0);
		}
		default: return false;
	}
}
/**
 * name: intval
 * prepis promenne ze sour do dest podle tabulky v zadani
 * @param d:	datova struktura - pro navrat chyby
 * @param sour: vstupni promenna 
 * @return konvertovanou hodnotu
 */
int intval(struct data* d, struct symbol_table* sour)
{
	//neni co zpracovavat, ale neni definovano, ze se ma vracet chyba
	if(sour == NULL) {
		//printf("\x1b[31mL\x1b[0m\tlibrary:intval:sour = NULL\n");
		d->error = 13;
		return 0;
	}
			
	switch(sour->var_type){
		case AST_VAR_NULL: {
			//printf("\x1b[31mL\x1b[0m\tlibrary:intval:NULL\n");
			return 0;
			break;
		}
		case AST_VAR_BOOLEAN: {
			//printf("\x1b[31mL\x1b[0m\tlibrary:intval:BOOL\n");
			if(sour->d.literal_type == AST_LITERAL_TRUE)
				return 1;
			else if(sour->d.literal_type == AST_LITERAL_FALSE)
				return 0;
		
			break;
		}
		case AST_VAR_INT: {
			//printf("\x1b[31mL\x1b[0m\tlibrary:intval:INT\n");
			return (int)sour->d.numeric_data;
			break;
		}
		case AST_VAR_DOUBLE: {
			//printf("\x1b[31mL\x1b[0m\tlibrary:intval:DOUBLE\n");
			return (int)sour->d.numeric_data;
			break;
		}
		case AST_VAR_STRING: {
			//printf("\x1b[31mL\x1b[0m 	\tlibrary:intval:STRING\n");
			int ret = 0;
			int i = 0;
			char cur;
			
			int reading = 0;
			int MINUS = 0;
			while((cur = sour->d.string_data->str[i++]) != '\0'){
				if(isdigit(cur)){
					if(!reading) reading = 1;
					
					ret = ret*10 + (int)(cur - '0');
				}
				else{
					// pokud je MINUS ale necte se cislo
					if(MINUS && !reading)
						MINUS = 0;
							
					// pokud jeste necte a narazi na - 
					// nastavi MINUS na 1 a ceka na cislo 
					if(cur == '-'){
						if(!reading)
							MINUS = 1;
						else
							break;
					}
										
					// pokud cte cislo a narazi na necislo ukonci cteni
					if(reading) break; 
				}
			}
			
			if(MINUS)
				ret = ret * (-1);
			
			return ret;
			break;
		}	
	}
	
	return -1;
}
/**
 * name: doubleval
 * prepis promenne ze sour do dest podle tabulky v zadani
 * @param d:	datova struktura - pro navrat chyby
 * @param dest:	vystupni promenna
 * @param sour: vstupni promenna 
 * @return TRUE kdyz neni chyba, FALSE naopak 
 */
double doubleval(struct data* d, struct symbol_table* sour)
{
	int i;
	
	if(sour == NULL) {
		d->error = 13;
		return 0.0;
	}
		
	switch (sour->var_type) {
		case AST_VAR_NULL:
			return 0.0;
			break;
		case AST_VAR_BOOLEAN:
			if(sour->d.literal_type == AST_LITERAL_TRUE) return 1.0;
			if(sour->d.literal_type == AST_LITERAL_FALSE) return 0.0;
			break;
		case AST_VAR_STRING: //
			i = 0;
			int desetine_misto = 0;
			char cur;
			int readnum = 0;
			int readbot = 0;
			int readexp = 0;
			double desetina_carka = 0;
			double return_doubleval = 0;
			int expo = 0;

			while((cur = sour->d.string_data->str[i]) != '\0') { 
				if(isdigit(cur)){
					readnum = 1;
					if(readbot == 1){
						desetine_misto++;
						desetina_carka = (desetina_carka*10) + cur;
					}else if(readbot == 1)
						expo = (expo*10) + cur;
					else
						return_doubleval = (return_doubleval*10) + cur;


				}
				else if ((cur=='.') && (readnum == 1) && (readbot == 0)){
						readbot = 1;
						readnum = 0;
				} 
				else if ((cur=='e' || cur=='E') && (readnum == 1) && (readbot == 1)&& (readexp == 0)){
						readexp = 1;
						readnum = 0;
				}		
				else {
					if((readbot == 1) || (readexp == 1)){
						d->error = 13;
						return 0.0;
					}
					else {
						if(readnum == 1){
							if(readexp == 0)
								return return_doubleval;
							else if(readbot == 0){
								desetine_misto = pow(10.0, (double)desetine_misto);
								return_doubleval = return_doubleval + (desetina_carka / desetine_misto);
								
								return return_doubleval;
							}
							else
								return_doubleval = pow(return_doubleval,(double)expo);
								
								return return_doubleval;
						}
						return 0.0;
					}
				}
			
				i++;
			}
			break;
		case AST_VAR_DOUBLE:
			return (double)sour->d.numeric_data;
			break;
		case AST_VAR_INT:
			return (double)sour->d.numeric_data;
			break;
		default:
			break;
	}
	
	return 0.0;
}
/**
 * name: strval
 * prepis promenne ze sour do dest podle tabulky v zadani
 * @param d:	datova struktura - pro navrat chyby
 * @param sour: vstupni promenna 
 * @return primo strukturu prevedeneho stringu
 */
string* strval(struct data* d, struct symbol_table sour)
{
	if(d->error) return NULL;

	string* str = NULL;	
	//printf("library:strval:type = %s\n", get_var_type_string(sour.var_type));
	switch(sour.var_type){
		case AST_VAR_NULL:{
			str = NULL;
			break;
		}
		case AST_VAR_BOOLEAN:{
			if(sour.d.literal_type == AST_LITERAL_TRUE)
				str = new_str("1");	
			else
				str = new_str("0");
						
			break;
		}
		case AST_VAR_INT:{
			char* numeric = NULL;
			sprintf(numeric, "%d", (int)sour.d.numeric_data);
			str = new_str(numeric);
		
			break;
		}
		case AST_VAR_DOUBLE:{
			char* numeric = gc_malloc(sizeof(char));
			//printf("library:strval:numeric_data = %g\n", sour.d.numeric_data);
			sprintf(numeric, "%g", sour.d.numeric_data);
			//printf("library:strval:numeric = %s\n", numeric);
			str = new_str(numeric);
			
			break;
		}
		case AST_VAR_STRING:{
			str = new_str(sour.d.string_data->str);
	
			break;
		}
	}
	//printf("strval: %s\n", str->str);
	return str;
}


/**
 * Vrati vyrez ze stringu
 * 
 * 
 */
string* getsubstring(string* s, int start, int end)
{
	if(start >= end) return NULL;
	if(start < 0) return NULL; 

	end   = (end < s->len) ? end : (s->len - 1);

	string* out = NULL;

	for (int i = start; i < end; i++)
	{
		add_char(out, s->str[i]);		
	}

	return out;
}

void getstring(struct symbol_table* dest)
{
	dest->d.string_data = NULL;
	
	char current; 

	while((current = fgetc(stdin)) != '\n'){
		add_char(dest->d.string_data, current);
	}
}


int print(struct data* d, struct ast_node* nd, struct symbol_table** vars, int scope)
{
	//printf("%s\n", get_op_type_string(nd->type));

	switch(nd->type){
		case AST_VAR:{
			struct symbol_table* var;
			char* name = encode_name(nd->d.string_data->str, scope);
			//printf("library:print:var (%s)\n", name);
			
			if(!(var = symbol_search(d->vars, name))){
				//printf("library:print:var %s = not found\n", name);
				return false;
			}
			else {
				string* str = strval(d, *var);
				print_string(str);
			}			
			return true;
		}
		case AST_LITERAL: {
			//printf("library:print:literal = %s\n", get_literal_type_string(nd->literal));
			switch(nd->literal){
				case AST_LITERAL_NULL: 
				case AST_LITERAL_FALSE: {
					//printf("%s", "");
					//printf();
					break;
				}
				case AST_LITERAL_TRUE: {
					print_string(new_str("1"));	
				
					break;
				}
				case AST_LITERAL_NUMERIC: {
					printf("%g", nd->d.numeric_data);
										
					break;
				}
				case AST_LITERAL_STRING: {
					print_string(nd->d.string_data);
										
					break;
				}
			}
			return true;
		}
		case AST_CALL: {
			//printf("library:print:call\n");

			struct symbol_table* output = new_symbol_table();
			if(!ast_call(d, nd, output, scope)){
				//printf("LIBRARY:PRINT:AST_CALL:ERROR IN CALL\n");
				return false;
			}
			
			string* str = strval(d, *output);
			print_string(str);
			
			return true;
		}
		case AST_BINARY_OP:{
			//printf("library:print:bin op\n");
			if(is_logic_op(nd)){
				if(ast_logic_binary_op(d, nd, scope))	
					print_string(new_str("1"));
			}
			else if(is_math_op(nd)){
				struct symbol_table output;
				if(!ast_binary_op(d, nd, &output, scope)){
					//printf("LIBRARY:PRINT:AST_BINARY_OP:MATH ERROR\n");
					return false;
				}
				
				string* s = strval(d, output);
				print_string(s);
			}
			else {
				// concat
				struct symbol_table output;
				if(!ast_concat(d, nd, &output, scope)){
					//printf("LIBRARY:PRINT:AST_BINARY_OP:CONCAT ERROR\n");
					return false;
				}
				
				string* s = strval(d, output);
				print_string(s);
			}

			return true;
		}
		default:{
			//printf("LIBRARY:PRINT:UNAKCEPTABLE TYPE OF PARAMS\n");
			break;
		}
		
	}
	return true;
}

int put_string(struct data* data, struct ast_list* list, struct symbol_table** vars, int scope)
{
	int printed = 0;
	struct symbol_table* kokoti;
	if((kokoti = (struct symbol_table*)gc_malloc(sizeof(struct symbol_table))) == NULL){
		//printf("LIBRARY:PUT_STRING:GC_MALLOC ERROR NOT ALLOCATED\n");
		data->error = 99;
		return false;
	}
	
	while(list != NULL && list->elem != NULL) {
		
		//printf("....\n");
		if(!print(data, list->elem, vars, scope)){
			//printf("LIBRARY:PUT_STRING:PRINT ENDED WITH ERROR\n");
	
			return printed;
		}
		//printf("....\n");
		printed++;
		list = list->next;
	}

	return printed;
}

