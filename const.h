/*
    IFJ 2013

    xmarko07 - Antonín Marko
    xmahne00 - Jakub Mahnert
    xkurit00 - Jakub Kuřitka
    xkubic34 - Martin Kubíček
    xlechp00  - Pali Lech

    "Let the C language die."
 */

#ifndef CONST_H
#define CONST_H

#define E_OK 0		//ok
#define E_LEX 1 	//lexikalni analyza
#define E_SYN 2 	//syntakticka analyza
#define E_SEM 3 	//semanticka chyba
#define E_PAR 4 	//chybejici parametr
#define E_VAR 5 	//nedeklarovana promenna
#define E_DIV 10	//deleni nulou
#define E_VAL 11	//pretypovani na cislo
#define E_TYP 12	//chyba typove kompatibility
#define E_ELS 13	//ostatni chyby
#define E_INT 99	//interni chyba interpretu

#endif
