/*
    IFJ 2013

    xmarko07 - Antonín Marko
    xmahne00 - Jakub Mahnert
    xkurit00 - Jakub Kuřitka
    xkubic34 - Martin Kubíček
    xlechp00  - Pali Lech

    "Let the C language die."
 */

#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include <stdlib.h>
#include <ctype.h>
#include "scanner.h"
#include "errors.h"
#include "const.h"
#include "gc.h"

#define C_C "\x1b[31m"
#define C_R "\x1b[0m"

#define PRINT 1

char* to_lower(char* sour)
{
	int delka = strlen(sour);
	//if ( PRINT ) printf(C_C"scanner:to_lower:strlen(%s) = %d\n"C_R, sour, delka);
	char* ret = (char*)malloc(sizeof(char) * delka);
	strcpy(ret, sour);	
	
	if(ret != NULL) {
		for(int i = 0; i < delka; i++){			
			ret[i] = tolower(ret[i]);
		}
	}
	//if ( PRINT ) printf(C_C"scanner:to_lower:after %s\n"C_R, ret);
	return ret;
}
void print_token_data(struct token* t){

	char* tokens[35] = { "T_ID\0", "T_VAR\0", "T_NUMBER\0", "T_STRING\0", "T_FUNCTION\0", "T_IF\0", "T_ELSE\0", "T_WHILE\0", "T_RETURN\0", "T_LPAR\0", "T_RPAR\0", "T_LBRACE\0", "T_RBRACE\0", "T_NOT\0", "T_PLUS\0", "T_MINUS\0", "T_MULTIPLY\0", "T_DIVIDE\0", "T_LESS\0", "T_MORE\0", "T_LESSOREQ\0", "T_MOREOREQ\0", "T_EQUALS\0", "T_NOTEQUAL\0", "T_STRICTEQ\0", "T_STRICTNOTEQ\0", "T_ASSIGN\0", "T_TRUE\0", "T_FALSE\0", "T_NULL\0", "T_COMMA\0", "T_SEMICOLON\0", "T_CONCATENATE\0", "T_THE_END\0" };

	
	if(t->type != T_NUMBER){
		char* str = (t->d.string_data == NULL) ? "null" : t->d.string_data->str;
		if ( PRINT ) printf(C_C"TOKEN_TYPE: (%d) %s [ \"%s\" | %.2f ]\n"C_R, (int)t->type, tokens[(int)t->type], str, t->d.numeric_data);	
	}
	else
		if ( PRINT ) printf(C_C"TOKEN_TYPE: (%d) %s [ \"%s\" | %.2f ]\n"C_R, (int)t->type, tokens[(int)t->type], "", t->d.numeric_data);
}

void add_token(struct data* d, enum token_type t, string* acc){

	char * end;
	if(t == T_NUMBER){
		d->token_next->type = t;
		double ndata = strtod(acc->str, &end);
		d->token_next->d.numeric_data = ndata;
	}else{
		d->token_next->type = t;
		d->token_next->d.string_data = acc;
	}
}

bool get_next_token(struct data* d){
	//if ( PRINT ) printf(C_C"scanner:get_next_token\n"C_R);

    if(d->token == NULL && d->token_next == NULL){
        //if ( PRINT ) printf(C_C"scanner:first tokens\n"C_R);

        if(!(d->token = (struct token*)gc_malloc(sizeof(struct token))))
        	return false;
        if(!(d->token_next = (struct token*)gc_malloc(sizeof(struct token))))
        	return false;
        
        if(!checkPhp(d)){
            d->error = E_LEX;
            add_token(d, T_THE_END, NULL);
            return false;
        }

        if(!load_lex(d)){
			//chyba v nacitani
			if ( PRINT ) printf(C_C"scanner:get_next_token:load_lex error\n"C_R);
		}
    }

    if(d->token_next->type == T_THE_END){
        d->token->type = d->token_next->type;
    	d->token->d.numeric_data = d->token_next->d.numeric_data;
    	d->token->d.string_data = d->token_next->d.string_data;
    	//if ( PRINT ) printf(C_C"scanner:get_next_token:Next token byl T_THE_END\n"C_R);

    	//return false;
    }else if(d->token->type == T_THE_END){
    	//return false;
    }else{
    	d->token->type = d->token_next->type;
    	d->token->d.numeric_data = d->token_next->d.numeric_data;
    	d->token->d.string_data = d->token_next->d.string_data;

    	//if ( PRINT ) printf(C_C"scanner:Predavam token_next tokenu\t- "C_R);
    	//print_token_data(d->token_next);
    	load_lex(d);
	}
    if ( PRINT ) printf(C_C"scanner:get_next_token:TOKEN:\t - "C_R);print_token_data(d->token);
    if ( PRINT ) printf(C_C"scanner:get_next_token:NEXT_TOKEN:\t - "C_R);print_token_data(d->token_next);
    //if ( PRINT ) printf(C_C"scanner:get_next_token:end\n"C_R);
	return true;
}

bool is_alpha(char c)
{
	// Aplha
	return ((c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z'));
}

bool is_number(char c)
{
	// Cisla
	return ((c >= '0' &&  c <= '9'));
}

bool is_extranumber(char c)
{
	// Cisla
	return ((c >= '0' &&  c <= '9') || (c == 'e') || (c == 'E') || (c == '.'));
}


bool is_operator(char c)
{
	//Operator
	return((c == '*') || (c == '/') || (c == '(') || (c == ')') || (c == '-') || (c == '.') || (c == '+') || (c == '{') || (c == '}'));
}

bool is_extraoperator(char c)
{
	//vracet jestli to je separator
	return((c == '=') || (c == '!') || (c == '<') || (c == '>'));
}

bool is_separator(char c)
{
	//vracet jestli to je separator
	return((c == ';') || (c == ' ') || (c == '/') || (c == '\n') || (c == '\t') || (c == '\r') || (c == '.') || (c == ',') || (c == '(') || (c == ')') || (c == '\'') || (c == '\"') || (c == '{') || (c == '}'));
}


int identificator(struct data* d, char akt)
{
    //if ( PRINT ) printf(C_C"scanner:identificator\n"C_R);

    string * acc;
	if((acc = new_str(NULL)) == NULL) {
		d->error = E_INT;
		if ( PRINT ) printf(C_C"scanner:identificator:error = str\n"C_R);
		add_token(d,T_ID,NULL);
		return false;
	}
	add_char(acc, akt);
     
	char c = getc(d->file);

	while(is_alpha(c) || is_number(c)  || c == '_')
	{
		if(c == EOF)
			break;
		add_char(acc,c);
		c = getc(d->file);
	}
		
	if(c == EOF){
		d->error = E_LEX;
		add_token(d, T_THE_END, NULL);
		return false;
	}
	
	ungetc(c,d->file);
	
	char* lower_case = to_lower(acc->str);
	if ( PRINT ) printf(C_C"scanner:lower_case = %s\n"C_R, lower_case);

	if (!strcmp(lower_case,"if"))
    	add_token(d,T_IF,acc);
    else if (!strcmp(lower_case,"else"))
    	add_token(d,T_ELSE,acc);
    else if (!strcmp(lower_case,"function"))
    	add_token(d,T_FUNCTION,acc);
    else if (!strcmp(lower_case,"while"))
    	add_token(d,T_WHILE,acc);
	else if (!strcmp(lower_case,"return"))
    	add_token(d,T_RETURN,acc);
    else if (!strcmp(lower_case,"false"))
    	add_token(d,T_FALSE,acc);
    else if (!strcmp(lower_case,"true")){   
		if ( PRINT ) printf(C_C"scanner:identificator:Keyword = true\n"C_R);
		if ( PRINT ) printf(C_C"scanner:identificator:Keyword-> = %s\n"C_R,acc->str);
		add_token(d,T_TRUE,acc);
	}
    else if (!strcmp(lower_case,"null"))
    	add_token(d,T_NULL,acc);
    else
    	add_token(d,T_ID,acc);

	free(lower_case);
	return true;
}

int number(struct data* d, char akt)
{
	string * acc;
	if((acc = new_str(NULL)) == NULL){
		d->error = E_INT;
		if ( PRINT ) printf(C_C"scanner:number:error = str\n"C_R);
		add_token(d,T_NUMBER,NULL);
		return false;
	}
	add_char(acc,akt); 
	 
	char c = getc(d->file);
	char last = c;
	int stateError = 0;
	int stateExpo = 0;
	int stateDot = 0;

	while(is_extranumber(c))
	{
		if(c == EOF){
			stateError = 1;
			break;
		}else if(c == '.' && stateDot == 1){
			stateError = 1;
			break;
		}else if((c == '.' && stateDot == 1) || (c == '.' && stateDot == 2)){
			stateError = 1;
			break;
		}else if((c == 'e' && stateDot == 1) || (c == 'E' && stateDot == 1)){
			stateError = 1;
			break;
		}else if((c == 'e' && stateExpo == 1) || (c == 'E' && stateExpo == 1)){
			stateError = 1;
			break;
		}else if(c == '.')
			stateDot = 1;
		else if((c == 'e') || (c == 'E'))
			stateExpo = 1;
		last = c;
		add_char(acc,c);
		c = getc(d->file);
	}
	
	if((last == '.') || (last == 'e') || (last == 'E'))
		stateError = 1;
	if(stateError == 1){
		d->error = E_LEX;
		add_token(d, T_THE_END, NULL);
		return false;
	}else{
		ungetc(c,d->file);
		add_token(d,T_NUMBER,acc);
		return true;
	}
}

int variable(struct data* d)
{
	string * acc;
	if((acc = new_str(NULL)) == NULL){
		d->error = E_INT;
		if ( PRINT ) printf(C_C"scanner:variable:error = str\n"C_R);
		add_token(d,T_VAR,NULL);
		return false;
	}
	
	char c = getc(d->file);

	while(is_alpha(c) || is_number(c) || c == '_')
	{
		if(c == EOF)
			break;
		add_char(acc,c);
		c = getc(d->file);
	}

	if(c == EOF){
		d->error = E_LEX;
		add_token(d, T_THE_END, NULL);
		return false;
	}else{
		ungetc(c,d->file);
		add_token(d,T_VAR,acc);
		return true;
	}
}

int text(struct data* d)
{
	char c = getc(d->file);
	char last = ' ';
	string * acc = new_str(NULL);
	if(!acc){
		d->error = E_INT;
		if ( PRINT ) printf(C_C"scanner:string:error = str\n"C_R);
		add_token(d,T_STRING,NULL);
		return false;
	}

	while(c != '\"' || last == '\\')
	//while(c != '\"' && (last != '\\'))
	{		
		if(c == EOF)
			break;


		last = c;
		add_char(acc,c);

		c = getc(d->file);
		if ( PRINT ) printf(C_C"scanner:string: last: %c %c :c\n"C_R,last,c);
	}


	if(c == EOF){
		d->error = E_LEX;
		add_token(d, T_THE_END, NULL);
		return true;
	}else{
		if(acc == NULL)
			add_token(d,T_STRING,NULL);
		else
			add_token(d,T_STRING,acc);
		return true;
	}
}

void comment_inline(struct data* d)
{
	char c = getc(d->file);
	while((c != '\n') && (c != '\r')) {
		if(c == EOF)
			break;
		c = getc(d->file);
		//if ( PRINT ) printf(C_C"scanner: comment_inline: nacetl jsem %c\n"C_R,c);
	}

	//if ( PRINT ) printf(C_C"scanner: comment_inline: posledni znak = %c\n"C_R,c);
	if(c == EOF){
		d->error = E_LEX;
		add_token(d, T_THE_END, NULL);
		return;
	}
	else
		load_lex(d);
}

void comment_multiline(struct data* d) /*UPRAVIT ABY MEL TONIK RADOST*/
{
	char c = getc(d->file);
	char last = ' ';
	while((c != '/') || (last != '*'))
	{
		if(c == EOF)
			break;
		//if ( PRINT ) printf(C_C"scanner: comment_multinline: c:%c last:%c\n"C_R,c,last);
		last = c;
		c = getc(d->file);
		//if ( PRINT ) printf(C_C"scanner: comment_multinline: c:%c last:%c\n",c,last);
	}
	
	//if ( PRINT ) printf(C_C"scanner: comment_multinline: posledni znak c:%c last:%c\n"C_R,c,last);
	if(c == EOF){
		d->error = E_LEX;
		add_token(d, T_THE_END, NULL);
		return;
	}else
		load_lex(d);
}

int operator(struct data* d, char akt)
{
	string * acc = new_str(NULL);
	if(!acc){
		d->error = E_INT;
		if ( PRINT ) printf(C_C"scanner:operator:error = str\n"C_R);
		add_token(d,T_ID,NULL);
		return false;
	}
	add_char(acc,akt);
	
	if (!strcmp(acc->str,"-"))
    	add_token(d,T_MINUS,acc);
    else if (!strcmp(acc->str,"+"))
    	add_token(d,T_PLUS,acc);
    else if (!strcmp(acc->str,"*"))
    	add_token(d,T_MULTIPLY,acc);
    else if (!strcmp(acc->str,"/"))
    	add_token(d,T_DIVIDE,acc);
    else if (!strcmp(acc->str,"{"))
    	add_token(d,T_LBRACE,acc);
    else if (!strcmp(acc->str,"}"))
    	add_token(d,T_RBRACE,acc);
    else if (!strcmp(acc->str,"("))
    	add_token(d,T_LPAR,acc);
    else if (!strcmp(acc->str,")"))
    	add_token(d,T_RPAR,acc);
    else if (!strcmp(acc->str,"."))
    	add_token(d,T_CONCATENATE,acc);

	return true;
}

int extraoperator(struct data* d, char akt){ 
	string * acc = new_str(NULL);
	if(!acc){
		d->error = E_INT;
		if ( PRINT ) printf(C_C"scanner:extraoperator:error = str\n"C_R);
		add_token(d,T_ID,NULL);
		return false;
	}
	add_char(acc,akt);
	char pom = getc(d->file);


	if(!is_extraoperator(pom)){
		if (!strcmp(acc->str,"<")){
			add_token(d,T_LESS,acc);
		}
        else if (!strcmp(acc->str,">")){
            //if ( PRINT ) printf(C_C"scanner: extraoperator: Jsme v more\n"C_R);
            add_token(d,T_MORE,acc);
        }
        else if (!strcmp(acc->str,"="))
                add_token(d,T_ASSIGN,acc);

        ungetc(pom,d->file);
        //if ( PRINT ) printf(C_C"scanner: extraoperator: Koncim s hodnotou: %c\n"C_R,pom);
		return true;
	}else{
		add_char(acc,pom);
		if(!strcmp(acc->str,"!=") || !strcmp(acc->str,"==")){
			pom = getc(d->file);
			if(pom == '='){
				add_char(acc,pom);
        		if (!strcmp(acc->str,"==="))
        			add_token(d,T_STRICTEQ,acc);
        		else if (!strcmp(acc->str,"!=="))
                	add_token(d,T_STRICTNOTEQ,acc);
				return true;
			}else{
				if (!strcmp(acc->str,"=="))
					add_token(d,T_EQUALS,acc);
        		else if (!strcmp(acc->str,"!="))
        			add_token(d,T_NOTEQUAL,acc);
        		else if (!strcmp(acc->str,"<="))
        			add_token(d,T_LESSOREQ,acc);
        		else if (!strcmp(acc->str,">="))
        			add_token(d,T_MOREOREQ,acc);
				ungetc(pom,d->file);
				return true;
			}
		}else{
			if (!strcmp(acc->str,"=="))
                add_token(d,T_EQUALS,acc);
        	else if (!strcmp(acc->str,"!="))
                add_token(d,T_NOTEQUAL,acc);
        	else if (!strcmp(acc->str,"<="))
                add_token(d,T_LESSOREQ,acc);
        	else if (!strcmp(acc->str,">="))
                add_token(d,T_MOREOREQ,acc);
			return true; 
		}
	}
}


int semicomma(struct data* d, char akt)
{
	string * acc = new_str(NULL);
	if(!acc){
		d->error = E_INT;
		if ( PRINT ) printf(C_C"scanner:operator:error = str\n"C_R);
		add_token(d,T_ID,NULL);
		return false;
	}
	add_char(acc,akt);
	
	if (!strcmp(acc->str,";"))
    	add_token(d,T_SEMICOLON,acc);
    else if (!strcmp(acc->str,","))
    	add_token(d,T_COMMA,acc);

	return true;
}

int load_lex(struct data* d)
{		

	char cur;
    cur = getc(d->file);

    //if ( PRINT ) printf(C_C"scanner:load_lex:char: %c\n", cur);

    if(cur == EOF) {
    	//if ( PRINT ) printf(C_C"scanner:EOF\n"C_R);
    	string * acc = new_str(NULL);
		if(!acc){
			d->error = E_INT;
			if ( PRINT ) printf(C_C"scanner:load_lex:error = str\n"C_R);
			add_token(d,T_THE_END,NULL);
			return false;
		}
        add_token(d, T_THE_END, acc);
		return true;
    }
    else if(cur == '\n' || cur == ' ' || cur == '\r'){
    	if ( PRINT ) printf(C_C"scanner:load_lex:Prazdny znak\n"C_R);
        return load_lex(d);
    }
    else if(is_alpha(cur))		// state text
	{
		return identificator(d,cur);
	}
	else if(is_number(cur))		// state number
	{	
		return number(d,cur);
	}
	else if(cur == '$')		// state var
	{	
		return variable(d);
	}
	else if(cur == '\"')	// state string
	{
		return text(d);
	}
	else if(cur == '/')
	{
		char pom = getc(d->file);
		
		if(pom == '/')		// state inline
		{
			comment_inline(d);
		}
		else if(pom == '*')
		{
			comment_multiline(d);	// state multiline
		}
		else
		{
			ungetc(pom,d->file);
			return operator(d,cur);
		}
	}
	else if(is_operator(cur))		// state operator	
	{
		return operator(d,cur);
	}
	else if(is_extraoperator(cur))		// state operator	
	{
		return extraoperator(d,cur);
	}
	else if((cur == ';') || (cur == ','))
	{
		return semicomma(d,cur);
	}
	else{
        return load_lex(d);
	}

	return true;
}

bool checkPhp(struct data* d){
    char start[5]="<?php";
    char cur;
    int i;
    for (i = 0; i < 5; i++){
        if((cur = getc(d->file)) != start[i] || cur == EOF)
            return false;
    }
    return true;
}

