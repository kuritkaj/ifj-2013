/*
    IFJ 2013

    xmarko07 - Antonín Marko
    xmahne00 - Jakub Mahnert
    xkurit00 - Jakub Kuřitka
    xkubic34 - Martin Kubíček
    xlechp00  - Pali Lech

    "Let the C language die."
 */

//jednoducha knihovna pro praci s nekonecne dlouhymi retezci
#include <string.h>
#include <stdio.h>
#include <stdbool.h>
#include "str.h"
#include "gc.h"

#define C_B "\x1b[34m"
#define C_R "\x1b[0m"
#define STR_LEN_INC 8
// konstanta STR_LEN_INC udava, na kolik bytu provedeme pocatecni alokaci pameti
// pokud nacitame retezec znak po znaku, pamet se postupne bude alkokovat na
// nasobky tohoto cisla 


string* new_str(char* txt)
// funkce vytvori novy retezec
{
	//printf("str:new_str\n");
        
	string* s = (string*)gc_malloc(sizeof(string));
	if(!s) 
	{
		//printf(C_B "str:new_str:s == NULL\n" C_R);
		return NULL;
	}
	//printf("str:string:pointer:%p\n", (void*)s);

    if((s->str = (char*)gc_malloc(sizeof(char) * STR_LEN_INC)) == NULL) {
        printf(C_B "str:new_str:s->str gc_malloc error\n" C_R);
        return NULL;
    }


	if(!txt){	
		//printf(C_B "str:txt==NULL\n" C_R);
		s->str[0] = '\0';
		s->len = 0;
		s->alloc_size = STR_LEN_INC;
	}
	else {
		//printf(C_B "str:txt<>NULL\n" C_R);
		s->len = 0;
		convert_chars(s, txt);
	}	
	
	//printf(C_B "str:new_str:string_data = %s\n" C_R, s->str);
	return s;
}

void clear_str(string*s)
// funkce vymaze obsah retezce
{
   //printf(C_B "str:clear_str\n" C_R);

   s->str[0] = '\0';
   s->len = 0;
}

string* cat_str(string* str1, string* str2)
// spoji dva stringy a vrati spojeny
{
   string* str = new_str(str1->str);
   convert_chars(str, str2->str);
   
   return str;
}

//ISSUE: co ta realokace? bude to v pohode??? DENNO LEL 
int add_char(string* s, char c)
// prida na konec retezce jeden znak
{
   //printf(C_B "str:add_char:%p := %c\n" C_R, (void*)s, c);

   if (s->len + 1 >= s->alloc_size)
   {
      //printf(C_B "str:add_char:before realloc\n" C_R);
      // pamet nestaci, je potreba provest realokaci
      if ((s->str = (char*)gc_realloc(s->str, (s->len + STR_LEN_INC) * sizeof(char))) == NULL)
         return false;
      s->alloc_size = s->len + STR_LEN_INC;
   }
   	
	//printf(C_B "str:add_char:before copy data (s->len=%d)\n" C_R, s->len);
   s->str[s->len] = c;
   s->len++;
   s->str[s->len] = '\0';
   //printf(C_B "str:add_char:after copy data\n" C_R);
   return true;
}

int convert_chars(string* s, char* chs)
// skonvertuje pole znaku do struktury
{
	//printf(C_B "str:convert_chars:start\n" C_R);
	if(s != NULL && chs != NULL){
		int i = 0;

		while(chs[i] != '\0'){
			add_char(s, chs[i]);
			//printf(C_B "str:convert_chars:adding\t%c\n" C_R, chs[i]);
			i++;
		}	

		//printf(C_B "str:convert_chars:end\n" C_R);	
		return true;
	}

	//printf(C_B "str:convert_chars:end\n" C_R);
	return false;
}

int equals(string* s1, string* s2)
// zjisti jestli jsou predane stringy totozne
{
	if(s1->len == s2->len) {
		for(int i = 0; i < s1->len; i++) {
			if(s1->str[i] != s2->str[i])
				return 0; //nalezen rozdil
		}
		
		//probehlo v poradku		
		return true;
	}
	
	//nejsou stejne delky = rozdilne stringy
	return false;
}
