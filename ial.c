/*
    IFJ 2013

    xmarko07 - Antonín Marko
    xmahne00 - Jakub Mahnert
    xkurit00 - Jakub Kuřitka
    xkubic34 - Martin Kubíček
    xlechp00  - Pali Lech

    "Let the C language die."
 */

#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include "str.h"
#include "gc.h"
#include "common.h"
#include "ast.h"
#include "ial.h"

/**
 * vytvori pattern tabulku pro pouziti v KMP algoritmu
 * @param  string retezec, ve kterem je vyhledavano
 * @return        navraci pole  integeru se vzorkem
 */
int * build_pattern_table(string str)
{
	int * table;
	if (!(table = (int*) gc_malloc(sizeof(int) * str.len)))
		return NULL;
	int k = -1;
	table[0] = k;

	for(int i = 1; i < str.len; i++) {
		while((k > -1) && (str.str[k+1] != str.str[i]))
			k = table[k];
		if(str.str[k+1] == str.str[k])
			k++;
		table[k] = k;
	}
	return table;
}

/**
 * knuth-morris-prattuv algoritmus pro hledani substringu
 * @param  field  string, nad kterym hledani provadime
 * @param  pattern vzorek, ktery hledame
 * @return         vraci index, kde byl nalezen substring
 */
int find_string(string * field, string * pattern)
{
	int * table = build_pattern_table(*pattern);
	int k = -1;
	if (!table)
		return -1;
	for (int i = 0; i < field->len; i++) {
		while ((k > -1) && (pattern->str[k+1] != field->str[i]))
			k = table[k];
		if (field->str[i] == field->str[k+1])
			k++;
		if (k == pattern->len - 1) {
			return i-k;
		}
	}
	return -1;
}
/**
 * Funkce pro serazeni pole znaku
 * vyuziva algoritmu merge-sort
 * @param  source, struktura string obsahujici text k serazeni
 * @return         vraci strukturu se serazenymi znaky
 */
string* sort_string(string* source)
{
	string* aux = new_str(NULL);
    
	for(int i = 0; i < source->len; i++){
		add_char(aux, ' ');
	}	
	
	merge_sort(source, aux, 0, source->len-1);

    *(aux->str) = *(source->str);

    for(int i = 0; i < source->len; i++){
        source->str[source->len - i - 1] = aux->str[i];
    }


	
	return source;
}

/** 
 * Razeni slevanim (od nejvyssiho)
 * @param array pole k serazeni
 * @param aux pomocne pole stejne delky jako array
 * @param left prvni index na ktery se smi sahnout
 * @param right posledni index, na ktery se smi sahnout
 */
void merge_sort(string * array, string * aux, int left, int right) {
    if (left == right) return; 
    int middle_index = (left + right)/2;
    merge_sort(array, aux, left, middle_index);
    merge_sort(array, aux, middle_index + 1, right);
    merge(array, aux, left, right);
  
    for (int i = left; i <= right; i++) {
        array->str[i] = aux->str[i];
    }
}    

/**
 * Slevani pro Merge sort
 * @param array pole k serazeni
 * @param aux pomocne pole (stejne velikosti jako razene)
 * @param left prvni index, na ktery smim sahnout
 * @param right posledni index, na ktery smim sahnout
 */
void merge(string * array, string * aux, int left, int right) {
    int middle_index = (left + right)/2;
    int left_index = left; 
    int right_index = middle_index + 1;
    int aux_index = left;
    while (left_index <= middle_index && right_index <= right) {
        if (array->str[left_index] >= array->str[right_index]) {
            aux->str[aux_index] = array->str[left_index++];
        } else {
            aux->str[aux_index] = array->str[right_index++];
        }
        aux_index++;
    }
    while (left_index <= middle_index) {
        aux->str[aux_index] = array->str[left_index++];
        aux_index++;
    }
    while (right_index <= right) {
        aux->str[aux_index] = array->str[right_index++];
        aux_index++;
    }
}    
