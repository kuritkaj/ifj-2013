/*
    IFJ 2013

    xmarko07 - Antonín Marko
    xmahne00 - Jakub Mahnert
    xkurit00 - Jakub Kuřitka
    xkubic34 - Martin Kubíček
    xlechp00  - Pali Lech

    "Let the C language die."
 */

#include <stdio.h>
#include <stdlib.h>
#include "stack.h"
#include "gc.h"

void stack_init(struct stack* s)
{
	// Inicializace zasobniku
	s->head = NULL;
	s->node = NULL;
}
int stack_empty(struct stack* s)
{
	//kontrola
	return (s->head == NULL);
}
void stack_free(struct stack* s)
{
	// Uvolneni vsech prvku zasobniku pomoci pop
	while(!stack_empty(s))
		stack_pop(s);
		
	s = NULL;
}
void stack_push(struct stack* s, string* arr)
{
	// Pushnuti do zasobniku
	if((s->node = (struct element*) gc_malloc(sizeof(struct element))) == NULL) 
		return;
	
	s->node->value = arr;
	s->node->next = s->head;
	s->head = s->node;	
}
string* stack_pop(struct stack* s)
{
	// Popnuti ze zasobniku
	if(s->head == NULL)
		return NULL;
		
	string* tmp = s->head->value;
	s->node = s->head;
	s->head = s->head->next;
	s->node = NULL;
	
	return tmp;
}
string* stack_top(struct stack* s)
{
	// Ziskani vrcholu zasobniku
	if (s->head) {
		return s->head->value;	
	} else
		return NULL;
}
