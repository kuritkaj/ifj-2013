/*
    IFJ 2013

    xmarko07 - Antonín Marko
    xmahne00 - Jakub Mahnert
    xkurit00 - Jakub Kuřitka
    xkubic34 - Martin Kubíček
    xlechp00  - Pali Lech

    "Let the C language die."
 */

#include <stdio.h>
#include "gc.h"

#define ALLOC_SIZE 20 //kolik budeme alokovat a pridavat k alokaci v gc.refs

//hlaicky
void gc_realloc_inner();

struct gc
{
    int current_count; // pocet prvku v refs
    int allocated_count; // pocet prvku pro ktere je naalokovano refs
    void** refs; // reference, ktere se budou mazat     
};

/**
 * Udrzuje v sobe aktualni stav gc
 */
struct gc gc_data;

/**
 * Inicializuje GC vraci false v pripade chyby
 * 
 * @return bool error
 */
bool gc_init()
{
    gc_data.current_count = 0;
    gc_data.allocated_count = ALLOC_SIZE;
    gc_data.refs = malloc(sizeof(void*) * ALLOC_SIZE);

    return (gc_data.refs != NULL);
}

void* gc_realloc(void* ref, size_t size)
{
    //printf("gc:gc_realloc:%lu\n", size);

    ref = realloc(ref, size);
    return ref;
}

/**
 * Prida prostor pro gc
 */
void gc_realloc_inner()
{
    gc_data.allocated_count += ALLOC_SIZE;
    gc_data.refs = realloc(gc_data.refs, (gc_data.allocated_count * sizeof(void*)));
}


/**
 * Prida pointer do pole referenci, ktere budou pri gc_clear mazany pomoci free
 * 
 * @param void* ref co ma byt pridano
 *
 * @return void* ref nebo NULL v pripade aloacni chyby 
 */
void* gc_add(void* ref)
{
    //printf("gc:gc_add\n");

    if (ref == NULL) return NULL;

    if(gc_data.allocated_count == gc_data.current_count) gc_realloc_inner();

    if(gc_data.refs == NULL) return NULL;

    //printf("gc:current_count:%d\n", gc_data.current_count);

    gc_data.refs[gc_data.current_count++] = ref;

    //printf("gc:pointer:%p\n", ref);

    //printf("gc:gc_add_end\n");
    return ref;
}

/**
 * Naalokuje misto na halde, prida ho do referenci gc a vrati pointer nebo NULL v pripade chyby
 * 
 * @param size
 *
 * @return void*
 */
void* gc_malloc(size_t size)
{
    //printf("gc:gc_malloc:%lu\n", size);

    return gc_add(malloc(size));
}


void gc_free(void* p)
{
    //printf("gc:gc_free: %p\n", p);

    if(!p) return;
    for(int i = 0; i < gc_data.current_count; i++){
        if(p != gc_data.refs[i]) continue;
        //printf("gc:gc_free:pointer found freeing\n");
        free(gc_data.refs[i]);
        gc_data.refs[i] = NULL;
        //rintf("\t\tGC_FREE: at index %d\n", i);
        // printf("gc:gc_free:internal pointer is now NULL\n");
        break;
    }
}

/**
 * Vycisti vsechny reference pomoci free. Pokud chceme po teto fci pouzivat gc, musime zavolat gc_init !
 *
 * @return void
 */
void gc_clear()
{
    //printf("gc:gc_clear: %d\n", gc_data.current_count);

    for (int i = 0; i < gc_data.current_count; i++)
    {
        //printf("gc:clear:clearing:%d -> %p\n", i, gc_data.refs[i]);
        
        //if(i % 50 == 0) printf("cleared references: %d\n", i);
        if(!gc_data.refs[i]) 
            continue; continue;

        printf("%p\n", (void*)gc_data.refs[i]);
        
        free(gc_data.refs[i]);
    }

    free(gc_data.refs);
    gc_data.current_count = 0;
    gc_data.allocated_count = 0;

   //printf("gc:gc_clear_end\n");
}

int gc_count()
{
    return gc_data.current_count;
}

