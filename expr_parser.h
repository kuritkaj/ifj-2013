/*
    IFJ 2013

    xmarko07 - Antonín Marko
    xmahne00 - Jakub Mahnert
    xkurit00 - Jakub Kuřitka
    xkubic34 - Martin Kubíček
    xlechp00  - Pali Lech

    "Let the C language die."
 */

#include "common.h"
#include "ast.h"
#include "stack.h"

#ifndef EXPR_PARSER_H
#define EXPR_PARSER_H

#define CYAN    "\x1b[36m"
#define RESET   "\x1b[0m"


struct expr_list
{
    struct ast_node * first;
    struct ast_node * current;
};

int is_expression(struct data * d);
int is_end(struct data * data, int * paretheses_count);
void check_validity(struct data * d, int * validity);
void until_left_par (struct stack * s, struct expr_list * list);
void do_operation (struct stack * s, struct data * d, struct expr_list * list);
void add_to_list(struct expr_list * list, struct ast_node * node);
struct ast_node * parse_concatenation(struct data * d);
struct ast_node * parse_boolean (struct data * d);
struct expr_list * init_list();
struct expr_list * infix2postfix(struct data * d, int * continues);
struct ast_node * new_op_node(string * op);
struct ast_node * new_fn_call_node(struct data * d);
struct ast_node * new_var_node(struct data * d);
struct ast_node * new_string_node(struct data * d);
struct ast_node * list2ast_tree(struct expr_list * list);
void clean_tree(struct ast_node * radix);

#endif
