/*
    IFJ 2013

    xmarko07 - Antonín Marko
    xmahne00 - Jakub Mahnert
    xkurit00 - Jakub Kuřitka
    xkubic34 - Martin Kubíček
    xlechp00  - Pali Lech

    "Let the C language die."
 */

#include "s_table.h"
#include "common.h"
#include "gc.h"
#include <string.h>

#define CB "\x1b[32m"
#define CR "\x1b[0m"

struct symbol_table* new_symbol_table()
{
	//printf(CB "symbol_table:new_symbol_table:start\n" CR);
	struct symbol_table* ret = (struct symbol_table*)gc_malloc(sizeof(struct symbol_table));
	ret->var_type = AST_VAR_NULL;
	ret->d.numeric_data = 0.0;
	ret->d.string_data = NULL;
	ret->d.literal_type = AST_LITERAL_NULL;
	//printf(CB "symbol_table:new_symbol_table:ret = %p\n" CR, (void*)ret);
	//printf(CB "symbol_table:new_symbol_table:end\n" CR);	
	return ret;
}
struct symbol_table_data* new_symbol_table_data()
{
	struct symbol_table_data* sdata;

	if((sdata = (struct symbol_table_data*)gc_malloc(sizeof(struct symbol_table_data))) == NULL){
		return NULL;
	}
	
	if((sdata->string_data = (string*)gc_malloc(sizeof(string))) == NULL){
		return NULL;
	}

	return sdata;
}

struct symbol_table* new_knot(struct data* d, char* key, enum ast_var_type type, struct symbol_table_data data)
{
    struct symbol_table* new_node = (struct symbol_table*)gc_malloc(sizeof(struct symbol_table));
    if(!new_node){
		//printf(CB "SYMBOL_TABLE:NEW_KNOT:GC_MALLOC NOT ALLOCATED\n" CR);
		d->error = 99;
		return NULL;
	}    

    new_node->key = key;
    new_node->var_type = type;
    new_node->d = data;
    new_node->left = NULL;
    new_node->right = NULL;
        

	//printf(CB "symbol_table:new_knot: ret = \t\"%s\"  pointer %p\n" CR, new_node->key, (void*)new_node);
    return new_node;
}

void symbol_init(struct symbol_table** root) 
{
	//printf(CB "symbol_table:symbol_init:start\n" CR);
	*root = new_symbol_table();
	//printf(CB "symbol_table:symbol_init:root = %p\n" CR, (void*)root);
	*root = NULL;
	//printf(CB "symbol_table:symbol_init:end\n" CR);
}	

int symbol_copy(struct data* d, struct symbol_table* original, struct symbol_table* copy)
{
	//printf(CB "symbol_table:symbol_copy:start\n" CR);
	if(original != NULL){
		copy = original;
		copy->d.string_data = original->d.string_data;
		

		//printf("symbol_table:symbol_copy:%s=%g:%d:%s\n", copy->key, copy->d.numeric_data, copy->d.literal_type, (copy->d.string_data == NULL)?"NULL":copy->d.string_data->str);
	
		symbol_copy(d, original->left, copy->left);
		symbol_copy(d, original->right, copy->right);			
	}
	
	return true;
}

struct symbol_table* symbol_search(struct symbol_table* root, char* key)	
{		
	//printf(CB "symbol_table:symbol_search:searching %s\n" CR, key);	
	if(root != NULL) {
		//kdyz je 0+ pak ma jit do praveho uzlu
		//kdyz je 0- pak ma jit do leveho uzlu
		int key_direction = strcmp(root->key, key);
		
		if(key_direction > 0)
			return symbol_search(root->right, key);
		else if(key_direction < 0)
			return symbol_search(root->left, key);
		else {
			//printf(CB "symbol_table:symbol_search:%s found\n" CR, key);	
			return root;
		}
	}
	
	return NULL;
} 
struct symbol_table symbol_search_data(struct symbol_table* root, char* key, int* is_found)	
{		
	//printf(CB "symbol_table:symbol_search_data:searching %s\n" CR, key);	
	if(root != NULL) {
		//kdyz je 0+ pak ma jit do praveho uzlu
		//kdyz je 0- pak ma jit do leveho uzlu
		int key_direction = strcmp(root->key, key);
		
		if(key_direction > 0)
			return symbol_search_data(root->right, key, is_found);
		else if(key_direction < 0)
			return symbol_search_data(root->left, key, is_found);
		else {
			struct symbol_table stable;
			stable.key = root->key;
			stable.var_type = root->var_type;
			stable.d = root->d;
			stable.statement = root->statement;
			stable.params = root->params;
			
			stable.left = root->left;
			stable.right = root->right;
			*is_found = 1;

			//printf(CB "symbol_table:symbol_search_data:found %s\n" CR, key);
			return stable;
		}
	}
	
	struct symbol_table null;
	*is_found = 0;
	return null;
} 
struct symbol_table* symbol_insert(struct data* d, struct symbol_table** root, char* key, enum ast_var_type type, struct symbol_table_data data)	
{	
	//printf(CB "symbol_table:symbol_insert:start\n" CR);

	//printf(CB "symbol_table:symbol_insert:root = %p\n" CR, (void*)*root);
	struct symbol_table* item = *root;
	//printf(CB "symbol_table:symbol_insert:item = %p\n" CR, (void*)item);
	if(item != NULL){
		int key_direction = strcmp(item->key, key);

		//printf(CB "symbol_table:symboL_insert:key_direction = %d\n" CR, key_direction);		
		if(key_direction > 0)
			return symbol_insert(d, &item->right, key, type, data);
		else if(key_direction < 0)
			return symbol_insert(d, &item->left, key, type, data);
		else {
			//printf(CB "symbol_table:symboL_insert:aktualizace dat\n" CR);

			// aktualizace 
			item->d = data;
			return item;
		}
	}
	else {
		//printf(CB "symbol_table:symboL_insert:adding new node\n" CR);
		item = new_knot(d, key, type, data);
		//printf(CB "symbol_table:symbol_insert:added node = %p (%s)\n" CR, (void*)item, key);

		*root = item;
		return item;
	}

	//printf(CB "symbol_table: returning item pointer\n" CR);
	return NULL;
	//printf(CB "symbol_table: end\n" CR);
}

void symbol_table_dispose(struct symbol_table** root) 
{	
	/*
	 * smazani tabulky rekurzivne 
	 */
	//printf(CB "symbol_table:symbol_table_dispose:start\n" CR);			
	if(*root != NULL) {
		
		//rekurzivne nicit pravou a levou vetev
		symbol_table_dispose(&(*root)->left);
		symbol_table_dispose(&(*root)->right);
    
		//uvolnit aktualni uzel
		free(*root);
		*root = NULL;
	}
	//printf(CB "symbol_table:symbol_table_dispose:end\n" CR);			
}

void replace_by_rightmost(struct symbol_table * replaced, struct symbol_table ** root)
{
	struct symbol_table * temp = NULL;
	if ((*root)->right)
		replace_by_rightmost(replaced, &(*root)->right);
	else {
		replaced->key = (*root)->key;
		replaced->var_type = (*root)->var_type;
		replaced->d = (*root)->d;	
		*(replaced->statement) = *((*root)->statement);
		*(replaced->params) = *((*root)->params);
		temp = *root;
		*root = (*root)->left;
		gc_free(temp);
	}
}

void symbol_table_delete(struct symbol_table ** root, char * key)
{
	//printf("symbol_table_delete:start\n");
	struct symbol_table * temp = NULL;
	if (*root) {
		//printf("symbol_table_delete:root not null\n");
		if (strcmp((*root)->key, key) < 0) {
			//printf("symbol_table_delete: left dir\n");
			symbol_table_delete(&((*root)->left), key);
		} else if (strcmp((*root)->key, key) > 0) {
			//printf("symbol_table_delete: right dir\n");
			symbol_table_delete(&((*root)->right), key);
		} else {
			//printf("symbol_table_delete:this\n");
			temp = *root;
			if (!(temp->right)) {
				//printf("symbol_table_delete:right temp null\n");
				*root = temp->left;
				//printf("symbol_table_delete:freee\n");
				free(temp);
				//printf("symbol_table_delete:done\n");
			}
			else if (!(temp->left)) {
				//printf("symbol_table_delete:left temp null\n");
				*root = temp->right;
				//printf("symbol_table_delete:freee\n");
				free(temp);
				//printf("symbol_table_delete:done\n");
			} else {
				//printf("symbol_table_delete:replace\n");
				replace_by_rightmost(temp, &(temp->left));
			}
		}
	}
}
