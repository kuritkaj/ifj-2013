/*
    IFJ 2013

    xmarko07 - Antonín Marko
    xmahne00 - Jakub Mahnert
    xkurit00 - Jakub Kuřitka
    xkubic34 - Martin Kubíček
    xlechp00  - Pali Lech

    "Let the C language die."
 */

#ifndef LIBRARY_H
#define LIBRARY_H

int str_len(string * source);
int boolval(struct data* d, struct symbol_table* sour);
int intval(struct data* d, struct symbol_table* sour);
double doubleval(struct data* d, struct symbol_table* sour);
string* strval(struct data* d, struct symbol_table sour);
string* getsubstring(string* s, int start, int end);
void getstring(struct symbol_table* dest);
int put_string(struct data* data, struct ast_list * list, struct symbol_table** vars, int scope);

#endif
