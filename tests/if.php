<?php

$var = 456;
if_control($var);


function if_control($x){
	$cont = 156;

	if($x > $cont){
		put_string($x, " je vetsi nez ", $cont, "\n");
	}else{
		put_string($x, " neni vetsi nez ", $cont, "\n");
	}

	if($x < $cont){
		put_string($x, " je mensi nez ", $cont, "\n");
	}else{
		put_string($x, " neni mensi nez ", $cont, "\n");
	}

	if($x >= $cont){
		put_string($x, " je vetsi rovno nez ", $cont, "\n");
	}else{
		put_string($x, " neni vetsi rovno nez ", $cont, "\n");
	}

	if($x <= $cont){
		put_string($x, " je mensi rovno nez ", $cont, "\n");
	}else{
		put_string($x, " neni mensi rovno nez ", $cont, "\n");
	}

	if($x == $cont){
		put_string($x, " je stejny jako ", $cont, "\n");
	}else{
		put_string($x, " neni stejny jako ", $cont, "\n");
	}

	if($x != $cont){
		put_string($x, " neni stejny jako ", $cont, "\n");
	}else{
		put_string($x, " je stejny jako ", $cont, "\n");
	}
}
