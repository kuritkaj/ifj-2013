/*
    IFJ 2013

    xmarko07 - Antonín Marko
    xmahne00 - Jakub Mahnert
    xkurit00 - Jakub Kuřitka
    xkubic34 - Martin Kubíček
    xlechp00  - Pali Lech

    "Let the C language die."
 */

#include "str.h"

#ifndef STACK_H
#define STACK_H

struct element{
	string* value;
	struct element* next;
};
struct stack{
	struct element* head;
	struct element* node;
};

int stack_empty(struct stack* s);
void stack_init(struct stack* s);
void stack_free(struct stack* s);
void stack_push(struct stack* s, string* ch);
string* stack_pop(struct stack* s);
string* stack_top(struct stack* s);

#endif
