/*
    IFJ 2013

    xmarko07 - Antonín Marko
    xmahne00 - Jakub Mahnert
    xkurit00 - Jakub Kuřitka
    xkubic34 - Martin Kubíček
    xlechp00  - Pali Lech

    "Let the C language die."
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "const.h"
#include "gc.h"
#include "common.h"
#include "parser.h"
#include "interpret.h"
#include "ast.h"

#define PRINT 0

int save_the_whales(struct data* d)
{
    if ( PRINT ) printf("main:save_the_whales\n");
    fclose(d->file);
    gc_clear();
    if ( PRINT ) printf("main:save_the_whales:error:%d\n", d->error);
    return d->error;
}
/*
 * 
 */
int main(int argc, char** argv)
{
    if ( PRINT ) printf("======================================================\n");
    if(argc < 2 || argc > 2) return E_PAR;

    if ( PRINT ) printf("main:gc_init\n");
    // initialize whales collector
    gc_init();

    if ( PRINT ) printf("main:alokacedat\n");
    struct data* d = gc_malloc(sizeof(struct data));

    if ( PRINT ) printf("main:otevreni souboru\n");
    
    d->error = E_OK;
    d->token = NULL;
    d->token_next = NULL;

    //otevrit soubor a ulozit do dat
    d->file = fopen(argv[1], "r");

    if ( PRINT ) printf("main:check souboru\n");
    if(d->file == NULL){
        gc_clear();
        return E_ELS;
    }

    if ( PRINT ) printf("main:parser_init\n");
    //parser
    parser_init(d);

    if ( PRINT ) printf("main:parser_run\n");
    if(!parser_run(d)){
        if ( PRINT ) printf("main:parser_run:ERROR\n");
        // chyba
        return save_the_whales(d);
    }

    if ( PRINT ) printf("main:interpret\n");
    if(!interpret(d)){
        if ( PRINT ) printf("main:interpret:ERROR\n");
        // chyba
        return save_the_whales(d);
    }

    // free the whales
    return save_the_whales(d);
}
