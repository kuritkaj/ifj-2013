/*
    IFJ 2013

    xmarko07 - Antonín Marko
    xmahne00 - Jakub Mahnert
    xkurit00 - Jakub Kuřitka
    xkubic34 - Martin Kubíček
    xlechp00  - Pali Lech

    "Let the C language die."
 */

#include <stdio.h>
#include <stdlib.h>
#include "const.h"
#include "gc.h"

void kill_me(int code)
{
	printf("errors:kill_me\n");
	
	//ukonci program s hlaskou na chybovem vystupu 
	if(code == E_LEX)
		fprintf(stderr,"Chyba v programu v ramci lexikalni analyzy (chybna struktura aktualniho lexemu)");
	else if(code == E_SYN)
		fprintf(stderr, "Chyba v programu v ramci syntakticke analyzy (chybna syntaxe struktury programu)");
	else if(code == E_SEM)
		fprintf(stderr, "Semanticka chyba v programu - nedefinovana funkce, pokus o redefinici funkce");
	else if(code == E_PAR)
		fprintf(stderr, "Semanticka/behova chyba v programu - chybejci parametr pri volani funkce");
	else if(code == E_VAR)
		fprintf(stderr, "Semanticka/behova chyba v programu - nedeklarovana promenna");
	else if(code == E_DIV)
		fprintf(stderr, "Semanticka/behova chyba deleni nulou");
	else if(code == E_VAL)
		fprintf(stderr, "Semanticka/behova chyba pri pretypovani na cislo (funkce doubleval)");
	else if(code == E_TYP)
		fprintf(stderr, "Semanticka/behova chyba typove kompatibility v aritmetickych a relacnich vyrazech");
	else if(code == E_ELS)
		fprintf(stderr, "Ostatni semanticke/behove chyby");
	else if(code == E_INT)
		fprintf(stderr, "Interni chyba interpretu");
	
	gc_clear();
	exit(code);
}
