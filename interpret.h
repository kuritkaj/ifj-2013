#include "common.h"
#include "ast.h"
#include "s_table.h"

#ifndef INTERPRET_H
#define INTERPRET_H

enum interpret_inline_func { BOOLVAL, DOUBLEVAL, INTVAL, STRVAL, PUT_STR, GET_STR, STR_LEN, GET_SUB, FIND_STR, SORT_STR };
struct var_list {
	struct var_node* frst;
};
struct var_node{
	int scope;
	char* name;
	struct var_node* next;
};
/*
void var_list_init(struct var_list* vl);
int var_list_add(struct data* d, struct var_list* vl, char* name, int scope);
int var_list_dispose(struct data* d, struct var_list* vl, int scope);
*/
char* encode_name(char* name, int scope);
char* decode_name(char* name, int* scope);

int ast_is_bool(enum ast_literal_type);
int ast_is_numeric(enum ast_literal_type);
int ast_is_string(enum ast_literal_type);
int ast_is_null(enum ast_literal_type);
int ast_is_strict_equals(struct ast_node*, struct ast_node*);
struct symbol_table fill_variable(struct symbol_table* var);
char* get_literal_type_string(int type);
char* get_var_type_string(int type);

int is_logic_op(struct ast_node* nd);
int is_math_op(struct ast_node* nd);
int exec_logic_operation(struct data* d, struct symbol_table* l, struct symbol_table* r, enum ast_binary_op_type op);
int make_from_literal(struct data* d, struct ast_node* lit, struct symbol_table* out);
int make_from_node(struct data* d, struct ast_node* node, struct symbol_table* out, int scope);
int exec_var(struct symbol_table var);
int find_all_definitions(struct data* d, struct ast_list* zero_lvl);
void print_tree(struct symbol_table* root, int lvl, char dir);
void print_ast(struct ast_node* tree, int lvl, char dir);
void print_param_list(struct ast_list* list, int l);
void print_instruction_list(struct ast_list* zero_lvl, int l);


int interpret(struct data* d);
int ast_statement(struct data* d, struct ast_node* nd, struct symbol_table* out, int scope);
int ast_assign(struct data* d, struct ast_node* nd, struct symbol_table* out, int scope);
int ast_loop(struct data* d, struct ast_node* nd, struct symbol_table* out, int scope);
int ast_if(struct data* d, struct ast_node* nd, struct symbol_table* out, int scope);
void ast_literal(struct data* d, struct ast_node* nd, struct symbol_table* out, int scope);
int ast_return(struct data* d, struct ast_node* nd, struct symbol_table* out, int scope);
int ast_function(struct data* d, struct ast_node* nd);
int ast_call(struct data* d, struct ast_node* nd, struct symbol_table* out, int scope);
int ast_var(struct data* d, struct ast_node* nd, struct symbol_table* out, int scope);
int ast_binary_op(struct data* d, struct ast_node* nd, struct symbol_table* out, int scope);
int ast_logic_binary_op(struct data* d, struct ast_node* nd, int scope);
int ast_concat(struct data* d, struct ast_node* nd, struct symbol_table* out, int scope);


int walk_through(struct data* d, struct ast_node* node, struct symbol_table* out, int scope);
#endif

