/*
    IFJ 2013

    xmarko07 - Antonín Marko
    xmahne00 - Jakub Mahnert
    xkurit00 - Jakub Kuřitka
    xkubic34 - Martin Kubíček
    xlechp00  - Pali Lech

    "Let the C language die."
 */
#include <stdlib.h>
#include <stdbool.h>

#ifndef GC_H
#define GC_H

/**
 * Inicializuje GC vraci false v pripade chyby. Nutne volat pred pouzitim gc !
 * 
 * @return bool error
 */
bool gc_init();

/**
 * Naalokuje misto na halde, prida ho do referenci gc a vrati pointer nebo NULL v pripade chyby
 * 
 * @param size
 *
 * @return void*
 */
void* gc_malloc(size_t size);


/**
 * Realokuje misto
 *
 * @param void* ref
 * @param size_t size
 * 
 * @return void*
 */
void* gc_realloc(void* ref, size_t size);


/**
 * Uvolni pointer
 *
 * @param void* p
 *
 * @return void
 */
void gc_free(void* p);

/**
 * Vycisti vsechny reference pomoci free. Pokud chceme po teto fci pouzivat gc, musime zavolat gc_init !
 *
 * @return void
 */
void gc_clear();

/**
 * Count of managed refs
 * @return int
 */
int gc_count();

/*
 * Makro, ktere ukonci funkci a vrati false, kdyz nastal error
 */
#define ALLOC(e, ret)\
        if(e == NULL)  { printf("ALLOC: not allocated!!! this means divorce!!!\n"); return ret;} 


#endif
