/*
    IFJ 2013

    xmarko07 - Antonín Marko
    xmahne00 - Jakub Mahnert
    xkurit00 - Jakub Kuřitka
    xkubic34 - Martin Kubíček
    xlechp00  - Pali Lech

    "Let the C language die."
 */
    
#include <stdbool.h>
#include "common.h"
#include "str.h"

#ifndef SCANNER_H
#define SCANNER_H

char* to_lower(char* sour);
void print_token_data(struct token* t);
void add_token(struct data* d, enum token_type t, string* acc);
bool get_next_token(struct data* d);
bool is_alpha(char c);
bool is_number(char c);
bool is_extranumber(char c);
bool is_operator(char c);
bool is_extraoperator(char c);
bool is_separator(char c);
int identificator(struct data* d, char akt);
int number(struct data* d, char akt);
int variable(struct data* d);
int text(struct data* d);
void comment_inline(struct data* d);
void comment_multiline(struct data* d);
int operator(struct data* d, char akt);
int extraoperator(struct data* d, char akt);
int semicomma(struct data* d, char akt);
int load_lex(struct data* d);
bool checkPhp(struct data* d);


#endif
