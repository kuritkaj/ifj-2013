/*
    IFJ 2013

    xmarko07 - Antonín Marko
    xmahne00 - Jakub Mahnert
    xkurit00 - Jakub Kuřitka
    xkubic34 - Martin Kubíček
    xlechp00  - Pali Lech

    "Let the C language die."
 */

#include <stdlib.h>
#include <stdio.h>
#include "ast.h"
#include "gc.h"

#define PRINT 0

struct ast_node* ast_create_node()
{
    struct ast_node* n = (struct ast_node*)gc_malloc(sizeof(struct ast_node));
    
    ALLOC( n, NULL )

    if ( PRINT ) printf("ast:ast_create_node:%p\n", (void*)n);
    n->left =  NULL;
    n->right = NULL;

    return n;
}

struct ast_list* ast_create_list()
{
    struct ast_list* l = (struct ast_list*)gc_malloc(sizeof(struct ast_list));

    ALLOC( l, NULL )
    
    if ( PRINT ) printf("ast:ast_create_list:%p\n", (void*)l);

    l->elem = NULL;
    l->next = NULL;

    return l;
}

bool ast_list_insert(struct ast_list* l, struct ast_node* n)
{
    if ( PRINT ) printf("ast:ast_list_insert: %p -> elem = %p\n", (void*)l, (void*)n);
    
    struct ast_node* elem = ast_create_node();

    if ( PRINT ) printf("ast:ast_list_insert: created new node %p for %p\n", (void*)elem, (void*)n);

    ALLOC( elem, false )

    *elem = *n;

    if (l->elem == NULL)
    {
        if ( PRINT ) printf("ast:ast_list_insert: l->elem = NULL, inserting to l->elem = %p\n, l->next = NULL\n", (void*)elem);
        l->elem = elem;
        l->next = NULL;
    }
    else
    {
        struct ast_list* sl = ast_create_list();

        ALLOC( sl, false )

        // najdeme nejpravejsi node
        struct ast_list* parent = ast_list_get_last(l);

        // a vloime do nej data
        if ( PRINT ) printf("ast:ast_list_insert: l = %p, l->elem = %p, inserting to l->next = %p, l->next->elem = %p, l->next->next = NULL\n", (void*)parent, (void*)parent->elem, (void*)sl, (void*)elem);

        sl->elem = elem;
        sl->next = NULL;
        parent->next = sl;
    }

    return true;
}

struct ast_list* ast_list_get_last(struct ast_list* l)
{
    if(l->next == NULL) return l;

    return ast_list_get_last(l->next);
}

void ast_list_print(struct ast_list* l)
{
    if(l == NULL) return;

    if ( PRINT ) printf("ast_list_print: elem = %p, next = %p\n", (void*)l->elem, (void*)l->next);
    ast_node_print(l->elem);
    ast_list_print(l->next);
}

void ast_node_print(struct ast_node* n)
{
    if(n == NULL) return;

    if ( PRINT ) printf("ast_node_print: node = %p\n", (void*)n);
    if ( PRINT ) printf("ast_node_print: node->type = %d\n", n->type);
    if ( PRINT ) printf("ast_node_print: node->literal = %d\n", n->literal);
    if ( PRINT ) printf("ast_node_print: node->left = %p\n", (void*)n->left);
    if ( PRINT ) printf("ast_node_print: node->right = %p\n", (void*)n->right);
}