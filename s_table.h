/*
    IFJ 2013

    xmarko07 - Antonín Marko
    xmahne00 - Jakub Mahnert
    xkurit00 - Jakub Kuřitka
    xkubic34 - Martin Kubíček
    xlechp00  - Pali Lech

    "Let the C language die."
 */
#include "common.h"
#include <stdlib.h>

#ifndef S_TABLE_H
#define S_TABLE_H

struct symbol_table* new_symbol_table();
struct symbol_table_data* new_symbol_table_data();
struct symbol_table* new_knot(struct data* d, char* key, enum ast_var_type type, struct symbol_table_data data);
void symbol_init(struct symbol_table**);
int symbol_copy(struct data* d, struct symbol_table* original, struct symbol_table* copy);
struct symbol_table* symbol_search(struct symbol_table*, char*);
struct symbol_table* symbol_insert(struct data* d, struct symbol_table** root, char* key, enum ast_var_type type, struct symbol_table_data data);
void symbol_table_dispose(struct symbol_table**);
struct symbol_table symbol_search_data(struct symbol_table* root, char* key, int* is_found);
void symbol_table_delete(struct symbol_table** root, char* key);
#endif
